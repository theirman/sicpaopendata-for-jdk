package fr.inrae.sicpa.SicpaOpenData.Examples;


/** Classe montrant comment utiliser les classes de <c>SicpaOpenData.Metadata</c>
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class MetadataExamples
{
    /** <c>HowToBuildABiomedicalNode</c> montre la manière de construire un <c>BiomedicalNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *     // je crée un node biomedical
     *     BiomedicalNode bn = new BiomedicalNode();
     *
     *     bn.addField(new MultipleControlledVocabularyNode("studyDesignType", new ArrayList<String>() {{ add("Case Control"); add("Cross Sectional"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyDesignTypeOther", new ArrayList<String>() {{ add("studyDesignTypeOther1"); add("studyDesignTypeOther2"); }}));
     *     bn.addField(new MultipleControlledVocabularyNode("studyFactorType", new ArrayList<String>() {{ add("Age"); add("Biomarkers"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyFactorTypeOther", new ArrayList<String>() {{ add("studyFactorTypeOther1"); add("studyFactorTypeOther2"); }}));
     *     bn.addField(new MultipleControlledVocabularyNode("studyAssayOrganism", new ArrayList<String>() {{ add("Arabidopsis thaliana"); add("Bos taurus"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyAssayOtherOrganism", new ArrayList<String>() {{ add("studyAssayOtherOrganism1"); add("studyAssayOtherOrganism2"); }}));
     *     bn.addField(new MultipleControlledVocabularyNode("studyAssayMeasurementType", new ArrayList<String>() {{ add("cell counting"); add("cell sorting"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyAssayOtherMeasurmentType", new ArrayList<String>() {{ add("studyAssayOtherMeasurmentType1"); add("studyAssayOtherMeasurmentType2"); }}));
     *     bn.addField(new MultipleControlledVocabularyNode("studyAssayTechnologyType", new ArrayList<String>() {{ add("culture based drug susceptibility testing, single concentration"); add("culture based drug susceptibility testing, two concentrations"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyAssayTechnologyTypeOther", new ArrayList<String>() {{ add("studyAssayTechnologyTypeOther1"); add("studyAssayTechnologyTypeOther2"); }}));
     *     bn.addField(new MultipleControlledVocabularyNode("studyAssayPlatform", new ArrayList<String>() {{ add("210-MS GC Ion Trap (Varian)"); add("220-MS GC Ion Trap (Varian)"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyAssayPlatformOther", new ArrayList<String>() {{ add("studyAssayPlatformOther1"); add("studyAssayPlatformOther2"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyAssayCellType", new ArrayList<String>() {{ add("studyAssayCellType1"); add("studyAssayCellType2"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studySampleType", new ArrayList<String>() {{ add("studySampleType1"); add("studySampleType2"); }}));
     *     bn.addField(new MultiplePrimitiveNode("studyProtocolType", new ArrayList<String>() {{ add("studyProtocolType1"); add("studyProtocolType2"); }}));
     *
     *     System.out.print(bn.toJSON());
     *  </pre>
     */
    public static void HowToBuildABiomedicalNode()
    {
        System.out.print
        (
            System.lineSeparator() +  "// je crée un node biomedical" + 
            System.lineSeparator() +  "BiomedicalNode bn = new BiomedicalNode();" + 
            System.lineSeparator() +  
            System.lineSeparator() +  "bn.addField(new MultipleControlledVocabularyNode(\"studyDesignType\", new ArrayList<String>() {{ add(\"Case Control\"); add(\"Cross Sectional\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyDesignTypeOther\", new ArrayList<String>() {{ add(\"studyDesignTypeOther1\"); add(\"studyDesignTypeOther2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultipleControlledVocabularyNode(\"studyFactorType\", new ArrayList<String>() {{ add(\"Age\"); add(\"Biomarkers\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyFactorTypeOther\", new ArrayList<String>() {{ add(\"studyFactorTypeOther1\"); add(\"studyFactorTypeOther2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultipleControlledVocabularyNode(\"studyAssayOrganism\", new ArrayList<String>() {{ add(\"Arabidopsis thaliana\"); add(\"Bos taurus\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyAssayOtherOrganism\", new ArrayList<String>() {{ add(\"studyAssayOtherOrganism1\"); add(\"studyAssayOtherOrganism2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultipleControlledVocabularyNode(\"studyAssayMeasurementType\", new ArrayList<String>() {{ add(\"cell counting\"); add(\"cell sorting\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyAssayOtherMeasurmentType\", new ArrayList<String>() {{ add(\"studyAssayOtherMeasurmentType1\"); add(\"studyAssayOtherMeasurmentType2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultipleControlledVocabularyNode(\"studyAssayTechnologyType\", new ArrayList<String>() {{ add(\"culture based drug susceptibility testing, single concentration\"); add(\"culture based drug susceptibility testing, two concentrations\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyAssayTechnologyTypeOther\", new ArrayList<String>() {{ add(\"studyAssayTechnologyTypeOther1\"); add(\"studyAssayTechnologyTypeOther2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultipleControlledVocabularyNode(\"studyAssayPlatform\", new ArrayList<String>() {{ add(\"210-MS GC Ion Trap (Varian)\"); add(\"220-MS GC Ion Trap (Varian)\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyAssayPlatformOther\", new ArrayList<String>() {{ add(\"studyAssayPlatformOther1\"); add(\"studyAssayPlatformOther2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyAssayCellType\", new ArrayList<String>() {{ add(\"studyAssayCellType1\"); add(\"studyAssayCellType2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studySampleType\", new ArrayList<String>() {{ add(\"studySampleType1\"); add(\"studySampleType2\"); }}));" + 
            System.lineSeparator() +  "bn.addField(new MultiplePrimitiveNode(\"studyProtocolType\", new ArrayList<String>() {{ add(\"studyProtocolType1\"); add(\"studyProtocolType2\"); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() +  "System.out.print(bn.toJSON());"
        );
    }

    /** <c>HowToBuildACitationNode</c> montre la manière de construire un <c>CitationNode</c> complet
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *  // je crée les auteurs du dataset
     *  Hashtable<String,BaseNode> htAuteur1 = new Hashtable<String,BaseNode>();
     *  htAuteur1.put("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1"));
     *  htAuteur1.put("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1"));
     *  htAuteur1.put("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"));
     *  htAuteur1.put("authorName", new SimplePrimitiveNode("authorName", "authorName1"));
     *  
     *  Hashtable<String,BaseNode> htAuteur2 = new Hashtable<String,BaseNode>();
     *  htAuteur2.put("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2"));
     *  htAuteur2.put("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2"));
     *  htAuteur2.put("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL"));
     *  htAuteur2.put("authorName", new SimplePrimitiveNode("authorName", "authorName2"));
     *  
     *  // je crée les autres identifiants
     *  Hashtable<String,BaseNode> htAutreId1 = new Hashtable<String,BaseNode>();
     *  htAutreId1.put("otherIdAgency", new SimplePrimitiveNode("otherIdAgency", "otherIdAgency1"));
     *  htAutreId1.put("otherIdValue", new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier1"));
     *  
     *  Hashtable<String,BaseNode> htAutreId2 = new Hashtable<String,BaseNode>();
     *  htAutreId2.put("otherIdAgency", new SimplePrimitiveNode("otherIdAgency", "otherIdAgency2"));
     *  htAutreId2.put("otherIdValue", new SimplePrimitiveNode("otherIdValue", "otherIdAgencyIdentifier2"));
     *  
     *  // je crée les contacts du dataset
     *  Hashtable<String,BaseNode> htContact1 = new Hashtable<String,BaseNode>();
     *  htContact1.put("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1"));
     *  htContact1.put("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com"));
     *  htContact1.put("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName1"));
     *  
     *  Hashtable<String,BaseNode> htContact2 = new Hashtable<String,BaseNode>();
     *  htContact2.put("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2"));
     *  htContact2.put("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com"));
     *  htContact2.put("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName2"));
     *  
     *  // je crée les contributeurs du dataset
     *  Hashtable<String,BaseNode> htContributeur1 = new Hashtable<String,BaseNode>();
     *  htContributeur1.put("contributorAffiliation", new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation1"));
     *  htContributeur1.put("contributorIdentifier", new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier1"));
     *  htContributeur1.put("contributorIdentifierScheme", new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID"));
     *  htContributeur1.put("contributorName", new SimplePrimitiveNode("contributorName", "contributorName1"));
     *  htContributeur1.put("contributorType", new SimpleControlledVocabularyNode("contributorType", "Data collector"));
     *  
     *  Hashtable<String,BaseNode> htContributeur2 = new Hashtable<String,BaseNode>();
     *  htContributeur2.put("contributorAffiliation", new SimplePrimitiveNode("contributorAffiliation", "contributorAffiliation2"));
     *  htContributeur2.put("contributorIdentifier", new SimplePrimitiveNode("contributorIdentifier", "contributorIdentifier2"));
     *  htContributeur2.put("contributorIdentifierScheme", new SimpleControlledVocabularyNode("contributorIdentifierScheme", "ORCID"));
     *  htContributeur2.put("contributorName", new SimplePrimitiveNode("contributorName", "contributorName2"));
     *  htContributeur2.put("contributorType", new SimpleControlledVocabularyNode("contributorType", "Editor"));
     *  
     *  // je crée les dates des collections du dataset
     *  Hashtable<String,BaseNode> htDates1 = new Hashtable<String,BaseNode>();
     *  htDates1.put("dateOfCollectionStart", new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-01"));
     *  htDates1.put("dateOfCollectionEnd", new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-02"));
     *  
     *  Hashtable<String,BaseNode> htDates2 = new Hashtable<String,BaseNode>();
     *  htDates2.put("dateOfCollectionStart", new SimplePrimitiveNode("dateOfCollectionStart", "2000-01-03"));
     *  htDates2.put("dateOfCollectionEnd", new SimplePrimitiveNode("dateOfCollectionEnd", "2000-01-04"));
     *  
     *  // je crée les descriptions du dataset
     *  Hashtable<String,BaseNode> htDescription1 = new Hashtable<String,BaseNode>();
     *  htDescription1.put("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
     *  htDescription1.put("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1"));
     *  
     *  Hashtable<String,BaseNode> htDescription2 = new Hashtable<String,BaseNode>();
     *  htDescription2.put("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
     *  htDescription2.put("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2"));
     *  
     *  // je crée les distributeurs du dataset
     *  Hashtable<String,BaseNode> htDistributeur1 = new Hashtable<String,BaseNode>();
     *  htDistributeur1.put("distributorAbbreviation", new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation1"));
     *  htDistributeur1.put("distributorAffiliation", new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation1"));
     *  htDistributeur1.put("distributorLogoURL", new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL1"));
     *  htDistributeur1.put("distributorName", new SimplePrimitiveNode("distributorName", "distributorName1"));
     *  htDistributeur1.put("distributorURL", new SimplePrimitiveNode("distributorURL", "distributorURL1"));
     *  
     *  Hashtable<String,BaseNode> htDistributeur2 = new Hashtable<String,BaseNode>();
     *  htDistributeur2.put("distributorAbbreviation", new SimplePrimitiveNode("distributorAbbreviation", "distributorAbbreviation2"));
     *  htDistributeur2.put("distributorAffiliation", new SimplePrimitiveNode("distributorAffiliation", "distributorAffiliation2"));
     *  htDistributeur2.put("distributorLogoURL", new SimplePrimitiveNode("distributorLogoURL", "distributorLogoURL2"));
     *  htDistributeur2.put("distributorName", new SimplePrimitiveNode("distributorName", "distributorName2"));
     *  htDistributeur2.put("distributorURL", new SimplePrimitiveNode("distributorURL", "distributorURL2"));
     *  
     *  // je crée les grant number du dataset
     *  Hashtable<String,BaseNode> htGrantNumber1 = new Hashtable<String,BaseNode>();
     *  htGrantNumber1.put("grantNumberAgency", new SimplePrimitiveNode("grantNumberAgency", "grantAgency1"));
     *  htGrantNumber1.put("grantNumberValue", new SimplePrimitiveNode("grantNumberValue", "grantNumber1"));
    
     *  Hashtable<String,BaseNode> htGrantNumber2 = new Hashtable<String,BaseNode>();
     *  htGrantNumber2.put("grantNumberAgency", new SimplePrimitiveNode("grantNumberAgency", "grantAgency2"));
     *  htGrantNumber2.put("grantNumberValue", new SimplePrimitiveNode("grantNumberValue", "grantNumber2"));
     *  
     *  // je crée les logiciels du dataset
     *  Hashtable<String,BaseNode> htLogiciel1 = new Hashtable<String,BaseNode>();
     *  htLogiciel1.put("softwareName", new SimplePrimitiveNode("softwareName", "softwareName1"));
     *  htLogiciel1.put("softwareVersion", new SimplePrimitiveNode("softwareVersion", "softwareVersion1"));
     *  
     *  Hashtable<String,BaseNode> htLogiciel2 = new Hashtable<String,BaseNode>();
     *  htLogiciel2.put("softwareName", new SimplePrimitiveNode("softwareName", "softwareName2"));
     *  htLogiciel2.put("softwareVersion", new SimplePrimitiveNode("softwareVersion", "softwareVersion2"));
     *  
     *  // je crée les mots clés du dataset
     *  Hashtable<String,BaseNode> htMotCle1 = new Hashtable<String,BaseNode>();
     *  htMotCle1.put("keywordTermURI", new SimplePrimitiveNode("keywordTermURI", "keywordTermURI1"));
     *  htMotCle1.put("keywordValue", new SimplePrimitiveNode("keywordValue", "keywordValue1"));
     *  htMotCle1.put("keywordVocabulary", new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary1"));
     *  htMotCle1.put("keywordVocabularyURI", new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI1"));
     *  
     *  Hashtable<String,BaseNode> htMotCle2 = new Hashtable<String,BaseNode>();
     *  htMotCle2.put("keywordTermURI", new SimplePrimitiveNode("keywordTermURI", "keywordTermURI2"));
     *  htMotCle2.put("keywordValue", new SimplePrimitiveNode("keywordValue", "keywordValue2"));
     *  htMotCle2.put("keywordVocabulary", new SimplePrimitiveNode("keywordVocabulary", "keywordVocabulary2"));
     *  htMotCle2.put("keywordVocabularyURI", new SimplePrimitiveNode("keywordVocabularyURI", "keywordVocabularyURI2"));
     *  
     *  // je crée les producteurs du dataset
     *  Hashtable<String,BaseNode> htProducteur1 = new Hashtable<String,BaseNode>();
     *  htProducteur1.put("producerAbbreviation", new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation1"));
     *  htProducteur1.put("producerAffiliation", new SimplePrimitiveNode("producerAffiliation", "producerAffiliation1"));
     *  htProducteur1.put("producerLogoURL", new SimplePrimitiveNode("producerLogoURL", "producerLogoURL1"));
     *  htProducteur1.put("producerName", new SimplePrimitiveNode("producerName", "producerName1"));
     *  htProducteur1.put("producerURL", new SimplePrimitiveNode("producerURL", "producerURL1"));
     *  
     *  Hashtable<String,BaseNode> htProducteur2 = new Hashtable<String,BaseNode>();
     *  htProducteur2.put("producerAbbreviation", new SimplePrimitiveNode("producerAbbreviation", "producerAbbreviation2"));
     *  htProducteur2.put("producerAffiliation", new SimplePrimitiveNode("producerAffiliation", "producerAffiliation2"));
     *  htProducteur2.put("producerLogoURL", new SimplePrimitiveNode("producerLogoURL", "producerLogoURL2"));
     *  htProducteur2.put("producerName", new SimplePrimitiveNode("producerName", "producerName2"));
     *  htProducteur2.put("producerURL", new SimplePrimitiveNode("producerURL", "producerURL2"));
     *  
     *  // je crée la période du dataset
     *  Hashtable<String,BaseNode> htPeriode1 = new Hashtable<String,BaseNode>();
     *  htPeriode1.put("timePeriodCoveredStart", new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-01"));
     *  htPeriode1.put("timePeriodCoveredEnd", new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-02"));
     *  
     *  Hashtable<String,BaseNode> htPeriode2 = new Hashtable<String,BaseNode>();
     *  htPeriode2.put("timePeriodCoveredStart", new SimplePrimitiveNode("timePeriodCoveredStart", "2000-01-03"));
     *  htPeriode2.put("timePeriodCoveredEnd", new SimplePrimitiveNode("timePeriodCoveredEnd", "2000-01-04"));
     *  
     *  // je crée le projet du dataset
     *  Hashtable<String,BaseNode> htProjet = new Hashtable<String,BaseNode>();
     *  htProjet.put("projectAcronym", new SimplePrimitiveNode("projectAcronym", "projectAcronym"));
     *  htProjet.put("projectId", new SimplePrimitiveNode("projectId", "projectId"));
     *  htProjet.put("projectTask", new SimplePrimitiveNode("projectTask", "projectTask"));
     *  htProjet.put("projectTitle", new SimplePrimitiveNode("projectTitle", "projectTitle"));
     *  htProjet.put("projectURL", new SimplePrimitiveNode("projectURL", "http://project.url"));
     *  htProjet.put("projectWorkPackage", new SimplePrimitiveNode("projectWorkPackage", "projectWP"));
     *  
     *  // je crée les producteurs du dataset
     *  Hashtable<String,BaseNode> htPublication1 = new Hashtable<String,BaseNode>();
     *  htPublication1.put("publicationCitation", new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation1"));
     *  htPublication1.put("publicationIDNumber", new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber1"));
     *  htPublication1.put("publicationIDType", new SimpleControlledVocabularyNode("publicationIDType", "ark"));
     *  htPublication1.put("publicationURL", new SimplePrimitiveNode("publicationURL", "http://related.publication.url1"));
     *  
     *  Hashtable<String,BaseNode> htPublication2 = new Hashtable<String,BaseNode>();
     *  htPublication2.put("publicationCitation", new SimplePrimitiveNode("publicationCitation", "relatedPublicationCitation2"));
     *  htPublication2.put("publicationIDNumber", new SimplePrimitiveNode("publicationIDNumber", "relatedPublicationIDNumber2"));
     *  htPublication2.put("publicationIDType", new SimpleControlledVocabularyNode("publicationIDType", "arXiv"));
     *  htPublication2.put("publicationURL", new SimplePrimitiveNode("publicationURL", "http://related.publication.url2"));
     *  
     *  // je crée les relatedDataset du dataset
     *  Hashtable<String,BaseNode> htRelatedDataset1 = new Hashtable<String,BaseNode>();
     *  htRelatedDataset1.put("relatedDatasetCitation", new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation1"));
     *  htRelatedDataset1.put("relatedDatasetIDNumber", new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber1"));
     *  htRelatedDataset1.put("relatedDatasetIDType", new SimpleControlledVocabularyNode("relatedDatasetIDType", "ark"));
     *  htRelatedDataset1.put("relatedDatasetURL", new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url1"));
     *  
     *  Hashtable<String,BaseNode> htRelatedDataset2 = new Hashtable<String,BaseNode>();
     *  htRelatedDataset2.put("relatedDatasetCitation", new SimplePrimitiveNode("relatedDatasetCitation", "relatedDatasetCitation2"));
     *  htRelatedDataset2.put("relatedDatasetIDNumber", new SimplePrimitiveNode("relatedDatasetIDNumber", "relatedDatasetIDNumber2"));
     *  htRelatedDataset2.put("relatedDatasetIDType", new SimpleControlledVocabularyNode("relatedDatasetIDType", "doi"));
     *  htRelatedDataset2.put("relatedDatasetURL", new SimplePrimitiveNode("relatedDatasetURL", "http://related.dataset.url2"));
     *  
     *  // je crée la série du dataset
     *  Hashtable<String,BaseNode> htSerie = new Hashtable<String,BaseNode>();
     *  htSerie.put("seriesInformation", new SimplePrimitiveNode("seriesInformation", "seriesInformation"));
     *  htSerie.put("seriesName", new SimplePrimitiveNode("seriesName", "seriesName"));
     *  
     *  // je crée les topics classifications du dataset
     *  Hashtable<String,BaseNode> htTopicClassification1 = new Hashtable<String,BaseNode>();
     *  htTopicClassification1.put("topicClassValue", new SimplePrimitiveNode("topicClassValue", "topicClassValue1"));
     *  htTopicClassification1.put("topicClassVocab", new SimplePrimitiveNode("topicClassVocab", "topicClassVocab1"));
     *  htTopicClassification1.put("topicClassVocabURI", new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI1"));
     *  
     *  Hashtable<String,BaseNode> htTopicClassification2 = new Hashtable<String,BaseNode>();
     *  htTopicClassification2.put("topicClassValue", new SimplePrimitiveNode("topicClassValue", "topicClassValue2"));
     *  htTopicClassification2.put("topicClassVocab", new SimplePrimitiveNode("topicClassVocab", "topicClassVocab2"));
     *  htTopicClassification2.put("topicClassVocabURI", new SimplePrimitiveNode("topicClassVocabURI", "topicClassVocabURI2"));
     *  
     *  // je crée un node citation
     *  CitationNode cn = new CitationNode();
     *  cn.addField(new SimplePrimitiveNode("title", "title"));
     *  cn.addField(new SimplePrimitiveNode("subtitle", "subtitle"));
     *  cn.addField(new SimplePrimitiveNode("alternativeTitle", "alternativeTitle"));
     *  cn.addField(new SimplePrimitiveNode("alternativeURL", "http://link.to.data"));
     *  cn.addField(new MultipleCompoundNode("otherId", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htAutreId1); add(htAutreId2); }}));
     *  cn.addField(new MultipleCompoundNode("datasetContact", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htContact1); add(htContact2); }}));
     *  cn.addField(new MultipleCompoundNode("author", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htAuteur1); add(htAuteur2); }}));
     *  cn.addField(new MultipleCompoundNode("contributor", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htContributeur1); add(htContributeur2); }}));
     *  cn.addField(new MultipleCompoundNode("producer", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htProducteur1); add(htProducteur2); }}));
     *  cn.addField(new SimplePrimitiveNode("productionDate", "2000-01-01"));
     *  cn.addField(new SimplePrimitiveNode("productionPlace", "productionPlace"));
     *  cn.addField(new MultipleCompoundNode("distributor", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDistributeur1); add(htDistributeur2); }}));
     *  cn.addField(new SimplePrimitiveNode("distributionDate", "2000-01-01"));
     *  cn.addField(new MultipleCompoundNode("dsDescription", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDescription1); add(htDescription2); }}));
     *  cn.addField(new MultipleControlledVocabularyNode("language", new ArrayList<String>() {{ add("Abkhaz"); add("Afar"); }}));
     *  cn.addField(new MultipleControlledVocabularyNode("subject", new ArrayList<String>() {{ add("Animal Breeding and Animal Products"); add("Animal Health and Pathology"); }}));
     *  cn.addField(new MultipleCompoundNode("keyword", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htMotCle1); add(htMotCle1); }}));
     *  cn.addField(new MultipleCompoundNode("topicClassification", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htTopicClassification1); add(htTopicClassification2); }}));
     *  cn.addField(new MultipleControlledVocabularyNode("kindOfData", new ArrayList<String>() {{ add("Audiovisual"); add("Collection"); }}));
     *  cn.addField(new MultiplePrimitiveNode("kindOfDataOther", new ArrayList<String>() {{ add("otherKindOfData1"); add("otherKindOfData2"); }}));
     *  cn.addField(new MultipleControlledVocabularyNode("dataOrigin", new ArrayList<String>() {{ add("observational data"); add("experimental data"); }}));
     *  cn.addField(new MultiplePrimitiveNode("dataSources", new ArrayList<String>() {{ add("dataSource1"); add("dataSource2"); }}));
     *  cn.addField(new SimplePrimitiveNode("originOfSources", "originOfSource"));
     *  cn.addField(new SimplePrimitiveNode("characteristicOfSources", "characteristicOfSource"));
     *  cn.addField(new SimplePrimitiveNode("accessToSources", "accessToSource"));
     *  cn.addField(new MultipleCompoundNode("software", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htLogiciel1); add(htLogiciel2); }}));
     *  cn.addField(new SimpleCompoundNode("series", htSerie));
     *  cn.addField(new MultipleControlledVocabularyNode("lifeCycleStep", new ArrayList<String>() {{ add("Study proposal"); add("Funding"); }}));
     *  cn.addField(new SimplePrimitiveNode("notesText", "notes"));
     *  cn.addField(new MultipleCompoundNode("publication", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htPublication1); add(htPublication2); }}));
     *  cn.addField(new MultiplePrimitiveNode("relatedMaterial", new ArrayList<String>() {{ add("relatedMaterial1"); add("relatedMaterial2"); }}));
     *  cn.addField(new MultipleCompoundNode("relatedDataset", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htRelatedDataset1); add(htRelatedDataset2); }}));
     *  cn.addField(new MultiplePrimitiveNode("otherReferences", new ArrayList<String>() {{ add("otherReference1"); add("otherReference2"); }}));
     *  cn.addField(new MultipleCompoundNode("grantNumber", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htGrantNumber1); add(htGrantNumber2); }}));
     *  cn.addField(new SimpleCompoundNode("project", htProjet));
     *  cn.addField(new MultipleCompoundNode("timePeriodCovered", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htPeriode1); add(htPeriode2); }}));
     *  cn.addField(new MultipleCompoundNode("dateOfCollection", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDates1); add(htDates2); }}));
     *  cn.addField(new SimplePrimitiveNode("depositor", "depositor"));
     *  cn.addField(new SimplePrimitiveNode("dateOfDeposit", "2000-01-01"));
     *  
     *  System.out.print(cn.toJSON());
     *  </pre>
     */ 
    public static void HowToBuildACitationNode()
    {
        System.out.print
        (
            System.lineSeparator() + "// je crée les auteurs du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htAuteur1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htAuteur1.put(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation1\"));" + 
            System.lineSeparator() + "htAuteur1.put(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier1\"));" + 
            System.lineSeparator() + "htAuteur1.put(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"ORCID\"));" + 
            System.lineSeparator() + "htAuteur1.put(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htAuteur2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htAuteur2.put(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation2\"));" + 
            System.lineSeparator() + "htAuteur2.put(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier2\"));" + 
            System.lineSeparator() + "htAuteur2.put(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"idHAL\"));" + 
            System.lineSeparator() + "htAuteur2.put(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les autres identifiants" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htAutreId1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htAutreId1.put(\"otherIdAgency\", new SimplePrimitiveNode(\"otherIdAgency\", \"otherIdAgency1\"));" + 
            System.lineSeparator() + "htAutreId1.put(\"otherIdValue\", new SimplePrimitiveNode(\"otherIdValue\", \"otherIdAgencyIdentifier1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htAutreId2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htAutreId2.put(\"otherIdAgency\", new SimplePrimitiveNode(\"otherIdAgency\", \"otherIdAgency2\"));" + 
            System.lineSeparator() + "htAutreId2.put(\"otherIdValue\", new SimplePrimitiveNode(\"otherIdValue\", \"otherIdAgencyIdentifier2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les contacts du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htContact1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htContact1.put(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation1\"));" + 
            System.lineSeparator() + "htContact1.put(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact1@mail.com\"));" + 
            System.lineSeparator() + "htContact1.put(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htContact2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htContact2.put(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation2\"));" + 
            System.lineSeparator() + "htContact2.put(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact2@mail.com\"));" + 
            System.lineSeparator() + "htContact2.put(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les contributeurs du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htContributeur1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htContributeur1.put(\"contributorAffiliation\", new SimplePrimitiveNode(\"contributorAffiliation\", \"contributorAffiliation1\"));" + 
            System.lineSeparator() + "htContributeur1.put(\"contributorIdentifier\", new SimplePrimitiveNode(\"contributorIdentifier\", \"contributorIdentifier1\"));" + 
            System.lineSeparator() + "htContributeur1.put(\"contributorIdentifierScheme\", new SimpleControlledVocabularyNode(\"contributorIdentifierScheme\", \"ORCID\"));" + 
            System.lineSeparator() + "htContributeur1.put(\"contributorName\", new SimplePrimitiveNode(\"contributorName\", \"contributorName1\"));" + 
            System.lineSeparator() + "htContributeur1.put(\"contributorType\", new SimpleControlledVocabularyNode(\"contributorType\", \"Data collector\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htContributeur2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htContributeur2.put(\"contributorAffiliation\", new SimplePrimitiveNode(\"contributorAffiliation\", \"contributorAffiliation2\"));" + 
            System.lineSeparator() + "htContributeur2.put(\"contributorIdentifier\", new SimplePrimitiveNode(\"contributorIdentifier\", \"contributorIdentifier2\"));" + 
            System.lineSeparator() + "htContributeur2.put(\"contributorIdentifierScheme\", new SimpleControlledVocabularyNode(\"contributorIdentifierScheme\", \"ORCID\"));" + 
            System.lineSeparator() + "htContributeur2.put(\"contributorName\", new SimplePrimitiveNode(\"contributorName\", \"contributorName2\"));" + 
            System.lineSeparator() + "htContributeur2.put(\"contributorType\", new SimpleControlledVocabularyNode(\"contributorType\", \"Editor\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les dates des collections du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDates1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDates1.put(\"dateOfCollectionStart\", new SimplePrimitiveNode(\"dateOfCollectionStart\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htDates1.put(\"dateOfCollectionEnd\", new SimplePrimitiveNode(\"dateOfCollectionEnd\", \"2000-01-02\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDates2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDates2.put(\"dateOfCollectionStart\", new SimplePrimitiveNode(\"dateOfCollectionStart\", \"2000-01-03\"));" + 
            System.lineSeparator() + "htDates2.put(\"dateOfCollectionEnd\", new SimplePrimitiveNode(\"dateOfCollectionEnd\", \"2000-01-04\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les descriptions du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDescription1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDescription1.put(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htDescription1.put(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDescription2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDescription2.put(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htDescription2.put(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les distributeurs du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDistributeur1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDistributeur1.put(\"distributorAbbreviation\", new SimplePrimitiveNode(\"distributorAbbreviation\", \"distributorAbbreviation1\"));" + 
            System.lineSeparator() + "htDistributeur1.put(\"distributorAffiliation\", new SimplePrimitiveNode(\"distributorAffiliation\", \"distributorAffiliation1\"));" + 
            System.lineSeparator() + "htDistributeur1.put(\"distributorLogoURL\", new SimplePrimitiveNode(\"distributorLogoURL\", \"distributorLogoURL1\"));" + 
            System.lineSeparator() + "htDistributeur1.put(\"distributorName\", new SimplePrimitiveNode(\"distributorName\", \"distributorName1\"));" + 
            System.lineSeparator() + "htDistributeur1.put(\"distributorURL\", new SimplePrimitiveNode(\"distributorURL\", \"distributorURL1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDistributeur2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDistributeur2.put(\"distributorAbbreviation\", new SimplePrimitiveNode(\"distributorAbbreviation\", \"distributorAbbreviation2\"));" + 
            System.lineSeparator() + "htDistributeur2.put(\"distributorAffiliation\", new SimplePrimitiveNode(\"distributorAffiliation\", \"distributorAffiliation2\"));" + 
            System.lineSeparator() + "htDistributeur2.put(\"distributorLogoURL\", new SimplePrimitiveNode(\"distributorLogoURL\", \"distributorLogoURL2\"));" + 
            System.lineSeparator() + "htDistributeur2.put(\"distributorName\", new SimplePrimitiveNode(\"distributorName\", \"distributorName2\"));" + 
            System.lineSeparator() + "htDistributeur2.put(\"distributorURL\", new SimplePrimitiveNode(\"distributorURL\", \"distributorURL2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les grant number du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htGrantNumber1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htGrantNumber1.put(\"grantNumberAgency\", new SimplePrimitiveNode(\"grantNumberAgency\", \"grantAgency1\"));" + 
            System.lineSeparator() + "htGrantNumber1.put(\"grantNumberValue\", new SimplePrimitiveNode(\"grantNumberValue\", \"grantNumber1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htGrantNumber2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htGrantNumber2.put(\"grantNumberAgency\", new SimplePrimitiveNode(\"grantNumberAgency\", \"grantAgency2\"));" + 
            System.lineSeparator() + "htGrantNumber2.put(\"grantNumberValue\", new SimplePrimitiveNode(\"grantNumberValue\", \"grantNumber2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les logiciels du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htLogiciel1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htLogiciel1.put(\"softwareName\", new SimplePrimitiveNode(\"softwareName\", \"softwareName1\"));" + 
            System.lineSeparator() + "htLogiciel1.put(\"softwareVersion\", new SimplePrimitiveNode(\"softwareVersion\", \"softwareVersion1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htLogiciel2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htLogiciel2.put(\"softwareName\", new SimplePrimitiveNode(\"softwareName\", \"softwareName2\"));" + 
            System.lineSeparator() + "htLogiciel2.put(\"softwareVersion\", new SimplePrimitiveNode(\"softwareVersion\", \"softwareVersion2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les mots clés du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htMotCle1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htMotCle1.put(\"keywordTermURI\", new SimplePrimitiveNode(\"keywordTermURI\", \"keywordTermURI1\"));" + 
            System.lineSeparator() + "htMotCle1.put(\"keywordValue\", new SimplePrimitiveNode(\"keywordValue\", \"keywordValue1\"));" + 
            System.lineSeparator() + "htMotCle1.put(\"keywordVocabulary\", new SimplePrimitiveNode(\"keywordVocabulary\", \"keywordVocabulary1\"));" + 
            System.lineSeparator() + "htMotCle1.put(\"keywordVocabularyURI\", new SimplePrimitiveNode(\"keywordVocabularyURI\", \"keywordVocabularyURI1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htMotCle2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htMotCle2.put(\"keywordTermURI\", new SimplePrimitiveNode(\"keywordTermURI\", \"keywordTermURI2\"));" + 
            System.lineSeparator() + "htMotCle2.put(\"keywordValue\", new SimplePrimitiveNode(\"keywordValue\", \"keywordValue2\"));" + 
            System.lineSeparator() + "htMotCle2.put(\"keywordVocabulary\", new SimplePrimitiveNode(\"keywordVocabulary\", \"keywordVocabulary2\"));" + 
            System.lineSeparator() + "htMotCle2.put(\"keywordVocabularyURI\", new SimplePrimitiveNode(\"keywordVocabularyURI\", \"keywordVocabularyURI2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les producteurs du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htProducteur1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htProducteur1.put(\"producerAbbreviation\", new SimplePrimitiveNode(\"producerAbbreviation\", \"producerAbbreviation1\"));" + 
            System.lineSeparator() + "htProducteur1.put(\"producerAffiliation\", new SimplePrimitiveNode(\"producerAffiliation\", \"producerAffiliation1\"));" + 
            System.lineSeparator() + "htProducteur1.put(\"producerLogoURL\", new SimplePrimitiveNode(\"producerLogoURL\", \"producerLogoURL1\"));" + 
            System.lineSeparator() + "htProducteur1.put(\"producerName\", new SimplePrimitiveNode(\"producerName\", \"producerName1\"));" + 
            System.lineSeparator() + "htProducteur1.put(\"producerURL\", new SimplePrimitiveNode(\"producerURL\", \"producerURL1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htProducteur2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htProducteur2.put(\"producerAbbreviation\", new SimplePrimitiveNode(\"producerAbbreviation\", \"producerAbbreviation2\"));" + 
            System.lineSeparator() + "htProducteur2.put(\"producerAffiliation\", new SimplePrimitiveNode(\"producerAffiliation\", \"producerAffiliation2\"));" + 
            System.lineSeparator() + "htProducteur2.put(\"producerLogoURL\", new SimplePrimitiveNode(\"producerLogoURL\", \"producerLogoURL2\"));" + 
            System.lineSeparator() + "htProducteur2.put(\"producerName\", new SimplePrimitiveNode(\"producerName\", \"producerName2\"));" + 
            System.lineSeparator() + "htProducteur2.put(\"producerURL\", new SimplePrimitiveNode(\"producerURL\", \"producerURL2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée la période du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htPeriode1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htPeriode1.put(\"timePeriodCoveredStart\", new SimplePrimitiveNode(\"timePeriodCoveredStart\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htPeriode1.put(\"timePeriodCoveredEnd\", new SimplePrimitiveNode(\"timePeriodCoveredEnd\", \"2000-01-02\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htPeriode2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htPeriode2.put(\"timePeriodCoveredStart\", new SimplePrimitiveNode(\"timePeriodCoveredStart\", \"2000-01-03\"));" + 
            System.lineSeparator() + "htPeriode2.put(\"timePeriodCoveredEnd\", new SimplePrimitiveNode(\"timePeriodCoveredEnd\", \"2000-01-04\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée le projet du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htProjet = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htProjet.put(\"projectAcronym\", new SimplePrimitiveNode(\"projectAcronym\", \"projectAcronym\"));" + 
            System.lineSeparator() + "htProjet.put(\"projectId\", new SimplePrimitiveNode(\"projectId\", \"projectId\"));" + 
            System.lineSeparator() + "htProjet.put(\"projectTask\", new SimplePrimitiveNode(\"projectTask\", \"projectTask\"));" + 
            System.lineSeparator() + "htProjet.put(\"projectTitle\", new SimplePrimitiveNode(\"projectTitle\", \"projectTitle\"));" + 
            System.lineSeparator() + "htProjet.put(\"projectURL\", new SimplePrimitiveNode(\"projectURL\", \"http://project.url\"));" + 
            System.lineSeparator() + "htProjet.put(\"projectWorkPackage\", new SimplePrimitiveNode(\"projectWorkPackage\", \"projectWP\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les producteurs du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htPublication1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htPublication1.put(\"publicationCitation\", new SimplePrimitiveNode(\"publicationCitation\", \"relatedPublicationCitation1\"));" + 
            System.lineSeparator() + "htPublication1.put(\"publicationIDNumber\", new SimplePrimitiveNode(\"publicationIDNumber\", \"relatedPublicationIDNumber1\"));" + 
            System.lineSeparator() + "htPublication1.put(\"publicationIDType\", new SimpleControlledVocabularyNode(\"publicationIDType\", \"ark\"));" + 
            System.lineSeparator() + "htPublication1.put(\"publicationURL\", new SimplePrimitiveNode(\"publicationURL\", \"http://related.publication.url1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htPublication2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htPublication2.put(\"publicationCitation\", new SimplePrimitiveNode(\"publicationCitation\", \"relatedPublicationCitation2\"));" + 
            System.lineSeparator() + "htPublication2.put(\"publicationIDNumber\", new SimplePrimitiveNode(\"publicationIDNumber\", \"relatedPublicationIDNumber2\"));" + 
            System.lineSeparator() + "htPublication2.put(\"publicationIDType\", new SimpleControlledVocabularyNode(\"publicationIDType\", \"arXiv\"));" + 
            System.lineSeparator() + "htPublication2.put(\"publicationURL\", new SimplePrimitiveNode(\"publicationURL\", \"http://related.publication.url2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les relatedDataset du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htRelatedDataset1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htRelatedDataset1.put(\"relatedDatasetCitation\", new SimplePrimitiveNode(\"relatedDatasetCitation\", \"relatedDatasetCitation1\"));" + 
            System.lineSeparator() + "htRelatedDataset1.put(\"relatedDatasetIDNumber\", new SimplePrimitiveNode(\"relatedDatasetIDNumber\", \"relatedDatasetIDNumber1\"));" + 
            System.lineSeparator() + "htRelatedDataset1.put(\"relatedDatasetIDType\", new SimpleControlledVocabularyNode(\"relatedDatasetIDType\", \"ark\"));" + 
            System.lineSeparator() + "htRelatedDataset1.put(\"relatedDatasetURL\", new SimplePrimitiveNode(\"relatedDatasetURL\", \"http://related.dataset.url1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htRelatedDataset2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htRelatedDataset2.put(\"relatedDatasetCitation\", new SimplePrimitiveNode(\"relatedDatasetCitation\", \"relatedDatasetCitation2\"));" + 
            System.lineSeparator() + "htRelatedDataset2.put(\"relatedDatasetIDNumber\", new SimplePrimitiveNode(\"relatedDatasetIDNumber\", \"relatedDatasetIDNumber2\"));" + 
            System.lineSeparator() + "htRelatedDataset2.put(\"relatedDatasetIDType\", new SimpleControlledVocabularyNode(\"relatedDatasetIDType\", \"doi\"));" + 
            System.lineSeparator() + "htRelatedDataset2.put(\"relatedDatasetURL\", new SimplePrimitiveNode(\"relatedDatasetURL\", \"http://related.dataset.url2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée la série du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htSerie = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htSerie.put(\"seriesInformation\", new SimplePrimitiveNode(\"seriesInformation\", \"seriesInformation\"));" + 
            System.lineSeparator() + "htSerie.put(\"seriesName\", new SimplePrimitiveNode(\"seriesName\", \"seriesName\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les topics classifications du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htTopicClassification1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htTopicClassification1.put(\"topicClassValue\", new SimplePrimitiveNode(\"topicClassValue\", \"topicClassValue1\"));" + 
            System.lineSeparator() + "htTopicClassification1.put(\"topicClassVocab\", new SimplePrimitiveNode(\"topicClassVocab\", \"topicClassVocab1\"));" + 
            System.lineSeparator() + "htTopicClassification1.put(\"topicClassVocabURI\", new SimplePrimitiveNode(\"topicClassVocabURI\", \"topicClassVocabURI1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htTopicClassification2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htTopicClassification2.put(\"topicClassValue\", new SimplePrimitiveNode(\"topicClassValue\", \"topicClassValue2\"));" + 
            System.lineSeparator() + "htTopicClassification2.put(\"topicClassVocab\", new SimplePrimitiveNode(\"topicClassVocab\", \"topicClassVocab2\"));" + 
            System.lineSeparator() + "htTopicClassification2.put(\"topicClassVocabURI\", new SimplePrimitiveNode(\"topicClassVocabURI\", \"topicClassVocabURI2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée un node citation" + 
            System.lineSeparator() + "CitationNode cn = new CitationNode();" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"title\", \"title\"));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"subtitle\", \"subtitle\"));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"alternativeTitle\", \"alternativeTitle\"));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"alternativeURL\", \"http://link.to.data\"));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"otherId\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htAutreId1); add(htAutreId2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"datasetContact\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htContact1); add(htContact2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"author\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htAuteur1); add(htAuteur2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"contributor\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htContributeur1); add(htContributeur2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"producer\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htProducteur1); add(htProducteur2); }}));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"productionDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"productionPlace\", \"productionPlace\"));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"distributor\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDistributeur1); add(htDistributeur2); }}));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"distributionDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"dsDescription\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDescription1); add(htDescription2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleControlledVocabularyNode(\"language\", new ArrayList<String>() {{ add(\"Abkhaz\"); add(\"Afar\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleControlledVocabularyNode(\"subject\", new ArrayList<String>() {{ add(\"Animal Breeding and Animal Products\"); add(\"Animal Health and Pathology\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"keyword\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htMotCle1); add(htMotCle1); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"topicClassification\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htTopicClassification1); add(htTopicClassification2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleControlledVocabularyNode(\"kindOfData\", new ArrayList<String>() {{ add(\"Audiovisual\"); add(\"Collection\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultiplePrimitiveNode(\"kindOfDataOther\", new ArrayList<String>() {{ add(\"otherKindOfData1\"); add(\"otherKindOfData2\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleControlledVocabularyNode(\"dataOrigin\", new ArrayList<String>() {{ add(\"observational data\"); add(\"experimental data\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultiplePrimitiveNode(\"dataSources\", new ArrayList<String>() {{ add(\"dataSource1\"); add(\"dataSource2\"); }}));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"originOfSources\", \"originOfSource\"));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"characteristicOfSources\", \"characteristicOfSource\"));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"accessToSources\", \"accessToSource\"));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"software\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htLogiciel1); add(htLogiciel2); }}));" + 
            System.lineSeparator() + "cn.addField(new SimpleCompoundNode(\"series\", htSerie));" + 
            System.lineSeparator() + "cn.addField(new MultipleControlledVocabularyNode(\"lifeCycleStep\", new ArrayList<String>() {{ add(\"Study proposal\"); add(\"Funding\"); }}));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"notesText\", \"notes\"));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"publication\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htPublication1); add(htPublication2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultiplePrimitiveNode(\"relatedMaterial\", new ArrayList<String>() {{ add(\"relatedMaterial1\"); add(\"relatedMaterial2\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"relatedDataset\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htRelatedDataset1); add(htRelatedDataset2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultiplePrimitiveNode(\"otherReferences\", new ArrayList<String>() {{ add(\"otherReference1\"); add(\"otherReference2\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"grantNumber\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htGrantNumber1); add(htGrantNumber2); }}));" + 
            System.lineSeparator() + "cn.addField(new SimpleCompoundNode(\"project\", htProjet));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"timePeriodCovered\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htPeriode1); add(htPeriode2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"dateOfCollection\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDates1); add(htDates2); }}));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"depositor\", \"depositor\"));" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"dateOfDeposit\", \"2000-01-01\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "System.out.print(cn.toJSON());"
        );
    }

    /*  <c>HowToBuildADerivedTextNode</c> montre la manière de construire un <c>DerivedTextNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      // je crée les sources du dataset
     *      Hashtable<String,BaseNode> htSource1 = new Hashtable<String,BaseNode>();
     *      htSource1.put("ageOfSource", new MultiplePrimitiveNode("ageOfSource", new ArrayList<String>() {{ add("sourceAge1"); }}));
     *      htSource1.put("citations", new MultiplePrimitiveNode("citations", new ArrayList<String>() {{ add("1"); }}));
     *      htSource1.put("experimentNumber", new MultiplePrimitiveNode("experimentNumber", new ArrayList<String>() {{ add("1"); }}));
     *      htSource1.put("typeOfSource", new MultiplePrimitiveNode("typeOfSource", new ArrayList<String>() {{ add("sourceType1"); }}));
     *      
     *      Hashtable<String,BaseNode> htSource2 = new Hashtable<String,BaseNode>();
     *      htSource2.put("ageOfSource", new MultiplePrimitiveNode("ageOfSource", new ArrayList<String>() {{ add("sourceAge2"); }}));
     *      htSource2.put("citations", new MultiplePrimitiveNode("citations", new ArrayList<String>() {{ add("2"); }}));
     *      htSource2.put("experimentNumber", new MultiplePrimitiveNode("experimentNumber", new ArrayList<String>() {{ add("2"); }}));
     *      htSource2.put("typeOfSource", new MultiplePrimitiveNode("typeOfSource", new ArrayList<String>() {{ add("sourceType2"); }}));
     *      
     *      // je crée un node derived-text
     *      DerivedTextNode dtn = new DerivedTextNode();
     *      dtn.addField(new MultipleCompoundNode("source", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htSource1); add(htSource2); }}));
     *      
     *      System.out.print(dtn.toJSON());
     *  </pre>
     */
    public static void HowToBuildADerivedTextNode()
    {
        System.out.print
        (
            System.lineSeparator() + "// je crée les sources du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htSource1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htSource1.put(\"ageOfSource\", new MultiplePrimitiveNode(\"ageOfSource\", new ArrayList<String>() {{ add(\"sourceAge1\"); }}));" + 
            System.lineSeparator() + "htSource1.put(\"citations\", new MultiplePrimitiveNode(\"citations\", new ArrayList<String>() {{ add(\"1\"); }}));" + 
            System.lineSeparator() + "htSource1.put(\"experimentNumber\", new MultiplePrimitiveNode(\"experimentNumber\", new ArrayList<String>() {{ add(\"1\"); }}));" + 
            System.lineSeparator() + "htSource1.put(\"typeOfSource\", new MultiplePrimitiveNode(\"typeOfSource\", new ArrayList<String>() {{ add(\"sourceType1\"); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htSource2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htSource2.put(\"ageOfSource\", new MultiplePrimitiveNode(\"ageOfSource\", new ArrayList<String>() {{ add(\"sourceAge2\"); }}));" + 
            System.lineSeparator() + "htSource2.put(\"citations\", new MultiplePrimitiveNode(\"citations\", new ArrayList<String>() {{ add(\"2\"); }}));" + 
            System.lineSeparator() + "htSource2.put(\"experimentNumber\", new MultiplePrimitiveNode(\"experimentNumber\", new ArrayList<String>() {{ add(\"2\"); }}));" + 
            System.lineSeparator() + "htSource2.put(\"typeOfSource\", new MultiplePrimitiveNode(\"typeOfSource\", new ArrayList<String>() {{ add(\"sourceType2\"); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée un node derived-text" + 
            System.lineSeparator() + "DerivedTextNode dtn = new DerivedTextNode();" + 
            System.lineSeparator() + "dtn.addField(new MultipleCompoundNode(\"source\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htSource1); add(htSource2); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "System.out.print(dtn.toJSON());" 
        );
    }

    /** <c>HowToBuildAGeospatialNode</c> montre la manière de construire un <c>GeospatialNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      // je crée la conformite du dataset
     *      Hashtable<String,BaseNode> htConformite1 = new Hashtable<String,BaseNode>();
     *      htConformite1.put("specification", new MultiplePrimitiveNode("specification", new ArrayList<String>() {{ add("conformitySpecification1"); }}));
     *      htConformite1.put("degree", new SimpleControlledVocabularyNode("degree", "Conformant"));
     *      
     *      Hashtable<String,BaseNode> htConformite2 = new Hashtable<String,BaseNode>();
     *      htConformite2.put("specification", new MultiplePrimitiveNode("specification", new ArrayList<String>() {{ add("conformitySpecification2"); }}));
     *      htConformite2.put("degree", new SimpleControlledVocabularyNode("degree", "Not Conformant"));
     *      
     *      // je crée la boite geographique du dataset
     *      Hashtable<String,BaseNode> htBoite1 = new Hashtable<String,BaseNode>();
     *      htBoite1.put("eastLongitude", new SimplePrimitiveNode("eastLongitude", "0"));
     *      htBoite1.put("northLongitude", new SimplePrimitiveNode("northLongitude", "1"));
     *      htBoite1.put("southLongitude", new SimplePrimitiveNode("southLongitude", "2"));
     *      htBoite1.put("westLongitude", new SimplePrimitiveNode("westLongitude", "3"));
     *      
     *      Hashtable<String,BaseNode> htBoite2 = new Hashtable<String,BaseNode>();
     *      htBoite2.put("eastLongitude", new SimplePrimitiveNode("eastLongitude", "4"));
     *      htBoite2.put("northLongitude", new SimplePrimitiveNode("northLongitude", "5"));
     *      htBoite2.put("southLongitude", new SimplePrimitiveNode("southLongitude", "6"));
     *      htBoite2.put("westLongitude", new SimplePrimitiveNode("westLongitude", "7"));
     *      
     *      // je crée la couverture geographique du dataset
     *      Hashtable<String,BaseNode> htCouverture1 = new Hashtable<String,BaseNode>();
     *      htCouverture1.put("city", new SimplePrimitiveNode("city", "geographicCoverageCity1"));
     *      htCouverture1.put("country", new SimpleControlledVocabularyNode("country", "Afghanistan"));
     *      htCouverture1.put("otherGeographicCoverage", new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther1"));
     *      htCouverture1.put("state", new SimplePrimitiveNode("state", "geographicCoverageProvince1"));
     *      
     *      Hashtable<String,BaseNode> htCouverture2 = new Hashtable<String,BaseNode>();
     *      htCouverture2.put("city", new SimplePrimitiveNode("city", "geographicCoverageCity2"));
     *      htCouverture2.put("country", new SimpleControlledVocabularyNode("country", "Albania"));
     *      htCouverture2.put("otherGeographicCoverage", new SimplePrimitiveNode("otherGeographicCoverage", "geographicCoverageOther2"));
     *      htCouverture2.put("state", new SimplePrimitiveNode("state", "geographicCoverageProvince2"));
     *      
     *      // je crée la qualité du dataset
     *      Hashtable<String,BaseNode> htQualite1 = new Hashtable<String,BaseNode>();
     *      htQualite1.put("lineage", new MultiplePrimitiveNode("lineage", new ArrayList<String>() {{ add("QualityAndValidityLineage1"); }}));
     *      htQualite1.put("spatialResolution", new MultiplePrimitiveNode("lineage", new ArrayList<String>() {{ add("QualityAndValiditySpatialResolution1"); }}));
     *      
     *      Hashtable<String,BaseNode> htQualite2 = new Hashtable<String,BaseNode>();
     *      htQualite2.put("lineage", new MultiplePrimitiveNode("lineage", new ArrayList<String>() {{ add("QualityAndValidityLineage2"); }}));
     *      htQualite2.put("spatialResolution", new MultiplePrimitiveNode("lineage", new ArrayList<String>() {{ add("QualityAndValiditySpatialResolution2"); }}));
     *      
     *      
     *      // je crée un node geospatial
     *      GeospatialNode gn = new GeospatialNode();
     *      gn.addField(new MultipleCompoundNode("geographicCoverage", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htCouverture1); add(htCouverture2); }}));
     *      gn.addField(new MultiplePrimitiveNode("geographicUnit", new ArrayList<String>() {{ add("geographicUnit1"); add("geographicUnit2"); }} ));
     *      gn.addField(new MultipleCompoundNode("geographicBoundingBox", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htBoite1); add(htBoite2); }}));
     *      gn.addField(new MultipleCompoundNode("qualityValidity", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htQualite1); add(htQualite2);  }}));
     *      gn.addField(new MultipleCompoundNode("conformity", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htConformite1); add(htConformite2); }}));
     *      
     *      System.out.print(gn.toJSON());
     *  </pre>
     */
    public static void HowToBuildAGeospatialNode()
    {
        System.out.print
        (
            System.lineSeparator() + "Hashtable<String,BaseNode> htConformite1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htConformite1.put(\"specification\", new MultiplePrimitiveNode(\"specification\", new ArrayList<String>() {{ add(\"conformitySpecification1\"); }}));" + 
            System.lineSeparator() + "htConformite1.put(\"degree\", new SimpleControlledVocabularyNode(\"degree\", \"Conformant\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htConformite2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htConformite2.put(\"specification\", new MultiplePrimitiveNode(\"specification\", new ArrayList<String>() {{ add(\"conformitySpecification2\"); }}));" + 
            System.lineSeparator() + "htConformite2.put(\"degree\", new SimpleControlledVocabularyNode(\"degree\", \"Not Conformant\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée la boite geographique du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htBoite1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htBoite1.put(\"eastLongitude\", new SimplePrimitiveNode(\"eastLongitude\", \"0\"));" + 
            System.lineSeparator() + "htBoite1.put(\"northLongitude\", new SimplePrimitiveNode(\"northLongitude\", \"1\"));" + 
            System.lineSeparator() + "htBoite1.put(\"southLongitude\", new SimplePrimitiveNode(\"southLongitude\", \"2\"));" + 
            System.lineSeparator() + "htBoite1.put(\"westLongitude\", new SimplePrimitiveNode(\"westLongitude\", \"3\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htBoite2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htBoite2.put(\"eastLongitude\", new SimplePrimitiveNode(\"eastLongitude\", \"4\"));" + 
            System.lineSeparator() + "htBoite2.put(\"northLongitude\", new SimplePrimitiveNode(\"northLongitude\", \"5\"));" + 
            System.lineSeparator() + "htBoite2.put(\"southLongitude\", new SimplePrimitiveNode(\"southLongitude\", \"6\"));" + 
            System.lineSeparator() + "htBoite2.put(\"westLongitude\", new SimplePrimitiveNode(\"westLongitude\", \"7\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée la couverture geographique du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htCouverture1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htCouverture1.put(\"city\", new SimplePrimitiveNode(\"city\", \"geographicCoverageCity1\"));" + 
            System.lineSeparator() + "htCouverture1.put(\"country\", new SimpleControlledVocabularyNode(\"country\", \"Afghanistan\"));" + 
            System.lineSeparator() + "htCouverture1.put(\"otherGeographicCoverage\", new SimplePrimitiveNode(\"otherGeographicCoverage\", \"geographicCoverageOther1\"));" + 
            System.lineSeparator() + "htCouverture1.put(\"state\", new SimplePrimitiveNode(\"state\", \"geographicCoverageProvince1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htCouverture2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htCouverture2.put(\"city\", new SimplePrimitiveNode(\"city\", \"geographicCoverageCity2\"));" + 
            System.lineSeparator() + "htCouverture2.put(\"country\", new SimpleControlledVocabularyNode(\"country\", \"Albania\"));" + 
            System.lineSeparator() + "htCouverture2.put(\"otherGeographicCoverage\", new SimplePrimitiveNode(\"otherGeographicCoverage\", \"geographicCoverageOther2\"));" + 
            System.lineSeparator() + "htCouverture2.put(\"state\", new SimplePrimitiveNode(\"state\", \"geographicCoverageProvince2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée la qualité du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htQualite1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htQualite1.put(\"lineage\", new MultiplePrimitiveNode(\"lineage\", new ArrayList<String>() {{ add(\"QualityAndValidityLineage1\"); }}));" + 
            System.lineSeparator() + "htQualite1.put(\"spatialResolution\", new MultiplePrimitiveNode(\"lineage\", new ArrayList<String>() {{ add(\"QualityAndValiditySpatialResolution1\"); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htQualite2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htQualite2.put(\"lineage\", new MultiplePrimitiveNode(\"lineage\", new ArrayList<String>() {{ add(\"QualityAndValidityLineage2\"); }}));" + 
            System.lineSeparator() + "htQualite2.put(\"spatialResolution\", new MultiplePrimitiveNode(\"lineage\", new ArrayList<String>() {{ add(\"QualityAndValiditySpatialResolution2\"); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée un node geospatial" + 
            System.lineSeparator() + "GeospatialNode gn = new GeospatialNode();" + 
            System.lineSeparator() + "gn.addField(new MultipleCompoundNode(\"geographicCoverage\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htCouverture1); add(htCouverture2); }}));" + 
            System.lineSeparator() + "gn.addField(new MultiplePrimitiveNode(\"geographicUnit\", new ArrayList<String>() {{ add(\"geographicUnit1\"); add(\"geographicUnit2\"); }} ));" + 
            System.lineSeparator() + "gn.addField(new MultipleCompoundNode(\"geographicBoundingBox\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htBoite1); add(htBoite2); }}));" + 
            System.lineSeparator() + "gn.addField(new MultipleCompoundNode(\"qualityValidity\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htQualite1); add(htQualite2);  }}));" + 
            System.lineSeparator() + "gn.addField(new MultipleCompoundNode(\"conformity\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htConformite1); add(htConformite2); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "System.out.print(gn.toJSON());"
        );
    }

    /**  <c>HowToBuildAJournalNode</c> montre la manière de construire un <c>JournalNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      // je crée les support de publication du dataset
     *      Hashtable<String,BaseNode> htJournalVolumeIssue1 = new Hashtable<String,BaseNode>();
     *      htJournalVolumeIssue1.put("journalIssue", new SimplePrimitiveNode("journalIssue", "journalIssue1"));
     *      htJournalVolumeIssue1.put("journalPubDate", new SimplePrimitiveNode("journalPubDate", "2000-01-01"));
     *      htJournalVolumeIssue1.put("journalVolume", new SimplePrimitiveNode("journalVolume", "journalVolume1"));
     *     
     *      Hashtable<String,BaseNode> htJournalVolumeIssue2 = new Hashtable<String,BaseNode>();
     *      htJournalVolumeIssue2.put("journalIssue", new SimplePrimitiveNode("journalIssue", "journalIssue2"));
     *      htJournalVolumeIssue2.put("journalPubDate", new SimplePrimitiveNode("journalPubDate", "2000-01-02"));
     *      htJournalVolumeIssue2.put("journalVolume", new SimplePrimitiveNode("journalVolume", "journalVolume2"));
     *     
     *      // je crée un node journal
     *      JournalNode jn = new JournalNode();
     *      jn.addField(new MultipleCompoundNode("journalVolumeIssue", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htJournalVolumeIssue1); add(htJournalVolumeIssue2); }}));
     *      jn.addField(new SimpleControlledVocabularyNode("journalArticleType", "abstract"));
     *     
     *      System.out.print(jn.toJSON());
     *  </pre>
     */
    public static void HowToBuildAJournalNode()
    {
        System.out.print
        (
            System.lineSeparator() + "// je crée les support de publication du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htJournalVolumeIssue1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htJournalVolumeIssue1.put(\"journalIssue\", new SimplePrimitiveNode(\"journalIssue\", \"journalIssue1\"));" + 
            System.lineSeparator() + "htJournalVolumeIssue1.put(\"journalPubDate\", new SimplePrimitiveNode(\"journalPubDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htJournalVolumeIssue1.put(\"journalVolume\", new SimplePrimitiveNode(\"journalVolume\", \"journalVolume1\"));" + 
            System.lineSeparator() +  
            System.lineSeparator() + "Hashtable<String,BaseNode> htJournalVolumeIssue2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htJournalVolumeIssue2.put(\"journalIssue\", new SimplePrimitiveNode(\"journalIssue\", \"journalIssue2\"));" + 
            System.lineSeparator() + "htJournalVolumeIssue2.put(\"journalPubDate\", new SimplePrimitiveNode(\"journalPubDate\", \"2000-01-02\"));" + 
            System.lineSeparator() + "htJournalVolumeIssue2.put(\"journalVolume\", new SimplePrimitiveNode(\"journalVolume\", \"journalVolume2\"));" + 
            System.lineSeparator() +  
            System.lineSeparator() + "// je crée un node journal" + 
            System.lineSeparator() + "JournalNode jn = new JournalNode();" + 
            System.lineSeparator() + "jn.addField(new MultipleCompoundNode(\"journalVolumeIssue\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htJournalVolumeIssue1); add(htJournalVolumeIssue2); }}));" + 
            System.lineSeparator() + "jn.addField(new SimpleControlledVocabularyNode(\"journalArticleType\", \"abstract\"));" + 
            System.lineSeparator() +  
            System.lineSeparator() + "System.out.print(jn.toJSON());"
        );
    }

    /** <c>HowToBuildASimplePrimitiveNode</c> montre la manière de construire un <c>SimplePrimitiveNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      BiomedicalNode bn =     new BiomedicalNode();           // ... puis configurer bn  comme ici : @see HowToBuildABiomedicalNode
     *      CitationNode cn =       new CitationNode();             // ... puis configurer cn  comme ici : @see HowToBuildACitationNode
     *      DerivedTextNode dtn =   new DerivedTextNode();          // ... puis configurer dtn comme ici : @see HowToBuildADerivedTextNode
     *      GeospatialNode gn =     new GeospatialNode();           // ... puis configurer gn  comme ici : @see HowToBuildAGeospatialNode
     *      JournalNode jn =        new JournalNode();              // ... puis configurer jn  comme ici : @see HowToBuildAJournalNode
     *      SemanticsNode sn =      new SemanticsNode();            // ... puis configurer sn  comme ici : @see HowToBuildASemanticsNode
     *      SocialScienceNode ssn = new SocialScienceNode();        // ... puis configurer ssn comme ici : @see HowToBuildASocialScienceNode
     *      
     *      MetadataDocument md = new MetadataDocument();
     *      md.AddCitationNode(cn);
     *      md.AddGeospatialNode(gn);
     *      md.AddSocialScienceNode(ssn);
     *      md.AddBiomedicalNode(bn);
     *      md.AddJournalNode(jn);
     *      md.AddDerivedTextNode(dtn);
     *      md.AddSemanticsNode(sn);
     *      
     *      System.out.print(md.ToJSON());
     *  </pre>
     */
    public static void HowToBuildAMetadataDocument()
    {
        System.out.print
        (
            System.lineSeparator() + "BiomedicalNode bn =     new BiomedicalNode();           // ... puis configurer bn  comme ici : @see HowToBuildABiomedicalNode" +
            System.lineSeparator() + "CitationNode cn =       new CitationNode();             // ... puis configurer cn  comme ici : @see HowToBuildACitationNode" +
            System.lineSeparator() + "DerivedTextNode dtn =   new DerivedTextNode();          // ... puis configurer dtn comme ici : @see HowToBuildADerivedTextNode" +
            System.lineSeparator() + "GeospatialNode gn =     new GeospatialNode();           // ... puis configurer gn  comme ici : @see HowToBuildAGeospatialNode" +
            System.lineSeparator() + "JournalNode jn =        new JournalNode();              // ... puis configurer jn  comme ici : @see HowToBuildAJournalNode" +
            System.lineSeparator() + "SemanticsNode sn =      new SemanticsNode();            // ... puis configurer sn  comme ici : @see HowToBuildASemanticsNode" +
            System.lineSeparator() + "SocialScienceNode ssn = new SocialScienceNode();        // ... puis configurer ssn comme ici : @see HowToBuildASocialScienceNode" +
            System.lineSeparator() +
            System.lineSeparator() + "MetadataDocument md = new MetadataDocument();" +
            System.lineSeparator() + "md.AddCitationNode(cn);" +
            System.lineSeparator() + "md.AddGeospatialNode(gn);" +
            System.lineSeparator() + "md.AddSocialScienceNode(ssn);" +
            System.lineSeparator() + "md.AddBiomedicalNode(bn);" +
            System.lineSeparator() + "md.AddJournalNode(jn);" +
            System.lineSeparator() + "md.AddDerivedTextNode(dtn);" +
            System.lineSeparator() + "md.AddSemanticsNode(sn);" +
            System.lineSeparator() +
            System.lineSeparator() + "System.out.print(md.ToJSON());"
        );
    }

    /** <c>HowToBuildAMinimalCitationNode</c> montre la manière de construire un <c>CitationNode</c> minimal
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      // je crée les auteurs du dataset
     *      Hashtable<String,BaseNode> htAuteur1 = new Hashtable<String,BaseNode>();
     *      htAuteur1.put("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation1"));
     *      htAuteur1.put("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier1"));
     *      htAuteur1.put("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"));
     *      htAuteur1.put("authorName", new SimplePrimitiveNode("authorName", "authorName1"));
     *      
     *      Hashtable<String,BaseNode> htAuteur2 = new Hashtable<String,BaseNode>();
     *      htAuteur2.put("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "authorAffiliation2"));
     *      htAuteur2.put("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "authorIdentifier2"));
     *      htAuteur2.put("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "idHAL"));
     *      htAuteur2.put("authorName", new SimplePrimitiveNode("authorName", "authorName2"));
     *      
     *      // je crée les contacts du dataset
     *      Hashtable<String,BaseNode> htContact1 = new Hashtable<String,BaseNode>();
     *      htContact1.put("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation1"));
     *      htContact1.put("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact1@mail.com"));
     *      htContact1.put("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName1"));
     *          
     *      Hashtable<String,BaseNode> htContact2 = new Hashtable<String,BaseNode>();
     *      htContact2.put("datasetContactAffiliation", new SimplePrimitiveNode("datasetContactAffiliation", "contactAffiliation2"));
     *      htContact2.put("datasetContactEmail", new SimplePrimitiveNode("datasetContactEmail", "contact2@mail.com"));
     *      htContact2.put("datasetContactName", new SimplePrimitiveNode("datasetContactName", "contactName2"));
     *      
     *      // je crée les descriptions du dataset
     *      Hashtable<String,BaseNode> htDescription1 = new Hashtable<String,BaseNode>();
     *      htDescription1.put("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
     *      htDescription1.put("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText1"));
     *      
     *      Hashtable<String,BaseNode> htDescription2 = new Hashtable<String,BaseNode>();
     *      htDescription2.put("dsDescriptionDate", new SimplePrimitiveNode("dsDescriptionDate", "2000-01-01"));
     *      htDescription2.put("dsDescriptionValue", new SimplePrimitiveNode("dsDescriptionValue", "descriptionText2"));
     *          
     *      // je crée un node citation
     *      CitationNode cn = new CitationNode();
     *      cn.addField(new SimplePrimitiveNode("title", "title"));
     *      cn.addField(new MultipleCompoundNode("datasetContact", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htContact1); add(htContact2); }}));
     *      cn.addField(new MultipleCompoundNode("author", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htAuteur1); add(htAuteur2); }}));
     *      cn.addField(new MultipleCompoundNode("dsDescription", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDescription1); add(htDescription2); }}));
     *      cn.addField(new MultipleControlledVocabularyNode("subject", new ArrayList<String>() {{ add("Animal Breeding and Animal Products"); add("Animal Health and Pathology"); }}));
     *      cn.addField(new MultipleControlledVocabularyNode("kindOfData", new ArrayList<String>() {{ add("Audiovisual"); add("Collection"); }}));
     *      
     *      System.out.print(cn.toJSON());
     *  </pre>
     */ 
    public static void HowToBuildAMinimalCitationNode()
    {
        System.out.print
        (
            System.lineSeparator() + "// je crée les auteurs du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htAuteur1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htAuteur1.put(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation1\"));" + 
            System.lineSeparator() + "htAuteur1.put(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier1\"));" + 
            System.lineSeparator() + "htAuteur1.put(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"ORCID\"));" + 
            System.lineSeparator() + "htAuteur1.put(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htAuteur2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htAuteur2.put(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"authorAffiliation2\"));" + 
            System.lineSeparator() + "htAuteur2.put(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"authorIdentifier2\"));" + 
            System.lineSeparator() + "htAuteur2.put(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"idHAL\"));" + 
            System.lineSeparator() + "htAuteur2.put(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"authorName2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les contacts du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htContact1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htContact1.put(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation1\"));" + 
            System.lineSeparator() + "htContact1.put(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact1@mail.com\"));" + 
            System.lineSeparator() + "htContact1.put(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htContact2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htContact2.put(\"datasetContactAffiliation\", new SimplePrimitiveNode(\"datasetContactAffiliation\", \"contactAffiliation2\"));" + 
            System.lineSeparator() + "htContact2.put(\"datasetContactEmail\", new SimplePrimitiveNode(\"datasetContactEmail\", \"contact2@mail.com\"));" + 
            System.lineSeparator() + "htContact2.put(\"datasetContactName\", new SimplePrimitiveNode(\"datasetContactName\", \"contactName2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée les descriptions du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDescription1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDescription1.put(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htDescription1.put(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htDescription2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htDescription2.put(\"dsDescriptionDate\", new SimplePrimitiveNode(\"dsDescriptionDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htDescription2.put(\"dsDescriptionValue\", new SimplePrimitiveNode(\"dsDescriptionValue\", \"descriptionText2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée un node citation" + 
            System.lineSeparator() + "CitationNode cn = new CitationNode();" + 
            System.lineSeparator() + "cn.addField(new SimplePrimitiveNode(\"title\", \"title\"));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"datasetContact\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htContact1); add(htContact2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"author\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htAuteur1); add(htAuteur2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleCompoundNode(\"dsDescription\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htDescription1); add(htDescription2); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleControlledVocabularyNode(\"subject\", new ArrayList<String>() {{ add(\"Animal Breeding and Animal Products\"); add(\"Animal Health and Pathology\"); }}));" + 
            System.lineSeparator() + "cn.addField(new MultipleControlledVocabularyNode(\"kindOfData\", new ArrayList<String>() {{ add(\"Audiovisual\"); add(\"Collection\"); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "System.out.print(cn.toJSON());"
        );
    }

    /** <c>HowToBuildAMultipleCompoundNode</c> montre la manière de construire un <c>MultipleCompoundNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      Hashtable<String,BaseNode> auteur1 = new Hashtable<String,BaseNode>()
     *      {{
     *          put("authorName", new SimplePrimitiveNode("authorName", "Heirman, Thierry"));
     *          put("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "INRAE"));
     *          put("authorIdentifier", new SimplePrimitiveNode("authorIdentifier", "https://orcid.org/0000-0002-3466-1099"));
     *          put("authorIdentifierScheme", new SimpleControlledVocabularyNode("authorIdentifierScheme", "ORCID"));
     *      }};
     *
     *      Hashtable<String,BaseNode> auteur2 = new Hashtable<String,BaseNode>()
     *      {{
     *          put("authorName", new SimplePrimitiveNode("authorName", "Pourfairelavaisselle, Vladimir"));
     *          put("authorAffiliation", new SimplePrimitiveNode("authorAffiliation", "INRAE"));
     *      }};
     * 
     *      MultipleCompoundNode mcn = new MultipleCompoundNode("author", new ArrayList<Hashtable<String, BaseNode>>() {{ add(auteur1); add(auteur2); }}); 
     *      System.out.print(mcn.ToJSON());
     *  </pre>
     */
    public static void HowToBuildAMultipleCompoundNode()
    {
        System.out.print(
            System.lineSeparator() + "Hashtable<String,BaseNode> auteur1 = new Hashtable<String,BaseNode>()" + 
            System.lineSeparator() + "{{" + 
            System.lineSeparator() + "    put(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"Heirman, Thierry\"));" + 
            System.lineSeparator() + "    put(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"INRAE\"));" + 
            System.lineSeparator() + "    put(\"authorIdentifier\", new SimplePrimitiveNode(\"authorIdentifier\", \"https://orcid.org/0000-0002-3466-1099\"));" + 
            System.lineSeparator() + "    put(\"authorIdentifierScheme\", new SimpleControlledVocabularyNode(\"authorIdentifierScheme\", \"ORCID\"));" + 
            System.lineSeparator() + "}};" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> auteur2 = new Hashtable<String,BaseNode>()" + 
            System.lineSeparator() + "{{" + 
            System.lineSeparator() + "    put(\"authorName\", new SimplePrimitiveNode(\"authorName\", \"Pourfairelavaisselle, Vladimir\"));" + 
            System.lineSeparator() + "    put(\"authorAffiliation\", new SimplePrimitiveNode(\"authorAffiliation\", \"INRAE\"));" + 
            System.lineSeparator() + "}};" + 
            System.lineSeparator() + 
            System.lineSeparator() + "MultipleCompoundNode mcn = new MultipleCompoundNode(\"author\", new ArrayList<Hashtable<String, BaseNode>>() {{ add(auteur1); add(auteur2); }}); " + 
            System.lineSeparator() + "System.out.print(mcn.ToJSON());"
        );
    }

    /** <c>HowToBuildAMultipleControlledVocabularyNode</c> montre la manière de construire un <c>MultipleControlledVocabularyNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *          List<String> values = new ArrayList<String>();
     *          values.add("French");
     *          values.add("English");
     *          values.add("Occitan");
     * 
     *          MultipleControlledVocabularyNode mcvn = new MultipleControlledVocabularyNode("language", values);
     *          System.out.print(mcvn.ToJSON());
     *  </pre>
     */  
    public static void HowToBuildAMultipleControlledVocabularyNode()
    {
        System.out.print
        (
            System.lineSeparator() + "List<String> values = new ArrayList<String>();" +
            System.lineSeparator() + "values.add(\"French\");" +
            System.lineSeparator() + "values.add(\"English\");" +
            System.lineSeparator() + "values.add(\"Occitan\");" +
            System.lineSeparator() +
            System.lineSeparator() + "MultipleControlledVocabularyNode mcvn = new MultipleControlledVocabularyNode(\"language\", values);" +
            System.lineSeparator() + "System.out.print(mcvn.ToJSON());"
        );
    }

    /** <c>HowToBuildAMultiplePrimitiveNode</c> montre la manière de construire un <c>MultiplePrimitiveNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *          List<String> values = new ArrayList<String>();
     *          values.add("dataSource1");
     *          values.add("dataSource2");
     *          values.add("dataSource3");
     *  
     *          MultiplePrimitiveNode mpn = new MultiplePrimitiveNode("dataSources", values);
     *          System.out.print(mpn.ToJSON());
     *  </pre>
     */
    public static void HowToBuildAMultiplePrimitiveNode()
    {
        System.out.print
        (
            System.lineSeparator() + "List<String> values = new ArrayList<String>();" +
            System.lineSeparator() + "values.add(\"dataSource1\");" +
            System.lineSeparator() + "values.add(\"dataSource2\");" +
            System.lineSeparator() + "values.add(\"dataSource3\");" +
            System.lineSeparator() + 
            System.lineSeparator() + "MultiplePrimitiveNode mpn = new MultiplePrimitiveNode(\"dataSources\", values);" +
            System.lineSeparator() + "System.out.print(mpn.ToJSON();"
        );
    }

    /**  <c>HowToBuildASemanticsNode</c> montre la manière de construire un <c>SemanticsNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      // je crée la version de ressource du dataset
     *      Hashtable<String,BaseNode> htResourceVersion = new Hashtable<String,BaseNode>();
     *      htResourceVersion.put("versionStatus", new MultipleControlledVocabularyNode("versionStatus", new ArrayList<String>() {{ add("alpha"); add("beta"); }}));
     *      htResourceVersion.put("versionInfo", new SimplePrimitiveNode("versionInfo", "version"));
     *      htResourceVersion.put("priorVersion", new MultiplePrimitiveNode("priorVersion", new ArrayList<String>() {{ add("http://prior.version"); }}));
     *      htResourceVersion.put("modificationDate", new SimplePrimitiveNode("modificationDate", "2000-01-01"));
     *      htResourceVersion.put("changes", new SimplePrimitiveNode("changes", "http://semantic.version.changes"));
     *      
     *      // je crée un node semantics
     *      SemanticsNode sn = new SemanticsNode();
     *      sn.addField(new SimpleControlledVocabularyNode("hasFormalityLevel", "Classification scheme"));
     *      sn.addField(new SimpleControlledVocabularyNode("hasOntologyLanguage", "JSON-LD"));
     *      sn.addField(new SimpleControlledVocabularyNode("typeOfSR", "Application Ontology"));
     *      sn.addField(new MultipleControlledVocabularyNode("designedForOntologyTask", new ArrayList<String>() {{ add("Annotation Task"); add("Configuration Task"); }}));
     *      sn.addField(new SimplePrimitiveNode("knownUsage", "knownUsage"));
     *      sn.addField(new SimpleCompoundNode("resourceVersion", htResourceVersion));
     *      sn.addField(new MultiplePrimitiveNode("imports", new ArrayList<String>() {{ add("http://semantic.imports1"); add("http://semantic.imports2"); }}));
     *      sn.addField(new SimplePrimitiveNode("bugDatabase", "http://semantic.bug.tracker"));
     *      sn.addField(new SimplePrimitiveNode("endpoint", "http://semantic.sparql.endpoint"));
     *      sn.addField(new MultiplePrimitiveNode("imports", new ArrayList<String>() {{ add("http://semantic.uri1"); add("http://semantic.uri2"); }}));
     *      
     *      System.out.print(sn.toJSON());
     *  </pre>
     */
    public static void HowToBuildASemanticsNode()
    {
        System.out.print
        (
            System.lineSeparator() + "// je crée la version de ressource du dataset" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htResourceVersion = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htResourceVersion.put(\"versionStatus\", new MultipleControlledVocabularyNode(\"versionStatus\", new ArrayList<String>() {{ add(\"alpha\"); add(\"beta\"); }}));" + 
            System.lineSeparator() + "htResourceVersion.put(\"versionInfo\", new SimplePrimitiveNode(\"versionInfo\", \"version\"));" + 
            System.lineSeparator() + "htResourceVersion.put(\"priorVersion\", new MultiplePrimitiveNode(\"priorVersion\", new ArrayList<String>() {{ add(\"http://prior.version\"); }}));" + 
            System.lineSeparator() + "htResourceVersion.put(\"modificationDate\", new SimplePrimitiveNode(\"modificationDate\", \"2000-01-01\"));" + 
            System.lineSeparator() + "htResourceVersion.put(\"changes\", new SimplePrimitiveNode(\"changes\", \"http://semantic.version.changes\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée un node semantics" + 
            System.lineSeparator() + "SemanticsNode sn = new SemanticsNode();" + 
            System.lineSeparator() + "sn.addField(new SimpleControlledVocabularyNode(\"hasFormalityLevel\", \"Classification scheme\"));" + 
            System.lineSeparator() + "sn.addField(new SimpleControlledVocabularyNode(\"hasOntologyLanguage\", \"JSON-LD\"));" + 
            System.lineSeparator() + "sn.addField(new SimpleControlledVocabularyNode(\"typeOfSR\", \"Application Ontology\"));" + 
            System.lineSeparator() + "sn.addField(new MultipleControlledVocabularyNode(\"designedForOntologyTask\", new ArrayList<String>() {{ add(\"Annotation Task\"); add(\"Configuration Task\"); }}));" + 
            System.lineSeparator() + "sn.addField(new SimplePrimitiveNode(\"knownUsage\", \"knownUsage\"));" + 
            System.lineSeparator() + "sn.addField(new SimpleCompoundNode(\"resourceVersion\", htResourceVersion));" + 
            System.lineSeparator() + "sn.addField(new MultiplePrimitiveNode(\"imports\", new ArrayList<String>() {{ add(\"http://semantic.imports1\"); add(\"http://semantic.imports2\"); }}));" + 
            System.lineSeparator() + "sn.addField(new SimplePrimitiveNode(\"bugDatabase\", \"http://semantic.bug.tracker\"));" + 
            System.lineSeparator() + "sn.addField(new SimplePrimitiveNode(\"endpoint\", \"http://semantic.sparql.endpoint\"));" + 
            System.lineSeparator() + "sn.addField(new MultiplePrimitiveNode(\"imports\", new ArrayList<String>() {{ add(\"http://semantic.uri1\"); add(\"http://semantic.uri2\"); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "System.out.print(sn.toJSON());"
        );
    }

    /** <c>HowToBuildASimpleCompoundNode</c> montre la manière de construire un <c>SimpleCompoundNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *          Hashtable<String,BaseNode> project = new Hashtable<String,BaseNode>();
     *          project.put("projectAcronym",       new SimplePrimitiveNode("projectAcronym", "projectAcronym"));
     *          project.put("projectId",            new SimplePrimitiveNode("projectId", "projectId"));
     *          project.put("projectTitle",         new SimplePrimitiveNode("projectTitle", "projectTitle"));
     *          project.put("projectTask",          new SimplePrimitiveNode("projectTask", "projectTask"));
     *          project.put("projectURL",           new SimplePrimitiveNode("projectURL", "projectURL"));
     *          project.put("projectWorkPackage",   new SimplePrimitiveNode("projectWorkPackage", "projectWorkPackage"));
     *  
     *          SimpleCompoundNode scn = new SimpleCompoundNode("project", project);
     *          System.out.print(scn.ToJSON());
     *  </pre>
     */
    public static void HowToBuildASimpleCompoundNode()
    {
        System.out.print
        (
            System.lineSeparator() + "Hashtable<String,BaseNode> project = new Hashtable<String,BaseNode>();" +
            System.lineSeparator() + "project.put(\"projectAcronym\", new SimplePrimitiveNode(\"projectAcronym\", \"projectAcronym\"));" +
            System.lineSeparator() + "project.put(\"projectId\", new SimplePrimitiveNode(\"projectId\", \"projectId\"));" +
            System.lineSeparator() + "project.put(\"projectTitle\", new SimplePrimitiveNode(\"projectTitle\", \"projectTitle\"));" +
            System.lineSeparator() + "project.put(\"projectTask\", new SimplePrimitiveNode(\"projectTask\", \"projectTask\"));" +
            System.lineSeparator() + "project.put(\"projectURL\", new SimplePrimitiveNode(\"projectURL\", \"projectURL\"));" +
            System.lineSeparator() + "project.put(\"projectWorkPackage\", new SimplePrimitiveNode(\"projectWorkPackage\", \"projectWorkPackage\"));" +
            System.lineSeparator() +
            System.lineSeparator() + "SimpleCompoundNode scn = new SimpleCompoundNode(\"project\", project);" +
            System.lineSeparator() + "System.out.print(scn.ToJSON());"
        );
    }

    /** <c>HowToBuildASimpleControlledVocabularyNode</c> montre la manière de construire un <c>SimpleControlledVocabularyNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *          SimpleControlledVocabularyNode scvn = new SimpleControlledVocabularyNode("journalArticleType", "news");";
     *          System.out.print(scvn.ToJSON());
     *  </pre>
     */
    public static void HowToBuildASimpleControlledVocabularyNode()
    {
        System.out.print
        (
            System.lineSeparator() + "SimpleControlledVocabularyNode scvn = new SimpleControlledVocabularyNode(\"journalArticleType\", \"news\");" +
            System.lineSeparator() + "System.out.print(scvn.ToJSON());"
        );
    }

    /** <c>HowToBuildASimplePrimitiveNode</c> montre la manière de construire un <c>SimplePrimitiveNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *          SimplePrimitiveNode spn = new SimplePrimitiveNode("softwareName", "softwareName");
     *          System.out.print(spn.ToJSON());
     *  </pre>
     */
    public static void HowToBuildASimplePrimitiveNode()
    {
        System.out.print
        (
            System.lineSeparator() + "SimplePrimitiveNode spn = new SimplePrimitiveNode(\"softwareName\", \"softwareName\");" +
            System.lineSeparator() + "System.out.print(spn.ToJSON());"
        );
    }

    /** <c>HowToBuildASocialScienceNode</c> montre la manière de construire un <c>SocialScienceNode</c>
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *      // je crée la taille de l'échantillon 
     *      Hashtable<String,BaseNode> htTargetSampleSize = new Hashtable<String,BaseNode>();
     *      htTargetSampleSize.put("targetSampleActualSize", new SimplePrimitiveNode("targetSampleActualSize", "42"));
     *      htTargetSampleSize.put("targetSampleSizeFormula", new SimplePrimitiveNode("targetSampleSizeFormula", "targetSampleSizeFormula"));
     *      
     *      // je crée notes du node
     *      Hashtable<String,BaseNode> htSocialScienceNotes = new Hashtable<String,BaseNode>();
     *      htSocialScienceNotes.put("socialScienceNotesSubject", new SimplePrimitiveNode("socialScienceNotesSubject", "socialScienceNotesSubject"));
     *      htSocialScienceNotes.put("socialScienceNotesText", new SimplePrimitiveNode("socialScienceNotesText", "socialScienceNotesText"));
     *      htSocialScienceNotes.put("socialScienceNotesType", new SimplePrimitiveNode("socialScienceNotesType", "socialScienceNotesType"));
     *      
     *      // je crée notes du node
     *      Hashtable<String,BaseNode> htGeographicalReferential1 = new Hashtable<String,BaseNode>();
     *      htGeographicalReferential1.put("level", new SimplePrimitiveNode("level", "geographicalReferentialLevel1"));
     *      htGeographicalReferential1.put("version", new SimplePrimitiveNode("version", "geographicalReferentialVersion1"));
     *      
     *      Hashtable<String,BaseNode> htGeographicalReferential2 = new Hashtable<String,BaseNode>();
     *      htGeographicalReferential2.put("level", new SimplePrimitiveNode("level", "geographicalReferentialLevel2"));
     *      htGeographicalReferential2.put("version", new SimplePrimitiveNode("version", "geographicalReferentialVersion2"));     *      
     *      
     *      // je crée un node socialscience
     *      SocialScienceNode ssn = new SocialScienceNode();
     *      ssn.addField(new MultipleControlledVocabularyNode("unitOfAnalysis", new ArrayList<String>() {{ add("Individual"); add("Organization"); }}));
     *      ssn.addField(new MultiplePrimitiveNode("universe", new ArrayList<String>() {{ add("universe1"); add("universe2"); }}));
     *      ssn.addField(new SimpleControlledVocabularyNode("timeMethod", "Longitudinal"));
     *      ssn.addField(new SimplePrimitiveNode("timeMethodOther", "timeMethodOther"));
     *      ssn.addField(new SimplePrimitiveNode("dataCollector", "dataCollector"));
     *      ssn.addField(new SimplePrimitiveNode("collectorTraining", "collectorTraining"));
     *      ssn.addField(new SimplePrimitiveNode("frequencyOfDataCollection", "frequency"));
     *      ssn.addField(new SimpleControlledVocabularyNode("samplingProcedure", "Total universe/Complete enumeration"));
     *      ssn.addField(new SimpleControlledVocabularyNode("samplingProcedureOther", "samplingProcedureOther"));
     *      ssn.addField(new SimpleCompoundNode("targetSampleSize", htTargetSampleSize));
     *      ssn.addField(new SimplePrimitiveNode("deviationsFromSampleDesign", "deviationsFromSampleDesign"));
     *      ssn.addField(new SimpleControlledVocabularyNode("collectionMode", "Interview"));
     *      ssn.addField(new SimplePrimitiveNode("collectionModeOther", "collectionModeOther"));
     *      ssn.addField(new SimplePrimitiveNode("researchInstrument", "researchInstrument"));
     *      ssn.addField(new SimplePrimitiveNode("dataCollectionSituation", "dataCollectionSituation"));
     *      ssn.addField(new SimplePrimitiveNode("actionsToMinimizeLoss", "actionsToMinimizeLoss"));
     *      ssn.addField(new SimplePrimitiveNode("controlOperations", "controlOperations"));
     *      ssn.addField(new SimplePrimitiveNode("weighting", "weighting"));
     *      ssn.addField(new SimplePrimitiveNode("cleaningOperations", "cleaningOperations"));
     *      ssn.addField(new SimplePrimitiveNode("datasetLevelErrorNotes", "datasetLevelErrorNotes"));
     *      ssn.addField(new SimplePrimitiveNode("responseRate", "responseRate"));
     *      ssn.addField(new SimplePrimitiveNode("samplingErrorEstimates", "samplingErrorEstimates"));
     *      ssn.addField(new SimplePrimitiveNode("otherDataAppraisal", "otherDataAppraisal"));
     *      ssn.addField(new SimpleCompoundNode("socialScienceNotes", htSocialScienceNotes));
     *      ssn.addField(new MultipleCompoundNode("geographicalReferential", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htGeographicalReferential1); add(htGeographicalReferential2); }}));
     *      
     *      System.out.print(ssn.toJSON());
     *  </pre>
     */
    public static void HowToBuildASocialScienceNode()
    {
        System.out.print
        (
            System.lineSeparator() + "// je crée la taille de l'échantillon " + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htTargetSampleSize = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htTargetSampleSize.put(\"targetSampleActualSize\", new SimplePrimitiveNode(\"targetSampleActualSize\", \"42\"));" + 
            System.lineSeparator() + "htTargetSampleSize.put(\"targetSampleSizeFormula\", new SimplePrimitiveNode(\"targetSampleSizeFormula\", \"targetSampleSizeFormula\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée notes du node" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htSocialScienceNotes = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htSocialScienceNotes.put(\"socialScienceNotesSubject\", new SimplePrimitiveNode(\"socialScienceNotesSubject\", \"socialScienceNotesSubject\"));" + 
            System.lineSeparator() + "htSocialScienceNotes.put(\"socialScienceNotesText\", new SimplePrimitiveNode(\"socialScienceNotesText\", \"socialScienceNotesText\"));" + 
            System.lineSeparator() + "htSocialScienceNotes.put(\"socialScienceNotesType\", new SimplePrimitiveNode(\"socialScienceNotesType\", \"socialScienceNotesType\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée notes du node" + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htGeographicalReferential1 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htGeographicalReferential1.put(\"level\", new SimplePrimitiveNode(\"level\", \"geographicalReferentialLevel1\"));" + 
            System.lineSeparator() + "htGeographicalReferential1.put(\"version\", new SimplePrimitiveNode(\"version\", \"geographicalReferentialVersion1\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "Hashtable<String,BaseNode> htGeographicalReferential2 = new Hashtable<String,BaseNode>();" + 
            System.lineSeparator() + "htGeographicalReferential2.put(\"level\", new SimplePrimitiveNode(\"level\", \"geographicalReferentialLevel2\"));" + 
            System.lineSeparator() + "htGeographicalReferential2.put(\"version\", new SimplePrimitiveNode(\"version\", \"geographicalReferentialVersion2\"));" + 
            System.lineSeparator() + 
            System.lineSeparator() + 
            System.lineSeparator() + "// je crée un node socialscience" + 
            System.lineSeparator() + "SocialScienceNode ssn = new SocialScienceNode();" + 
            System.lineSeparator() + "ssn.addField(new MultipleControlledVocabularyNode(\"unitOfAnalysis\", new ArrayList<String>() {{ add(\"Individual\"); add(\"Organization\"); }}));" + 
            System.lineSeparator() + "ssn.addField(new MultiplePrimitiveNode(\"universe\", new ArrayList<String>() {{ add(\"universe1\"); add(\"universe2\"); }}));" + 
            System.lineSeparator() + "ssn.addField(new SimpleControlledVocabularyNode(\"timeMethod\", \"Longitudinal\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"timeMethodOther\", \"timeMethodOther\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"dataCollector\", \"dataCollector\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"collectorTraining\", \"collectorTraining\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"frequencyOfDataCollection\", \"frequency\"));" + 
            System.lineSeparator() + "ssn.addField(new SimpleControlledVocabularyNode(\"samplingProcedure\", \"Total universe/Complete enumeration\"));" + 
            System.lineSeparator() + "ssn.addField(new SimpleControlledVocabularyNode(\"samplingProcedureOther\", \"samplingProcedureOther\"));" + 
            System.lineSeparator() + "ssn.addField(new SimpleCompoundNode(\"targetSampleSize\", htTargetSampleSize));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"deviationsFromSampleDesign\", \"deviationsFromSampleDesign\"));" + 
            System.lineSeparator() + "ssn.addField(new SimpleControlledVocabularyNode(\"collectionMode\", \"Interview\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"collectionModeOther\", \"collectionModeOther\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"researchInstrument\", \"researchInstrument\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"dataCollectionSituation\", \"dataCollectionSituation\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"actionsToMinimizeLoss\", \"actionsToMinimizeLoss\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"controlOperations\", \"controlOperations\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"weighting\", \"weighting\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"cleaningOperations\", \"cleaningOperations\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"datasetLevelErrorNotes\", \"datasetLevelErrorNotes\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"responseRate\", \"responseRate\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"samplingErrorEstimates\", \"samplingErrorEstimates\"));" + 
            System.lineSeparator() + "ssn.addField(new SimplePrimitiveNode(\"otherDataAppraisal\", \"otherDataAppraisal\"));" + 
            System.lineSeparator() + "ssn.addField(new SimpleCompoundNode(\"socialScienceNotes\", htSocialScienceNotes));" + 
            System.lineSeparator() + "ssn.addField(new MultipleCompoundNode(\"geographicalReferential\", new ArrayList<Hashtable<String,BaseNode>>() {{ add(htGeographicalReferential1); add(htGeographicalReferential2); }}));" + 
            System.lineSeparator() + 
            System.lineSeparator() + "System.out.print(ssn.toJSON());" 
        );
    }

    /** <c>ShowMetadataMap</c> détaille la carte des nodes du document de métadonnées
     * 
     *  <hr>
     *  <strong>Exemple de code : </strong>
     *  <pre>
     *          metadata
     *          |  citation
     *          |  |  accessToSources                       SimplePrimitiveNode
     *          |  |  alternativeTitle                      SimplePrimitiveNode
     *          |  |  alternativeURL                        SimplePrimitiveNode
     *          |  |  author                                MultipleCompoundNode
     *          |  |  |  authorAffiliation                  SimplePrimitiveNode
     *          |  |  |  authorIdentifier                   SimplePrimitiveNode
     *          |  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode
     *          |  |  |  authorName                         SimplePrimitiveNode
     *          |  |  characteristicOfSources               SimplePrimitiveNode
     *          |  |  contributor                           MultipleCompoundNode
     *          |  |  |  contributorAffiliation             SimplePrimitiveNode
     *          |  |  |  contributorIdentifier              SimplePrimitiveNode
     *          |  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode
     *          |  |  |  contributorName                    SimplePrimitiveNode
     *          |  |  |  contributorType                    SimpleControlledVocabularyNode
     *          |  |  dataOrigin                            MultipleControlledVocabularyNode
     *          |  |  datasetContact                        MultipleCompoundNode
     *          |  |  |  datasetContactAffiliation          SimplePrimitiveNode
     *          |  |  |  datasetContactEmail                SimplePrimitiveNode
     *          |  |  |  datasetContactName                 SimplePrimitiveNode
     *          |  |  dataSources                           MultiplePrimitiveNode
     *          |  |  dateOfCollection                      MultipleCompoundNode
     *          |  |  |  dateOfCollectionStart              SimplePrimitiveNode
     *          |  |  |  dateOfCollectionEnd                SimplePrimitiveNode
     *          |  |  dateOfDeposit                         SimplePrimitiveNode
     *          |  |  depositor                             SimplePrimitiveNode
     *          |  |  distributionDate                      SimplePrimitiveNode
     *          |  |  distributor                           MultipleCompoundNode
     *          |  |  |  distributorAbbreviation            SimplePrimitiveNode
     *          |  |  |  distributorAffiliation             SimplePrimitiveNode
     *          |  |  |  distributorLogoURL                 SimplePrimitiveNode
     *          |  |  |  distributorName                    SimplePrimitiveNode
     *          |  |  |  distributorURL                     SimplePrimitiveNode
     *          |  |  dsDescription                         MultipleCompoundNode
     *          |  |  |  dsDescriptionDate                  SimplePrimitiveNode
     *          |  |  |  dsDescriptionValue                 SimplePrimitiveNode
     *          |  |  grantNumber                           MultipleCompoundNode
     *          |  |  |  grantNumberAgency                  SimplePrimitiveNode
     *          |  |  |  grantNumberValue                   SimplePrimitiveNode
     *          |  |  keyword                               MultipleCompoundNode
     *          |  |  |  keywordTermURI                     SimplePrimitiveNode
     *          |  |  |  keywordValue                       SimplePrimitiveNode
     *          |  |  |  keywordVocabulary                  SimplePrimitiveNode
     *          |  |  |  keywordVocabularyURI               SimplePrimitiveNode
     *          |  |  kindOfData                            MultipleControlledVocabularyNode
     *          |  |  kindOfDataOther                       MultiplePrimitiveNode
     *          |  |  language                              MultipleControlledVocabularyNode
     *          |  |  lifeCycleStep                         MultipleControlledVocabularyNode
     *          |  |  notesText                             SimplePrimitiveNode
     *          |  |  originOfSources                       SimplePrimitiveNode
     *          |  |  otherId                               MultipleCompoundNode
     *          |  |  |  otherIdAgency                      SimplePrimitiveNode
     *          |  |  |  otherIdValue                       SimplePrimitiveNode
     *          |  |  otherReferences                       MultiplePrimitiveNode
     *          |  |  producer                              MultipleCompoundNode
     *          |  |  |  producerAbbreviation               SimplePrimitiveNode
     *          |  |  |  producerAffiliation                SimplePrimitiveNode
     *          |  |  |  producerLogoURL                    SimplePrimitiveNode
     *          |  |  |  producerName                       SimplePrimitiveNode
     *          |  |  |  producerURL                        SimplePrimitiveNode
     *          |  |  productionDate                        SimplePrimitiveNode
     *          |  |  productionPlace                       SimplePrimitiveNode
     *          |  |  project                               SimpleCompoundNode
     *          |  |  |  projectAcronym                     SimplePrimitiveNode
     *          |  |  |  projectId                          SimplePrimitiveNode
     *          |  |  |  projectTask                        SimplePrimitiveNode
     *          |  |  |  projectTitle                       SimplePrimitiveNode
     *          |  |  |  projectURL                         SimplePrimitiveNode
     *          |  |  |  projectWorkPackage                 SimplePrimitiveNode
     *          |  |  publication                           MultipleCompoundNode
     *          |  |  |  publicationCitation                SimplePrimitiveNode
     *          |  |  |  publicationIDNumber                SimplePrimitiveNode
     *          |  |  |  publicationIDType                  SimpleControlledVocabularyNode
     *          |  |  |  publicationURL                     SimplePrimitiveNode
     *          |  |  relatedDataset                        MultipleCompoundNode
     *          |  |  |  relatedDatasetCitation             SimplePrimitiveNode
     *          |  |  |  relatedDatasetIDNumber             SimplePrimitiveNode
     *          |  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode
     *          |  |  |  relatedDatasetURL                  SimplePrimitiveNode
     *          |  |  relatedMaterial                       MultiplePrimitiveNode
     *          |  |  series                                SimpleCompoundNode
     *          |  |  |  seriesInformation                  SimplePrimitiveNode
     *          |  |  |  seriesName                         SimplePrimitiveNode
     *          |  |  software                              MultipleCompoundNode
     *          |  |  |  softwareName                       SimplePrimitiveNode
     *          |  |  |  softwareVersion                    SimplePrimitiveNode
     *          |  |  subject                               MultipleControlledVocabularyNode
     *          |  |  subtitle                              SimplePrimitiveNode
     *          |  |  timePeriodCovered                     MultipleCompoundNode
     *          |  |  |  timePeriodCoveredStart             SimplePrimitiveNode
     *          |  |  |  timePeriodCoveredEnd               SimplePrimitiveNode
     *          |  |  title                                 SimplePrimitiveNode
     *          |  |  topicClassification                   MultipleCompoundNode
     *          |  |  |  topicClassValue                    SimplePrimitiveNode
     *          |  |  |  topicClassVocab                    SimplePrimitiveNode
     *          |  |  |  topicClassVocabURI                 SimplePrimitiveNode
     *          |  geospatial
     *          |  |  conformity                            MultipleCompoundNode
     *          |  |  |  degree                             SimpleControlledVocabularyNode
     *          |  |  |  specification                      MultiplePrimitiveNode
     *          |  |  geographicBoundingBox                 MultipleCompoundNode
     *          |  |  |  eastLongitude                      SimplePrimitiveNode
     *          |  |  |  northLongitude                     SimplePrimitiveNode
     *          |  |  |  southLongitude                     SimplePrimitiveNode
     *          |  |  |  westLongitude                      SimplePrimitiveNode
     *          |  |  geographicCoverage                    MultipleCompoundNode
     *          |  |  |  city                               SimplePrimitiveNode
     *          |  |  |  country                            SimpleControlledVocabularyNode
     *          |  |  |  otherGeographicCoverage            SimplePrimitiveNode
     *          |  |  |  state                              SimplePrimitiveNode
     *          |  |  geographicUnit                        MultiplePrimitiveNode
     *          |  |  qualityValidity                       MultipleCompoundNode
     *          |  |  |  lineage                            MultiplePrimitiveNode
     *          |  |  |  spatialResolution                  MultiplePrimitiveNode
     *          |  socialscience
     *          |  |  actionsToMinimizeLoss                 SimplePrimitiveNode
     *          |  |  cleaningOperations                    SimplePrimitiveNode
     *          |  |  collectionMode                        SimpleControlledVocabularyNode
     *          |  |  collectionModeOther                   SimplePrimitiveNode
     *          |  |  collectorTraining                     SimplePrimitiveNode
     *          |  |  controlOperations                     SimplePrimitiveNode
     *          |  |  dataCollectionSituation               SimplePrimitiveNode
     *          |  |  dataCollector                         SimplePrimitiveNode
     *          |  |  datasetLevelErrorNotes                SimplePrimitiveNode
     *          |  |  deviationsFromSampleDesign            SimplePrimitiveNode
     *          |  |  frequencyOfDataCollection             SimplePrimitiveNode
     *          |  |  geographicalReferential               MultipleCompoundNode
     *          |  |  |  level                              SimplePrimitiveNode
     *          |  |  |  version                            SimplePrimitiveNode
     *          |  |  otherDataAppraisal                    SimplePrimitiveNode
     *          |  |  researchInstrument                    SimplePrimitiveNode
     *          |  |  responseRate                          SimplePrimitiveNode
     *          |  |  samplingErrorEstimates                SimplePrimitiveNode
     *          |  |  samplingProcedure                     SimpleControlledVocabularyNode
     *          |  |  samplingProcedureOther                SimplePrimitiveNode
     *          |  |  socialScienceNotes                    SimpleCompoundNode
     *          |  |  |  socialScienceNotesSubject          SimplePrimitiveNode
     *          |  |  |  socialScienceNotesText             SimplePrimitiveNode
     *          |  |  |  socialScienceNotesType             SimplePrimitiveNode
     *          |  |  targetSampleSize                      SimpleCompoundNode
     *          |  |  |  targetSampleActualSize             SimplePrimitiveNode
     *          |  |  |  targetSampleSizeFormula            SimplePrimitiveNode
     *          |  |  timeMethod                            SimpleControlledVocabularyNode
     *          |  |  timeMethodOther                       SimplePrimitiveNode
     *          |  |  unitOfAnalysis                        MultipleControlledVocabularyNode
     *          |  |  unitOfAnalysisOther                   SimplePrimitiveNode
     *          |  |  universe                              MultiplePrimitiveNode
     *          |  |  weighting                             SimplePrimitiveNode
     *          |  biomedical
     *          |  |  studyAssayCellType                    MultiplePrimitiveNode
     *          |  |  studyAssayMeasurementType             MultipleControlledVocabularyNode
     *          |  |  studyAssayOrganism                    MultipleControlledVocabularyNode
     *          |  |  studyAssayOtherOrganism               MultiplePrimitiveNode
     *          |  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode
     *          |  |  studyAssayPlatform                    MultipleControlledVocabularyNode
     *          |  |  studyAssayPlatformOther               MultiplePrimitiveNode
     *          |  |  studyAssayTechnologyType              MultipleControlledVocabularyNode
     *          |  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode
     *          |  |  studyDesignType                       MultipleControlledVocabularyNode
     *          |  |  studyDesignTypeOther                  MultiplePrimitiveNode
     *          |  |  studyFactorType                       MultipleControlledVocabularyNode
     *          |  |  studyFactorTypeOther                  MultiplePrimitiveNode
     *          |  |  studySampleType                       MultiplePrimitiveNode
     *          |  |  studyProtocolType                     MultiplePrimitiveNode
     *          |  journal
     *          |  |  journalArticleType                    SimpleControlledVocabularyNode
     *          |  |  journalVolumeIssue                    MultipleCompoundNode
     *          |  |  |  journalIssue                       SimplePrimitiveNode
     *          |  |  |  journalPubDate                     SimplePrimitiveNode
     *          |  |  |  journalVolume                      SimplePrimitiveNode
     *          |  Derived-text
     *          |  |  source                                MultipleCompoundNode
     *          |  |  |  ageOfSource                        MultiplePrimitiveNode
     *          |  |  |  citations                          MultiplePrimitiveNode
     *          |  |  |  experimentNumber                   MultiplePrimitiveNode
     *          |  |  |  typeOfSource                       MultiplePrimitiveNode
     *          |  semantics
     *          |  |  bugDatabase                           SimplePrimitiveNode
     *          |  |  designedForOntologyTask               MultipleControlledVocabularyNode
     *          |  |  endpoint                              SimplePrimitiveNode
     *          |  |  hasFormalityLevel                     SimpleControlledVocabularyNode
     *          |  |  hasOntologyLanguage                   SimpleControlledVocabularyNode
     *          |  |  knownUsage                            SimplePrimitiveNode
     *          |  |  imports                               MultiplePrimitiveNode
     *          |  |  resourceVersion                       SimpleCompoundNode
     *          |  |  |  changes                            SimplePrimitiveNode
     *          |  |  |  modificationDate                   SimplePrimitiveNode
     *          |  |  |  priorVersion                       MultiplePrimitiveNode
     *          |  |  |  versionInfo                        SimplePrimitiveNode
     *          |  |  |  versionStatus                      MultipleControlledVocabularyNode
     *          |  |  typeOfSR                              SimpleControlledVocabularyNode
     *          |  |  URI                                   MultiplePrimitiveNode
     *  </pre>
     */
    public static void ShowMetadataMap()
    {
        System.out.print
        (
            System.lineSeparator() + "metadata" + 
            System.lineSeparator() + "|  citation" + 
            System.lineSeparator() + "|  |  accessToSources                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  alternativeTitle                      SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  alternativeURL                        SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  author                                MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  authorAffiliation                  SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  authorIdentifier                   SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  authorIdentifierScheme             SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  |  authorName                         SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  characteristicOfSources               SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  contributor                           MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  contributorAffiliation             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  contributorIdentifier              SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  contributorIdentifierScheme        SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  |  contributorName                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  contributorType                    SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  dataOrigin                            MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  datasetContact                        MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  datasetContactAffiliation          SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  datasetContactEmail                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  datasetContactName                 SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  dataSources                           MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  dateOfCollection                      MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  dateOfCollectionStart              SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  dateOfCollectionEnd                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  dateOfDeposit                         SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  depositor                             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  distributionDate                      SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  distributor                           MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  distributorAbbreviation            SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  distributorAffiliation             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  distributorLogoURL                 SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  distributorName                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  distributorURL                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  dsDescription                         MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  dsDescriptionDate                  SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  dsDescriptionValue                 SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  grantNumber                           MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  grantNumberAgency                  SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  grantNumberValue                   SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  keyword                               MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  keywordTermURI                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  keywordValue                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  keywordVocabulary                  SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  keywordVocabularyURI               SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  kindOfData                            MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  kindOfDataOther                       MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  language                              MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  lifeCycleStep                         MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  notesText                             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  originOfSources                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  otherId                               MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  otherIdAgency                      SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  otherIdValue                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  otherReferences                       MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  producer                              MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  producerAbbreviation               SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  producerAffiliation                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  producerLogoURL                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  producerName                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  producerURL                        SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  productionDate                        SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  productionPlace                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  project                               SimpleCompoundNode" + 
            System.lineSeparator() + "|  |  |  projectAcronym                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  projectId                          SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  projectTask                        SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  projectTitle                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  projectURL                         SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  projectWorkPackage                 SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  publication                           MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  publicationCitation                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  publicationIDNumber                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  publicationIDType                  SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  |  publicationURL                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  relatedDataset                        MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  relatedDatasetCitation             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  relatedDatasetIDNumber             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  relatedDatasetIDType               SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  |  relatedDatasetURL                  SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  relatedMaterial                       MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  series                                SimpleCompoundNode" + 
            System.lineSeparator() + "|  |  |  seriesInformation                  SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  seriesName                         SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  software                              MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  softwareName                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  softwareVersion                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  subject                               MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  subtitle                              SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  timePeriodCovered                     MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  timePeriodCoveredStart             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  timePeriodCoveredEnd               SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  title                                 SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  topicClassification                   MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  topicClassValue                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  topicClassVocab                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  topicClassVocabURI                 SimplePrimitiveNode" + 
            System.lineSeparator() + "|  geospatial" + 
            System.lineSeparator() + "|  |  conformity                            MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  degree                             SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  |  specification                      MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  geographicBoundingBox                 MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  eastLongitude                      SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  northLongitude                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  southLongitude                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  westLongitude                      SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  geographicCoverage                    MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  city                               SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  country                            SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  |  otherGeographicCoverage            SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  state                              SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  geographicUnit                        MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  qualityValidity                       MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  lineage                            MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  spatialResolution                  MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  socialscience" + 
            System.lineSeparator() + "|  |  actionsToMinimizeLoss                 SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  cleaningOperations                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  collectionMode                        SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  collectionModeOther                   SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  collectorTraining                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  controlOperations                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  dataCollectionSituation               SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  dataCollector                         SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  datasetLevelErrorNotes                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  deviationsFromSampleDesign            SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  frequencyOfDataCollection             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  geographicalReferential               MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  level                              SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  version                            SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  otherDataAppraisal                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  researchInstrument                    SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  responseRate                          SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  samplingErrorEstimates                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  samplingProcedure                     SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  samplingProcedureOther                SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  socialScienceNotes                    SimpleCompoundNode" + 
            System.lineSeparator() + "|  |  |  socialScienceNotesSubject          SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  socialScienceNotesText             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  socialScienceNotesType             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  targetSampleSize                      SimpleCompoundNode" + 
            System.lineSeparator() + "|  |  |  targetSampleActualSize             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  targetSampleSizeFormula            SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  timeMethod                            SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  timeMethodOther                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  unitOfAnalysis                        MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  unitOfAnalysisOther                   SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  universe                              MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  weighting                             SimplePrimitiveNode" + 
            System.lineSeparator() + "|  biomedical" + 
            System.lineSeparator() + "|  |  studyAssayCellType                    MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studyAssayMeasurementType             MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  studyAssayOrganism                    MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  studyAssayOtherOrganism               MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studyAssayOtherMeasurmentType         MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studyAssayPlatform                    MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  studyAssayPlatformOther               MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studyAssayTechnologyType              MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  studyAssayTechnologyTypeOther         MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studyDesignType                       MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  studyDesignTypeOther                  MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studyFactorType                       MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  studyFactorTypeOther                  MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studySampleType                       MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  studyProtocolType                     MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  journal" + 
            System.lineSeparator() + "|  |  journalArticleType                    SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  journalVolumeIssue                    MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  journalIssue                       SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  journalPubDate                     SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  journalVolume                      SimplePrimitiveNode" + 
            System.lineSeparator() + "|  Derived-text" + 
            System.lineSeparator() + "|  |  source                                MultipleCompoundNode" + 
            System.lineSeparator() + "|  |  |  ageOfSource                        MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  citations                          MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  experimentNumber                   MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  typeOfSource                       MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  semantics" + 
            System.lineSeparator() + "|  |  bugDatabase                           SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  designedForOntologyTask               MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  endpoint                              SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  hasFormalityLevel                     SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  hasOntologyLanguage                   SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  knownUsage                            SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  imports                               MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  resourceVersion                       SimpleCompoundNode" + 
            System.lineSeparator() + "|  |  |  changes                            SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  modificationDate                   SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  priorVersion                       MultiplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  versionInfo                        SimplePrimitiveNode" + 
            System.lineSeparator() + "|  |  |  versionStatus                      MultipleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  typeOfSR                              SimpleControlledVocabularyNode" + 
            System.lineSeparator() + "|  |  URI                                   MultiplePrimitiveNode"
        );
    }

}
