package fr.inrae.sicpa.SicpaOpenData.API;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.File;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


/** Classe implémentant les méthodes d'accès aux dataverses INRAE
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class DataverseAPI
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          





    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //


    /**
     * Cette méthode permet d'éxécuter une requête de type GET
     * @param apiToken          jeton API de l'utilisateur
     * @param endpoint          adresse vers la ressource HTTP
     * @return reponse http
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String jetonAPI   = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String endpoint   = "http://localhost:57170/api/method?param=1234";
     *          
     *      String reponse   = DataverseAPI.executeGetRequest(jetonAPI, endpoint);
     * </pre>
     */
    private static String executeGetRequest(String apiToken, String endpoint) throws IOException
    {
        String response = "";
        
        // je crée un client http
        CloseableHttpClient httpClient = HttpClients.createDefault();
        
        try
        {
            // je crée la requête http
            HttpGet httpRequest = new HttpGet(endpoint);
            
            //j'ajoute les headers à la requête
            httpRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
            httpRequest.addHeader("X-Dataverse-key", apiToken);
            
            // j'exécute la requête et stocke la réponse dans une repose http
            CloseableHttpResponse httpResponse = httpClient.execute(httpRequest);
            
            try
            {
                //je traite la réponse
                if (httpResponse.getEntity() != null) 
                    response = EntityUtils.toString(httpResponse.getEntity());
            }
            finally
            {
                httpResponse.close();
            }            
        }
        finally
        {
            httpClient.close();
        }
        
        return response;
    }
        
    /**
     * Cette méthode permet d'éxécuter une requête de type POST
     * @param apiToken          jeton API de l'utilisateur
     * @param endpoint          adresse vers la ressource HTTP
     * @param httpEntity        paramètre POST de la requete
     * @return reponse http
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : POST sans paramètre</strong>
     * <pre>
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String endpoint         = "http://localhost:57170/api/method?param=1234";
     *          
     *      String reponse   = DataverseAPI.executePostRequest(jetonAPI, endpoint, null);
     * </pre>
     * <strong>Exemple : POST avec paramètre</strong>
     * <pre>
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String endpoint         = "http://localhost:57170/api/method?param=1234";
     *      HttpEntity httpEntity   = new StringEntity("ceci est un test");   
     *          
     *      String reponse   = DataverseAPI.executePostRequest(jetonAPI, endpoint, httpEntity);
     * </pre>
     * <strong>Exemple : POST avec fichier</strong>
     * <pre>
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String endpoint         = "http://localhost:57170/api/method?param=1234";
     *      String cheminFichier    = "c:\\temp\\fichier.csv";
     *      HttpEntity httpEntity   = MultipartEntityBuilder.create()
     *                                                      .addPart("file", new FileBody(new File(cheminFichier)))
     *                                                      .build();
     *          
     *      String reponse   = DataverseAPI.executePostRequest(jetonAPI, endpoint, httpEntity);
     * </pre>
     */
    private static String executePostRequest(String apiToken, String endpoint, HttpEntity httpEntity) throws IOException
    {
        String response = "";
        
        // je crée un client http
        CloseableHttpClient httpClient = HttpClients.createDefault();
        
        try
        {
            // je crée la requête http
            HttpPost httpRequest = new HttpPost(endpoint);
            httpRequest.setEntity(httpEntity);
            
            //j'ajoute les headers à la requête
            httpRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
            httpRequest.addHeader("X-Dataverse-key", apiToken);
            
            // j'exécute la requête et stocke la réponse dans une repose http
            CloseableHttpResponse httpResponse = httpClient.execute(httpRequest);
            
            try
            {
                //je traite la réponse
                if (httpResponse.getEntity() != null) 
                    response = EntityUtils.toString(httpResponse.getEntity());
            }
            finally
            {
                httpResponse.close();
            }            
        }
        finally
        {
            httpClient.close();
        }
        
        return response;
    }

    /**
     * Cette méthode permet de lister le contenu d'un dataset sous la forme d'une trame JSON
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param doi               DOI ciblé
     * @return trame JSON détaillant le contenu du dataset
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String adresseServeur   = "http://localhost:57170";
     *      String doi              = "doi:10.12345/ABCDEF";
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *          
     *      String contenuDataset   = DataverseAPI.GetDatasetContents(adresseServeur, jetonAPI, doi);
     * </pre>
     */
    public static String getDatasetContents(String serverBaseURI, String apiToken, String doi) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/datasets/:persistentId/?persistentId={PERSISTENT_IDENTIFIER}";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }

    /**
     * Cette méthode permet de lister les fichiers d'un dataset sous la forme d'une trame JSON
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param datasetID         identifiant numérique du dataset
     * @param datasetVersion    numéro de version du dataset
     * @return trame JSON listant les fichiers du dataset
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String adresseServeur   = "http://localhost:57170";
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String idDataset        = "123456";
     *      String versionDataset   = "3.0";
     *          
     *      String listeFichiers    = DataverseAPI.GetDatasetFiles(adresseServeur, jetonAPI, idDataset, versionDataset);
     * </pre>
     */
    public static String getDatasetFiles(String serverBaseURI, String apiToken, String datasetID, String datasetVersion) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}/files";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{DATASET_ID}", datasetID);
        endpoint = endpoint.replace("{DATASET_VERSION}", datasetVersion);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }

    /**
     * Cette méthode permet d'exporter les métadonnées d'un dataset dans un format de métadonnées demandé en paramètre
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param doi               DOI ciblé
     * @param metadataFormat    format des métadonnées
     * @return trame JSON détaillant les métadonnées dans le format démandé
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String adresseServeur   = "http://localhost:57170";
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String doi              = "doi:10.12345/ABCDEF";
     *      String formatMetadonnee = "Datacite";
     *          
     *      String metadonnees      = DataverseAPI.GetDatasetMetadataExport(adresseServeur, jetonAPI, doi, formatMetadonnee);
     * </pre>
     */
    public static String getDatasetMetadataExport(String serverBaseURI, String apiToken, String doi, String metadataFormat) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/datasets/export?exporter={METADATA_FORMAT}&persistentId={PERSISTENT_IDENTIFIER}";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi);
        endpoint = endpoint.replace("{METADATA_FORMAT}", metadataFormat);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }

    /**
     * Cette méthode permet de lister le contenu d'une version d'un dataset sous la forme d'une trame JSON
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param datasetID         identifiant numérique du dataset
     * @param datasetVersion    numéro de version du dataset
     * @return trame JSON détaillant le contenu d'une version du dataset
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String adresseServeur   = "http://localhost:57170";
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String idDataset        = "123456";
     *      String versionDataset   = "3.0";
     *          
     *      String versionDataset   = DataverseAPI.GetDatasetVersion(adresseServeur, jetonAPI, idDataset, versionDataset);
     * </pre>
     */
    public static String getDatasetVersion(String serverBaseURI, String apiToken, String datasetID, String datasetVersion) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/datasets/{DATASET_ID}/versions/{DATASET_VERSION}";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{DATASET_ID}", datasetID);
        endpoint = endpoint.replace("{DATASET_VERSION}", datasetVersion);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }

    /**
     * Cette méthode permet de lister toutes les versions d'un dataset sous la forme d'une trame JSON
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param datasetID         identifiant numérique du dataset
     * @return trame JSON listant toutes les versions du dataset
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String adresseServeur   = "http://localhost:57170";
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String idDataset        = "123456";
     *          
     *      String listeVersions    = DataverseAPI.GetDatasetVersionList(adresseServeur, jetonAPI, idDataset);
     * </pre>
     */
    public static String getDatasetVersionList(String serverBaseURI, String apiToken, String datasetID) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/datasets/{DATASET_ID}/versions";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{DATASET_ID}", datasetID);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }

    /**
     * Cette méthode permet de lister le contenu d'un dataverse sous la forme d'une trame JSON
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param dataverseID       identifiant du dataverse
     * @return trame JSON détaillant le contenu du dataverse
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String adresseServeur   = "http://localhost:57170";
     *      String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *      String idDataverse      = "INRAE Dataverse";
     *          
     *      String contenuDataverse = DataverseAPI.GetDataverseContents(adresseServeur, jetonAPI, idDataverse);
     * </pre>
     */
    public static String getDataverseContents(String serverBaseURI, String apiToken, String dataverseID) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/dataverses/{DATAVERSE_ID}/contents";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }

    /**
     * Cette méthode permet de lister les infos concernant un dataverse sous la forme d'une trame JSON
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param dataverseID       identifiant du dataverse
     * @return trame JSON détaillant le contenu du dataverse
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *         String adresseServeur   = "http://localhost:57170";
     *         String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *         String idDataverse      = "INRAE Dataverse";
     *         
     *         String infosDataverse   = DataverseAPI.GetDataverseInfos(adresseServeur, jetonAPI, idDataverse);
     * </pre>
     */
    public static String getDataverseInfos(String serverBaseURI, String apiToken, String dataverseID) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/dataverses/{DATAVERSE_ID}";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }

    /**
     * Cette méthode permet de lister les métdonnées d'un dataverse sous la forme d'une trame JSON
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param dataverseID       identifiant du dataverse
     * @return trame JSON listant les métadonnées d'un dataverse
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *         String adresseServeur   = "http://localhost:57170";
     *         String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *         String idDataverse      = "INRAE Dataverse";
     *         
     *         String metadonnees      = DataverseAPI.GetDataverseMetadata(adresseServeur, jetonAPI, idDataverse);
     * </pre>
     */
    public static String getDataverseMetadata(String serverBaseURI, String apiToken, String dataverseID) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/dataverses/{DATAVERSE_ID}/metadatablocks";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executeGetRequest(apiToken, endpoint);        
    }
  
    /**
     * Cette méthode permet d'uploader un fichier vers un dataset
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param doi               DOI ciblé
     * @param filePath          chemin vers le fichier à uploader
     * @return trame JSON détaillant le succès/l'échec de l'upload de fichier
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *          String adresseServeur   = "http://localhost:57170";
     *          String doi              = "doi:10.12345/ABCDEF";
     *          String cheminFichier    = "c:\\temp\\fichier.csv";
     *          String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *          
     *          String status           = DataverseAPI.PostDatasetAddFile(adresseServeur, jetonAPI, doi, cheminFichier);
     * </pre>
     */
    public static String postDatasetAddFile(String serverBaseURI, String apiToken, String doi, String filePath) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/datasets/:persistentId/add?persistentId={PERSISTENT_IDENTIFIER}";

        // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        //je configure l'entité http
        HttpEntity httpEntity    = MultipartEntityBuilder.create()
                                                         .addPart("file", new FileBody(new File(filePath)))
                                                         .build();
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executePostRequest(apiToken, endpoint, httpEntity);        
    }

    /**
     * Cette méthode permet de créer un dataset
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param dataverseID       DOI ciblé
     * @param metadataJSON      chemin vers le fichier à uploader
     * @return trame JSON détaillant le succès/l'échec de la création du dataset
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *          String adresseServeur   = "http://localhost:57170";
     *          String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *          String idDataverse      = "INRAE Dataverse";
     *          
     *          Path filePath           = Paths.get("c:\\temp\\metadata.json");
     *          List<String> lines      = Files.readAllLines(filePath, Charsets.UTF_8);
     *          String metadata         = String.join("\n", lines);
     *          
     *          String status           = DataverseAPI.PostDatasetCreate(adresseServeur, jetonAPI, idDataverse, metadata);
     * </pre>
     */
    public static String postDatasetCreate(String serverBaseURI, String apiToken, String dataverseID, String metadataJSON) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/dataverses/{DATAVERSE_ID}/datasets";

         // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{DATAVERSE_ID}", dataverseID);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executePostRequest(apiToken, endpoint, new StringEntity(metadataJSON));        
    }

    /**
     * Cette méthode permet de publier un dataset
     * @param serverBaseURI     adresse du dataverse
     * @param apiToken          jeton API de l'utilisateur
     * @param doi               DOI ciblé
     * @param versionType       le type de version {minor | major}
     * @return trame JSON détaillant le succès/l'échec de la publication du dataset
     * @throws IOException 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *          String adresseServeur   = "http://localhost:57170";
     *          String doi              = "doi:10.12345/ABCDEF";
     *          String jetonAPI         = "abcdefgh-1234-ijkl-5678-mnopqrstuvwx";
     *          String typeVersion      = "minor";
     *          
     *          String status           = DataverseAPI.PostDatasetPublish(adresseServeur, jetonAPI, doi, typeVersion);
     * </pre>
     */
    public static String postDatasetPublish(String serverBaseURI, String apiToken, String doi, String versionType) throws IOException
    {
        // je déclare et initialise mon endpoint
        String endpoint = "/api/datasets/:persistentId/actions/:publish?persistentId={PERSISTENT_IDENTIFIER}&type={VERSION_TYPE}";

         // je met à jour le endpoint avec les paramètres de la requete url
        endpoint = endpoint.replace("{PERSISTENT_IDENTIFIER}", doi);
        endpoint = endpoint.replace("{VERSION_TYPE}", versionType);
        endpoint = serverBaseURI.replaceAll("\\/*$", "") + endpoint;
        
        // j'execute la requete et retourne le résultat
        return DataverseAPI.executePostRequest(apiToken, endpoint, null);        
    }
}

