package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.ArrayList;
import java.util.Hashtable;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import fr.inrae.sicpa.SicpaOpenData.Helper.Helper;





/** Classe détaillant les vocabulaires contrôlés
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class ControlledVocabulary
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          





    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //


    /**
     * <strong>getControlledVocabularies</strong> est une méthode qui permet d'obtenir la liste des vocabulaires contrôlés
     * @return une hashtable contenant la liste des vocabulaires contrôlés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	Hashtable<String,ArrayList<String>> cv = ControlledVocabulary.GetControlledVocabularies(); 
	 * </pre>
     */
    @SuppressWarnings({ "unchecked" })
    public static Hashtable<String,ArrayList<String>> getControlledVocabularies()
    {
        Hashtable<String,ArrayList<String>> jsonContent = new Hashtable<String,ArrayList<String>>();
        String url = Helper.getProperty("controlledVocabularyWS.url");        
        String response = Helper.getFromWS(url);
        
        LinkedTreeMap<String, Object> controlledVocabularies = new Gson().fromJson(response, LinkedTreeMap.class);
        
        for(String categoryKey : controlledVocabularies.keySet())
        {
            if(controlledVocabularies.get(categoryKey) instanceof String)
                continue;
            
            if(controlledVocabularies.get(categoryKey) instanceof LinkedTreeMap)
            {
                LinkedTreeMap<String,ArrayList<String>> categoryCV = (LinkedTreeMap<String,ArrayList<String>>) controlledVocabularies.get(categoryKey);

                for(String cvKey : categoryCV.keySet())
                    jsonContent.put(cvKey, categoryCV.get(cvKey));
            }
        }
        
        return jsonContent;
    }
    
    /**
     * <strong>listValuesFor</strong> est une méthode qui permet d'obtenir la liste des vocabulaires contrôlés
     * @param node le noeud pour lequel recupéré les valeurs autorisées
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesFor("language"); 
	 * </pre>
     */
    @SuppressWarnings("unchecked")
    public static ArrayList<String> listValuesFor(String node)
    {
        String url = Helper.getProperty("controlledVocabularyWS.url") + "/listValuesFor?key=" + node;        
        String response = Helper.getFromWS(url);
        return new Gson().fromJson(response, ArrayList.class);
    }

    /**
     * <strong>listValuesForAuthorIdentifierSchemes</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'authorIdentifierScheme'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForAuthorIdentifierSchemes(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForAuthorIdentifierSchemes()
    {
        return ControlledVocabulary.listValuesFor("authorIdentifierScheme");
    }

    /**
     * <strong>listValuesForCollectionMode</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'collectionMode'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForCollectionMode(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForCollectionMode()
    {
        return ControlledVocabulary.listValuesFor("collectionMode");
    }

    /**
     * <strong>listValuesForContributorIdentifierScheme</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorIdentifierScheme'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForContributorIdentifierScheme(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForContributorIdentifierScheme()
    {
        return ControlledVocabulary.listValuesFor("contributorIdentifierScheme");
    }

    /**
     * <strong>listValuesForContributorType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'contributorType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForContributorType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForContributorType()
    {
        return ControlledVocabulary.listValuesFor("contributorType");
    }

    /**
     * <strong>listValuesForCountry</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'country'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForCountry(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForCountry()
    {
        return ControlledVocabulary.listValuesFor("country");
    }

    /**
     * <strong>listValuesForDataOrigin</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'dataOrigin'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForDataOrigin(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForDataOrigin()
    {
        return ControlledVocabulary.listValuesFor("dataOrigin");
    }

    /**
     * <strong>listValuesForDegree</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'degree'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForDegree(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForDegree()
    {
        return ControlledVocabulary.listValuesFor("degree");
    }

    /**
     * <strong>listValuesForDesignForOntologyTask</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'designedForOntologyTask'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForDesignForOntologyTask(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForDesignForOntologyTask()
    {
        return ControlledVocabulary.listValuesFor("designedForOntologyTask");
    }

    /**
     * <strong>listValuesForHasFormalityLevel</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasFormalityLevel'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForHasFormalityLevel(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForHasFormalityLevel()
    {
        return ControlledVocabulary.listValuesFor("hasFormalityLevel");
    }

    /**
     * <strong>listValuesForHasOntologyLanguage</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'hasOntologyLanguage'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForHasOntologyLanguage(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForHasOntologyLanguage()
    {
        return ControlledVocabulary.listValuesFor("hasOntologyLanguage");
    }

    /**
     * <strong>listValuesForJournalArticleType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'journalArticleType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForJournalArticleType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForJournalArticleType()
    {
        return ControlledVocabulary.listValuesFor("journalArticleType");
    }

    /**
     * <strong>listValuesForKindOfData</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'kindOfData'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForKindOfData(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForKindOfData()
    {
        return ControlledVocabulary.listValuesFor("kindOfData");
    }

    /**
     * <strong>listValuesForLanguage</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'language'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForLanguage(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForLanguage()
    {
        return ControlledVocabulary.listValuesFor("language");
    }

    /**
     * <strong>listValuesForLifeCycleStep</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'lifeCycleStep'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForLifeCycleStep(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForLifeCycleStep()
    {
        return ControlledVocabulary.listValuesFor("lifeCycleStep");
    }

    /**
     * <strong>listValuesForPublicationIDType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'publicationIDType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForPublicationIDType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForPublicationIDType()
    {
        return ControlledVocabulary.listValuesFor("publicationIDType");
    }

    /**
     * <strong>listValuesForRelatedDatasetIDType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'relatedDatasetIDType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForRelatedDatasetIDType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForRelatedDatasetIDType()
    {
        return ControlledVocabulary.listValuesFor("relatedDatasetIDType");
    }

    /**
     * <strong>listValuesForSamplingProcedure</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'samplingProcedure'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForSamplingProcedure(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForSamplingProcedure()
    {
        return ControlledVocabulary.listValuesFor("samplingProcedure");
    }

    /**
     * <strong>listValuesForStudyAssayMeasurementType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayMeasurementType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 * 		ArrayList<String> values = ControlledVocabulary.listValuesForStudyAssayMeasurementType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForStudyAssayMeasurementType()
    {
        return ControlledVocabulary.listValuesFor("studyAssayMeasurementType");
    }

    /**
     * <strong>listValuesForStudyAssayOrganism</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayOrganism'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForStudyAssayOrganism(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForStudyAssayOrganism()
    {
        return ControlledVocabulary.listValuesFor("studyAssayOrganism");
    }

    /**
     * <strong>listValuesForStudyAssayPlatform</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayPlatform'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForStudyAssayPlatform(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForStudyAssayPlatform()
    {
        return ControlledVocabulary.listValuesFor("studyAssayPlatform");
    }

    /**
     * <strong>listValuesForStudyAssayTechnologyType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyAssayTechnologyType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForStudyAssayTechnologyType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForStudyAssayTechnologyType()
    {
        return ControlledVocabulary.listValuesFor("studyAssayTechnologyType");
    }

    /**
     * <strong>listValuesForStudyDesignType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyDesignType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForStudyDesignType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForStudyDesignType()
    {
        return ControlledVocabulary.listValuesFor("studyDesignType");
    }

    /**
     * <strong>listValuesForStudyFactorType</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'studyFactorType'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForStudyFactorType(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForStudyFactorType()
    {
        return ControlledVocabulary.listValuesFor("studyFactorType");
    }

    /**
     * <strong>listValuesForSubject</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'subject'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForSubject(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForSubject()
    {
        return ControlledVocabulary.listValuesFor("subject");
    }

    /**
     * <strong>listValuesForTimeMethod</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'timeMethod'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForTimeMethod(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForTimeMethod()
    {
        return ControlledVocabulary.listValuesFor("timeMethod");
    }

    /**
     * <strong>listValuesForTypeOfSR</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'typeOfSR'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForTypeOfSR(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForTypeOfSR()
    {
        return ControlledVocabulary.listValuesFor("typeOfSR");
    }

    /**
     * <strong>listValuesForUnitOfAnalysis</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'unitOfAnalysis'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForUnitOfAnalysis(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForUnitOfAnalysis()
    {
        return ControlledVocabulary.listValuesFor("unitOfAnalysis");
    }

    /**
     * <strong>listValuesForVersionStatus</strong> est une méthode qui permet d'obtenir la liste des valeurs autorisées pour 'versionStatus'
     * @return la liste de valeurs autorisées
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	ArrayList<String> values = ControlledVocabulary.listValuesForVersionStatus(); 
	 * </pre>
     */
    public static ArrayList<String> listValuesForVersionStatus()
    {
        return ControlledVocabulary.listValuesFor("versionStatus");
    }
}
