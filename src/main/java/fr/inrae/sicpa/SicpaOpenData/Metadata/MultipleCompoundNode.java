package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;





/** Classe implémentant les nodes à valeurs composées multiples
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class MultipleCompoundNode extends BaseNode
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  

    /** Cette propriété contient la valeur du node */
    private List<Hashtable<String, BaseNode>> value;





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      MultipleCompoundNode mpn = new MultipleCompoundNode(); 
	 * </pre>
     */
    public MultipleCompoundNode()
    {
        this.setMultiple(true);
        this.setTypeClass("compound");
        this.setTypeName("");
        this.value      = new ArrayList<Hashtable<String, BaseNode>>();
     }
    
    /**
     * Constructeur nécessitant deux paramètres : typeName et values
     * @param typeName : nom du node
     * @param values : valeurs du node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String, BaseNode> values = new Hashtable<String, BaseNode>();
     *      values.put("value1", new SimplePrimitiveNode());
     *      values.put("value2", new SimpleControlledVocabularyNode());
     *      values.put("value3", new MultiplePrimitiveNode());
     *      values.put("value4", new MultipleControlledVocabularyNode());
     *      
     *      MultipleCompoundNode mcn = new MultipleCompoundNode("name", values);     
     * </pre>
     */
    public MultipleCompoundNode(String typeName, ArrayList<Hashtable<String, BaseNode>> values)
    {
        this.setMultiple(true);
        this.setTypeClass("compound");
        this.setTypeName(typeName);
        this.value      = new ArrayList<Hashtable<String, BaseNode>>();

        this.addValues(values);
    }





    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
     * @return valeur de l'attribut <strong>value</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      List<Hashtable<String, BaseNode>> value = mcn.getValue(); 
	 * </pre>
     */
    public List<Hashtable<String, BaseNode>> getValue()
    {
        return this.value;
    }


    
    
    
    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             

    /**
     * <strong>addValue</strong> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node
     * @param value : valeur à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String, BaseNode> value = new Hashtable<String, BaseNode>();
     *      value.add("name1", new SimplePrimitiveNode("name1", "value1"));
     *      value.add("name2", new SimpleControlledVocabularyNode("name2", "value2"));
     *      value.add("name3", new MultiplePrimitiveNode("name3", new ArrayList<String>(){{ add("value1"); add("value2"); }}));
     *      value.add("name4", new MultipleControlledVocabularyNode("name4", new ArrayList<String>(){{ add("controlledValue1"); add("controlledValue2"); }}));
     *      
     *      mcn.addValue(value);
     * </pre>
     */
    public void addValue(Hashtable<String, BaseNode> value)
    {
        if(!this.isValueValid(value))
            return;

        if (!MultipleCompoundNode.areSubnodesValid(this.getTypeName(), value))
            return;

        this.value.add(value);
    }

    /**
     * <strong>addValues</strong> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node
     * @param values : valeur à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String, BaseNode> value1 = new Hashtable<String, BaseNode>();
     *      value.add("name11", new SimplePrimitiveNode("name11", "value11"));
     *      value.add("name12", new MultipleControlledVocabularyNode("name12", new ArrayList<String>(){{ add("controlledValue121"); add("controlledValue122"); }}));
     *      
     *      Hashtable<String, BaseNode> value2 = new Hashtable<String, BaseNode>();
     *      value.add("name21", new SimpleControlledVocabularyNode("name211", "value211"));
     *      value.add("name22", new MultiplePrimitiveNode("name22", new ArrayList<String>(){{ add("value221"); add("value222"); }}));
     *      
     *      List<Hashtable<String, BaseNode>> values = new ArrayList() {{ add(value1); add(value2); }};
     *      mcn.addValue(values);
     * </pre>
     */
    public void addValues(ArrayList<Hashtable<String, BaseNode>> values)
    {
        for (Hashtable<String, BaseNode> value : values)
        {
            if (!(value instanceof Hashtable))
                continue;

            this.addValue(value);
        }
    }

    /**
     * <strong>areSubnodesValid</strong> est une méthode qui permet de vérifier que les subnodes sont valides
     * @return true si les subnodes sont valides, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if( this.areSubnodesValide() )
     *      {
     *          ...
     *      }
     * </pre>
     */
    private Boolean areSubnodesValid()
    {
        for (Hashtable<String, BaseNode> val : this.value)
        {
            for(String key : val.keySet())
            {
                BaseNode subnode = (BaseNode)val.get(key);

                if (!AuthorizedFields.listAuthorizedFieldsForCategoryNode(this.getTypeName()).containsKey(subnode.getTypeName()))
                    return false;

                if (!subnode.getClass().getName().contains(AuthorizedFields.getNodeType(subnode.getTypeName())))
                    return false;
            }
        }

        return true;
    }

    /**
     * <strong>clearValues</strong> est une méthode qui permet de réinitialiser la liste des valeurs du node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mcn.clearValues(); 
	 * </pre>
     */
    public void clearValues()
    {
        this.value.clear();
    }

    /**
     * <strong>containsValue</strong> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node
     * @param value : valeur à vérifier 
     * @return true si la valeur appartient à la liste de valeurs du node, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Boolean contains = mcn.containsValue(value); 
	 * </pre>
     */
    public Boolean containsValue(Hashtable<String, BaseNode> value)
    {
        return this.indexOf(value) >= 0;
    }

    /**
     * <strong>getValueAt</strong> est une méthode qui permet de récupérer la valeur située à un index précis
     * @param index : index de la valeur à récupérer 
     * @return valeur situé à l'index
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String value = mcn.getValueAt(3); 
	 * </pre>
     */
    public Hashtable<String, BaseNode> getValueAt(int index)
    {
        try
        {
            return this.value.get(index);
        }
        catch(Exception e)
        {
            return null;
        }
    }

    /**
     * <strong>indexOf</strong> est une méthode qui permet de récupérer l'index d'une valeur
     * @param value : valeur à rechercher 
     * @return index de la valeur
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      int index = mcn.indexOf(value);
     * </pre>
     */
    public int indexOf(Hashtable<String, BaseNode> value)
    {
        for (Hashtable<String, BaseNode> val : this.value)
        {
            Boolean contains = true;
            
            for (String key : val.keySet())
            {
                if (!value.containsKey(key))
                    contains = false;

                if (val.get(key).getClass().getName() != value.get(key).getClass().getName())
                    contains = false;

                if (val.get(key) instanceof SimplePrimitiveNode)
                    if (!MultipleCompoundNode.isEquals((SimplePrimitiveNode)val.get(key), (SimplePrimitiveNode)value.get(key)))
                        contains = false;

                if (val.get(key) instanceof SimpleControlledVocabularyNode)
                    if (!MultipleCompoundNode.isEquals((SimpleControlledVocabularyNode)val.get(key), (SimpleControlledVocabularyNode)value.get(key)))
                        contains = false;

                if (val.get(key) instanceof MultiplePrimitiveNode)
                    if (!MultipleCompoundNode.isEquals((MultiplePrimitiveNode)val.get(key), (MultiplePrimitiveNode)value.get(key)))
                        contains = false;

                if (val.get(key) instanceof MultipleControlledVocabularyNode)
                    if (!MultipleCompoundNode.isEquals((MultipleControlledVocabularyNode)val.get(key), (MultipleControlledVocabularyNode)value.get(key)))
                        contains = false;
            }

            if (contains)
                return (int)this.value.indexOf(val);
        }

        return -1;
    }

    /**
     * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
     * @return true si le node est valide, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if(mcn.isValid())
     *      {
     *          ...
     *      }
     * </pre>
     */
    public Boolean isValid()
    {
        return this.getMultiple()   instanceof Boolean      && this.getMultiple()
            && this.getTypeClass()  instanceof String       && this.getTypeClass() == "compound"
            && this.getTypeName()   instanceof String
            && this.getValue()      instanceof ArrayList    
            && this.isValueValid()
            && this.areSubnodesValid();
    }

    /**
     * <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur du node<br>
     * @return true si la valeur est valide, false sinon
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if(this.isValueValid())
     *      {
     *          ...
     *      }
     * </pre>
     */
    private Boolean isValueValid()
    {
        for(Hashtable<String, BaseNode> value : this.value)
        {
            for(String key : value.keySet())
            {
                if( !(value.get(key) instanceof SimplePrimitiveNode)
                    && !(value.get(key) instanceof SimpleControlledVocabularyNode)
                    && !(value.get(key) instanceof MultiplePrimitiveNode)
                    && !(value.get(key) instanceof MultipleControlledVocabularyNode) )
                    return false;
            }
        }        
        
        return true;
    }

    /**
     * <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur du node<br>
     * @param value : valeur à tester 
     * @return true si la valeur est valide, false sinon
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if(this.isValueValid(value))
     *      {
     *          ...
     *      }
     * </pre>
     */
    private Boolean isValueValid(Hashtable<String, BaseNode> value)
    {
        for(String key : value.keySet())
        {
            if( !(value.get(key) instanceof SimplePrimitiveNode)
                && !(value.get(key) instanceof SimpleControlledVocabularyNode)
                && !(value.get(key) instanceof MultiplePrimitiveNode)
                && !(value.get(key) instanceof MultipleControlledVocabularyNode) )
                return false;
        }

        return true;
    }

    /**
     * <strong>removeAll</strong> est une méthode qui permet de supprimer toutes les valeurs de la liste des valeurs
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mcn.removeAll(); 
	 * </pre>
     */
    public void removeAll()
    {
        this.clearValues();
    }

    /**
     * <strong>removeValue</strong> est une méthode qui permet de supprimer une valeur de la liste des valeurs
     * @param value : valeur à supprimer
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mcn.removeValue(value);
     * </pre>
     */
    public void removeValue(Hashtable<String, BaseNode> value)
    {
        try
        {
            this.value.remove( this.indexOf(value) );
        }
        catch(Exception e)
        {
        }
    }

    /**
     * <strong>removeValueAt</strong> est une méthode qui permet de supprimer la valeur située à un index précis
     * @param index : index de la valeur à supprimer
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mcn.removeValueAt(1); 
	 * </pre>
     */
    public void removeValueAt(int index)
    {
        try
        {
            this.value.remove(index);
        }
        catch(Exception e)
        {
        }
    }

    /**
     * <strong>toJSON</strong> est une méthode qui permet d'obtenir une réprésentation JSON du node
     * @return la représentation du node sous forme de chaine JSON
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = mpn.toJSON(); 
	 * </pre>
     */
    public String toJSON()
    {
        if (!this.isValid())
            return "";

        return new GsonBuilder().setPrettyPrinting()
                                .create()
                                .toJson(this);
    }

    /**
     * <strong>toString</strong> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)
     * @return la représentation du node sous forme de chaine de caractère
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = mpn.toString(); 
	 * </pre>
     */
    public String toString()
    {
        if (!this.isValid())
            return "";

        return new Gson().toJson(this);
    }





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //

    /**
     * <strong>areSubnodesValid</strong> est une méthode qui permet de vérifier que les subnodes sont valides
     * @param nodeName : le nom du noeud pour lequel vérifier les subnodes
     * @param value : une hashtable contenant les subnodes à vérifier
     * @return true si les subnodes sont valides, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *          if( MultipleCompoundNode.areSubnodesValid("name", hashtableSubnodes) )
     *          {
     *              ...
     *          }
     * </pre>
     */
    private static Boolean areSubnodesValid(String nodeName, Hashtable<String, BaseNode> value)
    {
        for(String key : value.keySet())
        {
            BaseNode subnode = (BaseNode)value.get(key);

            if (!AuthorizedFields.listAuthorizedFieldsForCategoryNode(nodeName).containsKey(subnode.getTypeName()))
                return false;

            if (!subnode.getClass().getName().contains(AuthorizedFields.getNodeType(subnode.getTypeName())))
                return false;
        }

        return true;
    }

    /**
     * <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux SimpleControlledVocabularyNodes
     * @param node1 : premier node à tester
     * @param node2 : second node à tester
     * @return true si les deux nodes sont similaires, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *          if(MultipleCompoundNode.isEquals(node1, node2))
     *          {
     *              ...
     *          }
     * </pre>
     */
    private static Boolean isEquals(SimpleControlledVocabularyNode node1, SimpleControlledVocabularyNode node2)
    {
        if (node1.getClass().getName() != node2.getClass().getName())
            return false;

        if (node1.getMultiple() != node2.getMultiple())
            return false;

        if (node1.getTypeClass() != node2.getTypeClass())
            return false;

        if (node1.getTypeName() != node2.getTypeName())
            return false;

        if (node1.getValue() != node2.getValue())
            return false;

        return true;
    }

    /**
     * <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux SimplePrimitiveNode
     * @param node1 : premier node à tester
     * @param node2 : second node à tester
     * @return true si les deux nodes sont similaires, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *          if(MultipleCompoundNode.isEquals(node1, node2))
     *          {
     *              ...
     *          }
     * </pre>
     */
    private static Boolean isEquals(SimplePrimitiveNode node1, SimplePrimitiveNode node2)
    {
        if (node1.getClass().getName() != node2.getClass().getName())
            return false;

        if (node1.getMultiple() != node2.getMultiple())
            return false;

        if (node1.getTypeClass() != node2.getTypeClass())
            return false;

        if (node1.getTypeName() != node2.getTypeName())
            return false;

        if (node1.getValue() != node2.getValue())
            return false;

        return true;
    }

    /**
     * <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux MultiplePrimitiveNode
     * @param node1 : premier node à tester
     * @param node2 : second node à tester
     * @return true si les deux nodes sont similaires, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *          if(MultipleCompoundNode.isEquals(node1, node2))
     *          {
     *              ...
     *          }
     * </pre>
     */
    private static Boolean isEquals(MultiplePrimitiveNode node1, MultiplePrimitiveNode node2)
    {
        if (node1.getClass().getName() != node2.getClass().getName())
            return false;

        if (node1.getMultiple() != node2.getMultiple())
            return false;

        if (node1.getTypeClass() != node2.getTypeClass())
            return false;

        if (node1.getTypeName() != node2.getTypeName())
            return false;

        for (String val : node1.getValue())
            if (!node2.getValue().contains(val))
                return false;

        for (String val : node2.getValue())
            if (!node1.getValue().contains(val))
                return false;

        return true;
    }

    /**
     * <strong>isEquals</strong> est une méthode qui permet de vérifier la similarité de deux MultipleControlledVocabularyNode
     * @param node1 : premier node à tester
     * @param node2 : second node à tester
     * @return true si les deux nodes sont similaires, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *          if(MultipleCompoundNode.isEquals(node1, node2))
     *          {
     *              ...
     *          }
     * </pre>
     */
    private static Boolean isEquals(MultipleControlledVocabularyNode node1, MultipleControlledVocabularyNode node2)
    {
        if (node1.getClass().getName() != node2.getClass().getName())
            return false;

        if (node1.getMultiple() != node2.getMultiple())
            return false;

        if (node1.getTypeClass() != node2.getTypeClass())
            return false;

        if (node1.getTypeName() != node2.getTypeName())
            return false;

        for (String val : node1.getValue())
            if (!node2.getValue().contains(val))
                return false;

        for (String val : node2.getValue())
            if (!node1.getValue().contains(val))
                return false;

        return true;
    }
}
