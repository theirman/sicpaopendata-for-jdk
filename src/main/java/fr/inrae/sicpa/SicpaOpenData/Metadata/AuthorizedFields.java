package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.Hashtable;

/** Classe détaillant les valeurs autorisées par nodes
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class AuthorizedFields
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          





    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //


    /**
     * <strong>getNodeType</strong> est une méthode qui permet de récupérer le type d'un node
     * @param node : le node pour lequel on souhaite récupérer le type
     * @return le type de node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String nodeType = AuthorizedFields.getNodeType("author"); 
     * </pre>
     */
    public static String getNodeType(String node)
    {
        switch(node)            
        {
            case "accessToSources":                     return "SimplePrimitiveNode";
            case "actionsToMinimizeLoss":               return "SimplePrimitiveNode";
            case "ageOfSource":                         return "MultiplePrimitiveNode";
            case "alternativeTitle":                    return "SimplePrimitiveNode";
            case "alternativeURL":                      return "SimplePrimitiveNode";
            case "author":                              return "MultipleCompoundNode";
            case "authorAffiliation":                   return "SimplePrimitiveNode";
            case "authorIdentifier":                    return "SimplePrimitiveNode";
            case "authorIdentifierScheme":              return "SimpleControlledVocabularyNode";
            case "authorName":                          return "SimplePrimitiveNode";
            case "bugDatabase":                         return "SimplePrimitiveNode";
            case "changes":                             return "SimplePrimitiveNode";
            case "characteristicOfSources":             return "SimplePrimitiveNode";
            case "citations":                           return "MultiplePrimitiveNode";
            case "city":                                return "SimplePrimitiveNode";
            case "cleaningOperations":                  return "SimplePrimitiveNode";
            case "collectionMode":                      return "SimpleControlledVocabularyNode";
            case "collectionModeOther":                 return "SimplePrimitiveNode";
            case "collectorTraining":                   return "SimplePrimitiveNode";
            case "conformity":                          return "MultipleCompoundNode";
            case "contributor":                         return "MultipleCompoundNode";
            case "contributorAffiliation":              return "SimplePrimitiveNode";
            case "contributorIdentifier":               return "SimplePrimitiveNode";
            case "contributorIdentifierScheme":         return "SimpleControlledVocabularyNode";
            case "contributorName":                     return "SimplePrimitiveNode";
            case "contributorType":                     return "SimpleControlledVocabularyNode";
            case "controlOperations":                   return "SimplePrimitiveNode";
            case "country":                             return "SimpleControlledVocabularyNode";
            case "dataCollectionSituation":             return "SimplePrimitiveNode";
            case "dataCollector":                       return "SimplePrimitiveNode";
            case "dataOrigin":                          return "MultipleControlledVocabularyNode";
            case "datasetContact":                      return "MultipleCompoundNode";
            case "datasetContactAffiliation":           return "SimplePrimitiveNode";
            case "datasetContactEmail":                 return "SimplePrimitiveNode";
            case "datasetContactName":                  return "SimplePrimitiveNode";
            case "datasetLevelErrorNotes":              return "SimplePrimitiveNode";
            case "dataSources":                         return "MultiplePrimitiveNode";
            case "dateOfCollection":                    return "MultipleCompoundNode";
            case "dateOfCollectionEnd":                 return "SimplePrimitiveNode";
            case "dateOfCollectionStart":               return "SimplePrimitiveNode";
            case "dateOfDeposit":                       return "SimplePrimitiveNode";
            case "degree":                              return "SimpleControlledVocabularyNode";
            case "depositor":                           return "SimplePrimitiveNode";
            case "designedForOntologyTask":             return "MultipleControlledVocabularyNode";
            case "deviationsFromSampleDesign":          return "SimplePrimitiveNode";
            case "distributionDate":                    return "SimplePrimitiveNode";
            case "distributor":                         return "MultipleCompoundNode";
            case "distributorAbbreviation":             return "SimplePrimitiveNode";
            case "distributorAffiliation":              return "SimplePrimitiveNode";
            case "distributorLogoURL":                  return "SimplePrimitiveNode";
            case "distributorName":                     return "SimplePrimitiveNode";
            case "distributorURL":                      return "SimplePrimitiveNode";
            case "dsDescription":                       return "MultipleCompoundNode";
            case "dsDescriptionDate":                   return "SimplePrimitiveNode";
            case "dsDescriptionValue":                  return "SimplePrimitiveNode";
            case "eastLongitude":                       return "SimplePrimitiveNode";
            case "endpoint":                            return "SimplePrimitiveNode";
            case "experimentNumber":                    return "MultiplePrimitiveNode";
            case "frequencyOfDataCollection":           return "SimplePrimitiveNode";
            case "geographicalReferential":             return "MultipleCompoundNode";
            case "geographicBoundingBox":               return "MultipleCompoundNode";
            case "geographicCoverage":                  return "MultipleCompoundNode";
            case "geographicUnit":                      return "MultiplePrimitiveNode";
            case "grantNumber":                         return "MultipleCompoundNode";
            case "grantNumberAgency":                   return "SimplePrimitiveNode";
            case "grantNumberValue":                    return "SimplePrimitiveNode";
            case "hasFormalityLevel":                   return "SimpleControlledVocabularyNode";
            case "hasOntologyLanguage":                 return "MultipleControlledVocabularyNode";
            case "imports":                             return "MultiplePrimitiveNode";
            case "journalArticleType":                  return "SimpleControlledVocabularyNode";
            case "journalIssue":                        return "SimplePrimitiveNode";
            case "journalPubDate":                      return "SimplePrimitiveNode";
            case "journalVolume":                       return "SimplePrimitiveNode";
            case "journalVolumeIssue":                  return "MultipleCompoundNode";
            case "keyword":                             return "MultipleCompoundNode";
            case "keywordTermURI":                      return "SimplePrimitiveNode";
            case "keywordValue":                        return "SimplePrimitiveNode";
            case "keywordVocabulary":                   return "SimplePrimitiveNode";
            case "keywordVocabularyURI":                return "SimplePrimitiveNode";
            case "kindOfData":                          return "MultipleControlledVocabularyNode";
            case "kindOfDataOther":                     return "MultiplePrimitiveNode";
            case "knownUsage":                          return "SimplePrimitiveNode";
            case "language":                            return "MultipleControlledVocabularyNode";
            case "level":                               return "SimplePrimitiveNode";
            case "lifeCycleStep":                       return "MultipleControlledVocabularyNode";
            case "lineage":                             return "MultiplePrimitiveNode";
            case "modificationDate":                    return "SimplePrimitiveNode";
            case "northLongitude":                      return "SimplePrimitiveNode";
            case "notesText":                           return "SimplePrimitiveNode";
            case "originOfSources":                     return "SimplePrimitiveNode";
            case "otherDataAppraisal":                  return "SimplePrimitiveNode";
            case "otherGeographicCoverage":             return "SimplePrimitiveNode";
            case "otherId":                             return "MultipleCompoundNode";
            case "otherIdAgency":                       return "SimplePrimitiveNode";
            case "otherIdValue":                        return "SimplePrimitiveNode";
            case "otherReferences":                     return "MultiplePrimitiveNode";
            case "priorVersion":                        return "MultiplePrimitiveNode";
            case "producer":                            return "MultipleCompoundNode";
            case "producerAbbreviation":                return "SimplePrimitiveNode";
            case "producerAffiliation":                 return "SimplePrimitiveNode";
            case "producerLogoURL":                     return "SimplePrimitiveNode";
            case "producerName":                        return "SimplePrimitiveNode";
            case "producerURL":                         return "SimplePrimitiveNode";
            case "productionDate":                      return "SimplePrimitiveNode";
            case "productionPlace":                     return "SimplePrimitiveNode";
            case "project":                             return "SimpleCompoundNode";
            case "projectAcronym":                      return "SimplePrimitiveNode";
            case "projectId":                           return "SimplePrimitiveNode";
            case "projectTask":                         return "SimplePrimitiveNode";
            case "projectTitle":                        return "SimplePrimitiveNode";
            case "projectURL":                          return "SimplePrimitiveNode";
            case "projectWorkPackage":                  return "SimplePrimitiveNode";
            case "publication":                         return "MultipleCompoundNode";
            case "publicationCitation":                 return "SimplePrimitiveNode";
            case "publicationIDNumber":                 return "SimplePrimitiveNode";
            case "publicationIDType":                   return "SimpleControlledVocabularyNode";
            case "publicationURL":                      return "SimplePrimitiveNode";
            case "qualityValidity":                     return "MultipleCompoundNode";
            case "relatedDataset":                      return "MultipleCompoundNode";
            case "relatedDatasetCitation":              return "SimplePrimitiveNode";
            case "relatedDatasetIDNumber":              return "SimplePrimitiveNode";
            case "relatedDatasetIDType":                return "SimpleControlledVocabularyNode";
            case "relatedDatasetURL":                   return "SimplePrimitiveNode";
            case "relatedMaterial":                     return "MultiplePrimitiveNode";
            case "researchInstrument":                  return "SimplePrimitiveNode";
            case "resourceVersion":                     return "SimpleCompoundNode";
            case "responseRate":                        return "SimplePrimitiveNode";
            case "samplingErrorEstimates":              return "SimplePrimitiveNode";
            case "samplingProcedure":                   return "SimpleControlledVocabularyNode";
            case "samplingProcedureOther":              return "SimplePrimitiveNode";
            case "series":                              return "SimpleCompoundNode";
            case "seriesInformation":                   return "SimplePrimitiveNode";
            case "seriesName":                          return "SimplePrimitiveNode";
            case "socialScienceNotes":                  return "SimpleCompoundNode";
            case "socialScienceNotesSubject":           return "SimplePrimitiveNode";
            case "socialScienceNotesText":              return "SimplePrimitiveNode";
            case "socialScienceNotesType":              return "SimplePrimitiveNode";
            case "software":                            return "MultipleCompoundNode";
            case "softwareName":                        return "SimplePrimitiveNode";
            case "softwareVersion":                     return "SimplePrimitiveNode";
            case "source":                              return "MultipleCompoundNode";
            case "southLongitude":                      return "SimplePrimitiveNode";
            case "spatialResolution":                   return "MultiplePrimitiveNode";
            case "specification":                       return "MultiplePrimitiveNode";
            case "state":                               return "SimplePrimitiveNode";
            case "studyAssayCellType":                  return "MultiplePrimitiveNode";
            case "studyAssayMeasurementType":           return "MultipleControlledVocabularyNode";
            case "studyAssayOrganism":                  return "MultipleControlledVocabularyNode";
            case "studyAssayOtherMeasurmentType":       return "MultiplePrimitiveNode";
            case "studyAssayOtherOrganism":             return "MultiplePrimitiveNode";
            case "studyAssayPlatform":                  return "MultipleControlledVocabularyNode";
            case "studyAssayPlatformOther":             return "MultiplePrimitiveNode";
            case "studyAssayTechnologyType":            return "MultipleControlledVocabularyNode";
            case "studyAssayTechnologyTypeOther":       return "MultiplePrimitiveNode";
            case "studyDesignType":                     return "MultipleControlledVocabularyNode";
            case "studyDesignTypeOther":                return "MultiplePrimitiveNode";
            case "studyFactorType":                     return "MultipleControlledVocabularyNode";
            case "studyFactorTypeOther":                return "MultiplePrimitiveNode";
            case "studyProtocolType":                   return "MultiplePrimitiveNode";
            case "studySampleType":                     return "MultiplePrimitiveNode";
            case "subject":                             return "MultipleControlledVocabularyNode";
            case "subtitle":                            return "SimplePrimitiveNode";
            case "targetSampleActualSize":              return "SimplePrimitiveNode";
            case "targetSampleSize":                    return "SimpleCompoundNode";
            case "targetSampleSizeFormula":             return "SimplePrimitiveNode";
            case "timeMethod":                          return "SimpleControlledVocabularyNode";
            case "timeMethodOther":                     return "SimplePrimitiveNode";
            case "timePeriodCovered":                   return "MultipleCompoundNode";
            case "timePeriodCoveredEnd":                return "SimplePrimitiveNode";
            case "timePeriodCoveredStart":              return "SimplePrimitiveNode";
            case "title":                               return "SimplePrimitiveNode";
            case "topicClassification":                 return "MultipleCompoundNode";
            case "topicClassValue":                     return "SimplePrimitiveNode";
            case "topicClassVocab":                     return "SimplePrimitiveNode";
            case "topicClassVocabURI":                  return "SimplePrimitiveNode";
            case "typeOfSource":                        return "MultiplePrimitiveNode";
            case "typeOfSR":                            return "SimpleControlledVocabularyNode";
            case "unitOfAnalysis":                      return "MultipleControlledVocabularyNode";
            case "unitOfAnalysisOther":                 return "SimplePrimitiveNode";
            case "universe":                            return "MultiplePrimitiveNode";
            case "URI":                                 return "MultiplePrimitiveNode";
            case "version":                             return "SimplePrimitiveNode";
            case "versionInfo":                         return "SimplePrimitiveNode";
            case "versionStatus":                       return "MultipleControlledVocabularyNode";
            case "weighting":                           return "SimplePrimitiveNode";
            case "westLongitude":                       return "SimplePrimitiveNode";
            default:                                    return "";
        }
    }

    /**
     * <strong>listAuthorizedFields</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le document des métadonnées
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFields();
     * </pre>
     */
    public static Hashtable<String,String> listAuthorizedFields()
    {
        Hashtable<String,String> authorizedFields = new Hashtable<String,String>();
        authorizedFields.putAll(AuthorizedFields.listAuthorizedFieldsForBiomedicalNode());
        authorizedFields.putAll(AuthorizedFields.listAuthorizedFieldsForCitationNode());
        authorizedFields.putAll(AuthorizedFields.listAuthorizedFieldsForDerivedTextNode());
        authorizedFields.putAll(AuthorizedFields.listAuthorizedFieldsForGeospatialNode());
        authorizedFields.putAll(AuthorizedFields.listAuthorizedFieldsForJournalNode());
        authorizedFields.putAll(AuthorizedFields.listAuthorizedFieldsForSemanticsNode());
        authorizedFields.putAll(AuthorizedFields.listAuthorizedFieldsForSocialScienceNode());
        return authorizedFields;
    }

    /**
     * <strong>listAuthorizedFieldsForCategoryNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un node donné
     * @param node : le node pour lequel lister les champs autorisés
     * @return la liste des fields autorisés pour le node passé en paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForCategoryNode("citation");
     * </pre>
     */
    public static Hashtable<String,String> listAuthorizedFieldsForCategoryNode(String node)
    {
        //category node
        switch (node)
        {
            case "biomedical":      return AuthorizedFields.listAuthorizedFieldsForBiomedicalNode();
            case "citation":        return AuthorizedFields.listAuthorizedFieldsForCitationNode();
            case "Derived-text":    return AuthorizedFields.listAuthorizedFieldsForDerivedTextNode();
            case "geospatial":      return AuthorizedFields.listAuthorizedFieldsForGeospatialNode();
            case "journal":         return AuthorizedFields.listAuthorizedFieldsForJournalNode();
            case "semantics":       return AuthorizedFields.listAuthorizedFieldsForSemanticsNode();
            case "socialscience":   return AuthorizedFields.listAuthorizedFieldsForSocialScienceNode();
            default:                return AuthorizedFields.listAuthorizedFieldsForCompoundSubnode(node);
        }
    }

    /**
     * <strong>listAuthorizedFieldsForCompoundSubNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés pour un subnode donné
     * @param compoundSubnode : le subnode pour lequel lister les champs autorisés
     * @return la liste des fields autorisés pour le subnode passé en paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForCompoundSubNode("author"); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForCompoundSubnode(String compoundSubnode)
    {
        //category node
        switch (compoundSubnode)
        {
            case "author":                      
                return new Hashtable<String,String>() 
                {{ 
                    put("authorAffiliation",                "SimplePrimitiveNode"             ); 
                    put("authorIdentifier",                 "SimplePrimitiveNode"             ); 
                    put("authorIdentifierScheme",           "SimpleControlledVocabularyNode"  ); 
                    put("authorName",                       "SimplePrimitiveNode"             ); 
                }};

            case "contributor":                 
                return new Hashtable<String,String>() 
                {{ 
                    put( "contributorAffiliation",          "SimplePrimitiveNode"               ); 
                    put( "contributorIdentifier",           "SimplePrimitiveNode"               ); 
                    put( "contributorIdentifierScheme",     "SimpleControlledVocabularyNode"    ); 
                    put( "contributorName",                 "SimplePrimitiveNode"               ); 
                    put( "contributorType",                 "SimpleControlledVocabularyNode"    ); 
                }};
            
            case "datasetContact":              
                return new Hashtable<String,String>() 
                {{ 
                    put( "datasetContactAffiliation",       "SimplePrimitiveNode"               ); 
                    put( "datasetContactEmail",             "SimplePrimitiveNode"               ); 
                    put( "datasetContactName",              "SimplePrimitiveNode"               ); 
                }};
            
            case "dateOfCollection":            
                return new Hashtable<String,String>() 
                {{ 
                    put( "dateOfCollectionStart",           "SimplePrimitiveNode"               ); 
                    put( "dateOfCollectionEnd",             "SimplePrimitiveNode"               ); 
                }};
            
            case "distributor":                 
                return new Hashtable<String,String>() 
                {{ 
                    put( "distributorAbbreviation",         "SimplePrimitiveNode"               ); 
                    put( "distributorAffiliation",          "SimplePrimitiveNode"               ); 
                    put( "distributorLogoURL",              "SimplePrimitiveNode"               ); 
                    put( "distributorName",                 "SimplePrimitiveNode"               ); 
                    put( "distributorURL",                  "SimplePrimitiveNode"               ); 
                }};
            
            case "dsDescription":               
                return new Hashtable<String,String>() 
                {{ 
                    put( "dsDescriptionDate",               "SimplePrimitiveNode"               ); 
                    put( "dsDescriptionValue",              "SimplePrimitiveNode"               ); 
                }};
            
            case "grantNumber":                 
                return new Hashtable<String,String>() 
                {{ 
                    put( "grantNumberAgency",               "SimplePrimitiveNode"               ); 
                    put( "grantNumberValue",                "SimplePrimitiveNode"               ); 
                }};
            
            case "keyword":                     
                return new Hashtable<String,String>() 
                {{ 
                    put( "keywordTermURI",                  "SimplePrimitiveNode"               ); 
                    put( "keywordValue",                    "SimplePrimitiveNode"               ); 
                    put( "keywordVocabulary",               "SimplePrimitiveNode"               ); 
                    put( "keywordVocabularyURI",            "SimplePrimitiveNode"               ); 
                }};
            
            case "otherId":                     
                return new Hashtable<String,String>() 
                {{ 
                    put( "otherIdAgency",                   "SimplePrimitiveNode"               ); 
                    put( "otherIdValue",                    "SimplePrimitiveNode"               ); 
                }};
            
            case "producer":                    
                return new Hashtable<String,String>() 
                {{ 
                    put( "producerAbbreviation",            "SimplePrimitiveNode"               ); 
                    put( "producerAffiliation",             "SimplePrimitiveNode"               ); 
                    put( "producerLogoURL",                 "SimplePrimitiveNode"               ); 
                    put( "producerName",                    "SimplePrimitiveNode"               ); 
                    put( "producerURL",                     "SimplePrimitiveNode"               ); 
                }};
            
            case "project":                     
                return new Hashtable<String,String>() 
                {{ 
                    put( "projectAcronym",                  "SimplePrimitiveNode"               ); 
                    put( "projectId",                       "SimplePrimitiveNode"               ); 
                    put( "projectTask",                     "SimplePrimitiveNode"               ); 
                    put( "projectTitle",                    "SimplePrimitiveNode"               ); 
                    put( "projectURL",                      "SimplePrimitiveNode"               ); 
                    put( "projectWorkPackage",              "SimplePrimitiveNode"               ); 
                }};
            
            case "publication":                 
                return new Hashtable<String,String>() 
                {{ 
                    put( "publicationCitation",             "SimplePrimitiveNode"               ); 
                    put( "publicationIDNumber",             "SimplePrimitiveNode"               ); 
                    put( "publicationIDType",               "SimpleControlledVocabularyNode"    ); 
                    put( "publicationURL",                  "SimplePrimitiveNode"               ); 
                }};
            
            case "relatedDataset":              
                return new Hashtable<String,String>() 
                {{ 
                    put( "relatedDatasetCitation",          "SimplePrimitiveNode"               ); 
                    put( "relatedDatasetIDNumber",          "SimplePrimitiveNode"               ); 
                    put( "relatedDatasetIDType",            "SimpleControlledVocabularyNode"    ); 
                    put( "relatedDatasetURL",               "SimplePrimitiveNode"               ); 
                }};
            
            case "series":                      
                return new Hashtable<String,String>() 
                {{ 
                    put( "seriesInformation",               "SimplePrimitiveNode"               ); 
                    put( "seriesName",                      "SimplePrimitiveNode"               ); 
                }};
            
            case "software":                    
                return new Hashtable<String,String>() 
                {{ 
                    put( "softwareName",                    "SimplePrimitiveNode"               ); 
                    put( "softwareVersion",                 "SimplePrimitiveNode"               ); 
                }};
            
            case "timePeriodCovered":           
                return new Hashtable<String,String>() 
                {{ 
                    put( "timePeriodCoveredStart",          "SimplePrimitiveNode"               ); 
                    put( "timePeriodCoveredEnd",            "SimplePrimitiveNode"               ); 
                }};
            
            case "topicClassification":         
                return new Hashtable<String,String>() 
                {{ 
                    put( "topicClassValue",                 "SimplePrimitiveNode"               ); 
                    put( "topicClassVocab",                 "SimplePrimitiveNode"               ); 
                    put( "topicClassVocabURI",              "SimplePrimitiveNode"               ); 
                }};
            
            case "conformity":                  
                return new Hashtable<String,String>() 
                {{ 
                    put( "degree",                          "SimpleControlledVocabularyNode"    ); 
                    put( "specification",                   "SimplePrimitiveNode"               ); 
                }};
            
            case "geographicBoundingBox":       
                return new Hashtable<String,String>() 
                {{ 
                    put( "eastLongitude",                   "SimplePrimitiveNode"               ); 
                    put( "northLongitude",                  "SimplePrimitiveNode"               ); 
                    put( "southLongitude",                  "SimplePrimitiveNode"               ); 
                    put( "westLongitude",                   "SimplePrimitiveNode"               ); 
                }};
            
            case "geographicCoverage":          
                return new Hashtable<String,String>() 
                {{ 
                    put( "city",                            "SimplePrimitiveNode"               ); 
                    put( "country",                         "SimpleControlledVocabularyNode"    ); 
                    put( "otherGeographicCoverage",         "SimplePrimitiveNode"               ); 
                    put( "state",                           "SimplePrimitiveNode"               ); 
                }};
            
            case "qualityValidity":             
                return new Hashtable<String,String>() 
                {{ 
                    put( "lineage",                         "MultiplePrimitiveNode"             ); 
                    put( "spatialResolution",               "MultiplePrimitiveNode"             ); 
                }};
            
            case "geographicalReferential":     
                return new Hashtable<String,String>() 
                {{ 
                    put( "level",                           "SimplePrimitiveNode"               ); 
                    put( "version",                         "SimplePrimitiveNode"               ); 
                }};
            
            case "socialScienceNotes":          
                return new Hashtable<String,String>() 
                {{ 
                    put( "socialScienceNotesSubject",       "SimplePrimitiveNode"               ); 
                    put( "socialScienceNotesText",          "SimplePrimitiveNode"               ); 
                    put( "socialScienceNotesType",          "SimplePrimitiveNode"               ); 
                }};
            
            case "targetSampleSize":            
                return new Hashtable<String,String>() 
                {{ 
                    put( "targetSampleActualSize",          "SimplePrimitiveNode"               ); 
                    put( "targetSampleSizeFormula",         "SimplePrimitiveNode"               ); 
                }};
            
            case "journalVolumeIssue":          
                return new Hashtable<String,String>() 
                {{ 
                    put( "journalIssue",                    "SimplePrimitiveNode"               ); 
                    put( "journalPubDate",                  "SimplePrimitiveNode"               ); 
                    put( "journalVolume",                   "SimplePrimitiveNode"               ); 
                }};
            
            case "source":                      
                return new Hashtable<String,String>() 
                {{ 
                    put( "ageOfSource",                     "MultiplePrimitiveNode"             ); 
                    put( "citations",                       "MultiplePrimitiveNode"             ); 
                    put( "experimentNumber",                "MultiplePrimitiveNode"             ); 
                    put( "typeOfSource",                    "MultiplePrimitiveNode"             ); 
                }};
            
            case "resourceVersion":             
                return new Hashtable<String,String>() 
                {{ 
                    put( "changes",                         "SimplePrimitiveNode"               ); 
                    put( "modificationDate",                "SimplePrimitiveNode"               ); 
                    put( "priorVersion",                    "MultiplePrimitiveNode"             ); 
                    put( "versionInfo",                     "SimplePrimitiveNode"               ); 
                    put( "versionStatus",                   "MultipleControlledVocabularyNode"  ); 
                }};
            
            default:                            
                return new Hashtable<String,String>();
        }
    }

    /**
     * <strong>listAuthorizedFieldsForBiomedicalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "biomedical"
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForBiomedicalNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForBiomedicalNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "studyAssayCellType",              "MultiplePrimitiveNode"                 );
            put( "studyAssayMeasurementType",       "MultipleControlledVocabularyNode"      );
            put( "studyAssayOrganism",              "MultipleControlledVocabularyNode"      );
            put( "studyAssayOtherMeasurmentType",   "MultiplePrimitiveNode"                 );
            put( "studyAssayOtherOrganism",         "MultiplePrimitiveNode"                 );
            put( "studyAssayPlatform",              "MultipleControlledVocabularyNode"      );
            put( "studyAssayPlatformOther",         "MultiplePrimitiveNode"                 );
            put( "studyAssayTechnologyType",        "MultipleControlledVocabularyNode"      );
            put( "studyAssayTechnologyTypeOther",   "MultiplePrimitiveNode"                 );
            put( "studyDesignType",                 "MultipleControlledVocabularyNode"      );
            put( "studyDesignTypeOther",            "MultiplePrimitiveNode"                 );
            put( "studyFactorType",                 "MultipleControlledVocabularyNode"      );
            put( "studyFactorTypeOther",            "MultiplePrimitiveNode"                 );
            put( "studyProtocolType",               "MultiplePrimitiveNode"                 );
            put( "studySampleType",                 "MultiplePrimitiveNode"                 );
        }};
    }

    /**
     * <strong>listAuthorizedFieldsForCitationNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "citation"
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForCitationNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForCitationNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "accessToSources",             "SimplePrimitiveNode"               );
            put( "alternativeTitle",            "SimplePrimitiveNode"               );
            put( "alternativeURL",              "SimplePrimitiveNode"               );
            put( "author",                      "MultipleCompoundNode"              );
            put( "characteristicOfSources",     "SimplePrimitiveNode"               );
            put( "contributor",                 "MultipleCompoundNode"              );
            put( "dataOrigin",                  "MultipleControlledVocabularyNode"  );
            put( "datasetContact",              "MultipleCompoundNode"              );
            put( "dataSources",                 "MultiplePrimitiveNode"             );
            put( "dateOfCollection",            "MultipleCompoundNode"              );
            put( "dateOfDeposit",               "SimplePrimitiveNode"               );
            put( "depositor",                   "SimplePrimitiveNode"               );
            put( "distributionDate",            "SimplePrimitiveNode"               );
            put( "distributor",                 "MultipleCompoundNode"              );
            put( "dsDescription",               "MultipleCompoundNode"              );
            put( "grantNumber",                 "MultipleCompoundNode"              );
            put( "keyword",                     "MultipleCompoundNode"              );
            put( "kindOfData",                  "MultipleControlledVocabularyNode"  );
            put( "kindOfDataOther",             "MultiplePrimitiveNode"             );
            put( "language",                    "MultipleControlledVocabularyNode"  );
            put( "lifeCycleStep",               "MultipleControlledVocabularyNode"  );
            put( "notesText",                   "SimplePrimitiveNode"               );
            put( "originOfSources",             "SimplePrimitiveNode"               );
            put( "otherId",                     "MultipleCompoundNode"              );
            put( "otherReferences",             "MultiplePrimitiveNode"             );
            put( "producer",                    "MultipleCompoundNode"              );
            put( "productionDate",              "SimplePrimitiveNode"               );
            put( "productionPlace",             "SimplePrimitiveNode"               );
            put( "project",                     "SimpleCompoundNode"                );
            put( "publication",                 "MultipleCompoundNode"              );
            put( "relatedDataset",              "MultipleCompoundNode"              );
            put( "relatedMaterial",             "MultiplePrimitiveNode"             );
            put( "series",                      "SimpleCompoundNode"                );
            put( "software",                    "MultipleCompoundNode"              );
            put( "subject",                     "MultipleControlledVocabularyNode"  );
            put( "subtitle",                    "SimplePrimitiveNode"               );
            put( "timePeriodCovered",           "MultipleCompoundNode"              );
            put( "title",                       "SimplePrimitiveNode"               );
            put( "topicClassification",         "MultiplePrimitiveNode"             );
        }};
    }

    /**
     * <strong>listAuthorizedFieldsForDerivedTextNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "Derived-text"
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForDerivedTextNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForDerivedTextNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "source", "MultipleCompoundNode" );
        }};
    }

    /**
     * <strong>listAuthorizedFieldsForGeospatialNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "geospatial"
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForGeospatialNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForGeospatialNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "conformity",             "MultipleCompoundNode"  );
            put( "geographicBoundingBox",  "MultipleCompoundNode"  );
            put( "geographicCoverage",     "MultipleCompoundNode"  );
            put( "geographicUnit",         "MultiplePrimitiveNode" );
            put( "qualityValidity",        "MultipleCompoundNode"  );
        }};
    }

    /**
     * <strong>listAuthorizedFieldsForJournalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "journal"
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForJournalNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForJournalNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "journalArticleType",      "SimpleControlledVocabularyNode"    );
            put( "journalVolumeIssue",      "MultipleCompoundNode"              );
        }};
    }

    /**
     * <strong>listAuthorizedFieldsForSemanticsNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "semantics"
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForSemanticsNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForSemanticsNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "bugDatabase",                "SimplePrimitiveNode"               );
            put( "designedForOntologyTask",    "MultipleControlledVocabularyNode"  );
            put( "endpoint",                   "SimplePrimitiveNode"               );
            put( "hasFormalityLevel",          "SimpleControlledVocabularyNode"    );
            put( "hasOntologyLanguage",        "MultipleControlledVocabularyNode"  );
            put( "knownUsage",                 "SimplePrimitiveNode"               );
            put( "imports",                    "MultiplePrimitiveNode"             );
            put( "resourceVersion",            "SimpleCompoundNode"                );
            put( "typeOfSR",                   "SimpleControlledVocabularyNode"    );
            put( "URI",                        "MultiplePrimitiveNode"             );
        }};
    }

    /**
     * <strong>listAuthorizedFieldsForSocialScienceNode</strong> est une méthode qui permet de récupérer la liste de tous les fields autorisés dans le noeud "socialscience"
     * @return la liste des fields autorisés
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,String> authorizedFields = AuthorizedFields.listAuthorizedFieldsForSocialScienceNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listAuthorizedFieldsForSocialScienceNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "actionsToMinimizeLoss",        "SimplePrimitiveNode"                 );
            put( "cleaningOperations",           "SimplePrimitiveNode"                 );
            put( "collectionMode",               "SimpleControlledVocabularyNode"      );
            put( "collectionModeOther",          "SimplePrimitiveNode"                 );
            put( "collectorTraining",            "SimplePrimitiveNode"                 );
            put( "controlOperations",            "SimplePrimitiveNode"                 );
            put( "dataCollectionSituation",      "SimplePrimitiveNode"                 );
            put( "dataCollector",                "SimplePrimitiveNode"                 );
            put( "datasetLevelErrorNotes",       "SimplePrimitiveNode"                 );
            put( "deviationsFromSampleDesign",   "SimplePrimitiveNode"                 );
            put( "frequencyOfDataCollection",    "SimplePrimitiveNode"                 );
            put( "geographicalReferential",      "MultipleCompoundNode"                );
            put( "otherDataAppraisal",           "SimplePrimitiveNode"                 );
            put( "researchInstrument",           "SimplePrimitiveNode"                 );
            put( "responseRate",                 "SimplePrimitiveNode"                 );
            put( "samplingErrorEstimates",       "SimplePrimitiveNode"                 );
            put( "samplingProcedure",            "SimpleControlledVocabularyNode"      );
            put( "samplingProcedureOther",       "SimplePrimitiveNode"                 );
            put( "socialScienceNotes",           "SimpleCompoundNode"                  );
            put( "targetSampleSize",             "SimpleCompoundNode"                  );
            put( "timeMethod",                   "SimpleControlledVocabularyNode"      );
            put( "timeMethodOther",              "SimplePrimitiveNode"                 );
            put( "unitOfAnalysis",               "MultipleControlledVocabularyNode"    );
            put( "unitOfAnalysisOther",          "SimplePrimitiveNode"                 );
            put( "universe",                     "MultiplePrimitiveNode"               );
            put( "weighting",                    "SimpleCompoundNode"                  );
        }};
    }
}
