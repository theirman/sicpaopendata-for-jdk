package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.Hashtable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;





/** Classe implémentant les nodes à valeur composée unique
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class SimpleCompoundNode extends BaseNode
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  

    /** Cette propriété contient la valeur du node */
    private Hashtable<String, BaseNode> value;





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      SimpleCompoundNode scn = new SimpleCompoundNode(); 
     * </pre>
     */
    public SimpleCompoundNode()
    {
        this.setMultiple(false);
        this.setTypeClass("compound");
        this.setTypeName("");
        this.setValue(new Hashtable<String,BaseNode>());
    }

    /**
     * Constructeur nécessitant deux paramètres : typeName et values
     * @param typeName  nom du node
     * @param value     valeur du node
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,BaseNode> ht = new Hashtable<String,BaseNode>();
     *      ht.put("field1", new SimplePrimitiveNode("field1", "value1"));
     *      ht.put("field2", new SimpleControlledVocabularyNode("field2", "value2"));
     *      ht.put("field3", new SimplePrimitiveNode("field3", "value3"));
     *          
     *      SimpleCompoundNode scn = new SimpleCompoundNode("name", ht);
     * </pre>
     */
    public SimpleCompoundNode(String typeName, Hashtable<String,BaseNode> value)
    {
        this.setMultiple(false);
        this.setTypeClass("compound");
        this.setTypeName(typeName);
        this.setValue(value);
    }


    
    
    
    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
     * @return valeur de l'attribut <strong>value</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String value = scn.getValue(); 
     * </pre>
     */
    public Hashtable<String,BaseNode> getValue()
    {
        return this.value;
    }
    
    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>value</strong>
     * @param value : valeur à enregistrer dans l'attribut <strong>value</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,BaseNode> ht = new Hashtable<String,BaseNode>();
     *      ht.put("field1", new SimplePrimitiveNode("field1", "value1"));
     *      ht.put("field2", new SimpleControlledVocabularyNode("field2", "value2"));
     *      ht.put("field3", new SimplePrimitiveNode("field3", "value3"));
     *          
     *      scn.setValue(ht);
     * </pre>
     */
    public void setValue(Hashtable<String,BaseNode> value)
    {
        this.value = value;

        if (!this.isValueValid())
        {
            this.value = new Hashtable<String,BaseNode>();
            return;
        }

        if (!this.areSubnodesValid())
        {
            this.value = new Hashtable<String,BaseNode>();
            return;
        }
    }
    
    
    
    
    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             

    /**
     * <strong>AreSubnodesValid</strong> est une méthode qui permet de s'assurer de la validité de tous les subnodes 
     * @return true si les subnodes sont valides, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      if(scn.AreSubnodesValid())
     *      {
     *          ...
     *      }
     * </pre>
     */
    private Boolean areSubnodesValid()
    {
        for(String key : this.value.keySet())
        {
            BaseNode subnode = (BaseNode)this.value.get(key);

          if (!AuthorizedFields.listAuthorizedFieldsForCategoryNode(this.getTypeName()).containsKey(subnode.getTypeName()))
              return false;

          if (!subnode.getClass().getName().contains(AuthorizedFields.getNodeType(subnode.getTypeName()).toString()))
              return false;        
        }

        return true;
    }

    /**
     * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
     * @return true si le node est valide, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      if(scn.isValid())
     *      {
     *          ...
     *      }
     * </pre>
     */
    public Boolean isValid()
    {
        return this.getMultiple()  instanceof Boolean   && !this.getMultiple()
            && this.getTypeClass() instanceof String    &&  this.getTypeClass() == "compound"
            && this.getTypeName()  instanceof String
            && this.getValue()     instanceof Hashtable && this.isValueValid()
            && this.areSubnodesValid();
    }

    /**
     * <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur <br/>
     * @return true si la valeur est valide, false sinon
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      if(this.isValueValid())
     *      {
     *          ...
     *      }
     * </pre>
     */
    private Boolean isValueValid()
    {
        for(String key : this.value.keySet())
        {
            BaseNode node = (BaseNode)this.value.get(key);

            if (node instanceof SimplePrimitiveNode
                || node instanceof SimpleControlledVocabularyNode
                || node instanceof MultiplePrimitiveNode
                || node instanceof MultipleControlledVocabularyNode
               )
                continue;
            else
                return false;
        }

        return true;
    }

    /**
     * <strong>toJSON</strong> est une méthode qui permet d'obtenir une réprésentation JSON du node
     * @return la représentation du node sous forme de chaine JSON
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = scn.toJSON(); 
     * </pre>
     */
    public String toJSON()
    {
        if (!this.isValid())
            return "";

        return new GsonBuilder().setPrettyPrinting()
                                .create()
                                .toJson(this);
    }

    /**
     * <strong>toString</strong> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)
     * @return la représentation du node sous forme de chaine de caractère
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = scn.toString(); 
     * </pre>
     */
    public String toString()
    {
        if (!this.isValid())
            return "";

        return new Gson().toJson(this);
    }

    
    
    
    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //





}
