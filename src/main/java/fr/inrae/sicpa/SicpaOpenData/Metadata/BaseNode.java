package fr.inrae.sicpa.SicpaOpenData.Metadata;





/** Classe implémentant les nodes de base
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class BaseNode
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  

    /** Cette propriété indique si l'on est en présence d'un noeud mono- ou multi-valeur(s) */
    private Boolean multiple;

    /** Cette propriété indique le type de la valeur attendue */
    private String typeClass;

    /** Cette propriété identifie la valeur */
    private String typeName;


    
    
    
    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 * 		BaseNode bn = new BaseNode();
	 * </pre>
     */
    public BaseNode()
    {
        this.setMultiple(false);
        this.setTypeClass("");
        this.setTypeName("");
    }
    
    
    
    
    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>multiple</strong> 
     * @return valeur de l'attribut <strong>multiple</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 * 		Boolean multiple = bn.getMultiple(); 
	 * </pre>
     */
    public Boolean getMultiple()
    {
        return this.multiple;
    }
    
    /**
     * Permet d'obtenir la valeur de l'attribut <strong>typeClass</strong> 
     * @return valeur de l'attribut <strong>typeClass</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 * 		String typeClass = bn.getTypeClass(); 
	 * </pre>
     */
    public String getTypeClass()
    {
        return this.typeClass;
    }

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>typeName</strong> 
     * @return valeur de l'attribut <strong>typeName</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	String typeName = bn.getTypeName();
	 * </pre>
     */
    public String getTypeName()
    {
        return this.typeName;
    }

    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>multiple</strong>
     * @param multiple : valeur à enregistrer dans l'attribut <strong>multiple</strong>
     * 
     * <hr>
	 * <strong>Exemple : </strong>
     * <pre>
	 *  	bn.setMultiple(true); 
	 * </pre>
     */
    protected void setMultiple(Boolean multiple)
    {
        this.multiple = multiple;
    }
    
    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>typeClass</strong>
     * @param typeClass : valeur à enregistrer dans l'attribut <strong>typeClass</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 * 		bn.setTypeClass("compound"); 
	 * </pre>
     */
    protected void setTypeClass(String typeClass)
    {
        this.typeClass = typeClass;
    }

    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>typeName</strong>
     * @param typeName : valeur à enregistrer dans l'attribut <strong>typeName</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
	 *  	bn.setTypeName("language"); 
	 * </pre>
     */
    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }


    
    
    
    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             



    
    
    
    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //





}
