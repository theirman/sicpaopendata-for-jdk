package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.ArrayList;
import java.util.List;






/** Classe implémentant les nodes catégorie
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class CategoryNode
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  

    /** Cette propriété indique la valeur à afficher dans le fichier de métadonnées pour ce node */
    protected String displayName;

    /** Cette propriété permet de gérer la liste des valeurs pour le node */
    protected ArrayList<BaseNode> fields;


    
    
    
    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      CategoryNode cn = new CategoryNode();
     * </pre>
     */
     public CategoryNode()
    {
        this.setDisplayName("");
        this.initFields();
    }


    
    
    
    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>displayName</strong> 
     * @return valeur de l'attribut <strong>displayName</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     String displayName = cn.getDisplayName(); 
     * </pre>
     */
    public String getDisplayName()
    {
        return this.displayName;
    }
    
    /**
     * Permet d'obtenir la valeur de l'attribut <strong>fields</strong> 
     * @return valeur de l'attribut <strong>fields</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      ArrayList<BaseNode> fields = cn.getFields(); 
     * </pre>
     */
    public ArrayList<BaseNode> getFields()
    {
        return this.fields;
    }
    
    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>displayName</strong> 
     * @param displayName   : valeur à enregistrer dans l'attribut <strong>fields</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      cn.setDisplayName("displayName"); 
     * </pre>
     */
    protected void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }
    
    
    
    
       
    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             

    /**
     * <strong>clearFields</strong> est une méthode qui permet de réinitialiser la liste des fields du node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      cn.clearFields();
     * </pre>
     */
    public void clearFields()
    {
        this.fields.clear();
    }

    /**
     * <strong>containsField</strong> est une méthode qui permet de vérifier si un field existe dans la liste des fields du node
     * @param fieldName : valeur à vérifier
     * @return true si la valeur appartient à la liste de valeurs du node, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Boolean contains =  cn.containsFields("fieldName");
     * </pre>
     */
    public Boolean containsField(String fieldName)
    {
        return this.indexOfField(fieldName) >= 0;
    }

    /**
     * <strong>fieldNames</strong> est une méthode qui permet de récupérer la liste des noms des fields du node
     * @return la liste des noms des fields
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      List<String> fnList = cn.fieldNames();
     * </pre>
     */
    public List<String> fieldNames()
    {
        List<String> fieldNames = new ArrayList<String>();

        for (BaseNode node : this.fields)
            fieldNames.add(node.getTypeName());

        return fieldNames;
    }

    /**
     * <strong>indexOfField</strong> est une méthode qui permet de récupérer l'index d'un field
     * @param fieldName : valeur à rechercher
     * @return index de la valeur
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      int index = cn.indexOfField("fieldName");
     * </pre>
     */
    public int indexOfField(String fieldName)
    {
        for(int index=0; index < this.fields.size(); index++)
        {
            BaseNode node = (BaseNode)this.fields.get(index);

            if (node.getTypeName() == fieldName)
                return index;
        }

        return -1;
    }

    /**
     * <strong>initField</strong> est une méthode qui permet d'initialiser la liste des fields du node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      cn.initFields();
     * </pre>
     */
    public void initFields()
    {
        this.fields = new ArrayList<BaseNode>();
    }

    /**
     * <strong>removeField</strong> est une méthode qui permet de supprimer un field de la liste des fields
     * @param fieldName : valeur à supprimer
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      cn.removeField("fieldName");
     * </pre>
     */
    public void removeField(String fieldName)
    {
        try
        {
            this.fields.remove(this.indexOfField(fieldName));
        }
        catch(Exception e)
        {
        }
    }

    /**
     * <strong>removeFieldAt</strong> est une méthode qui permet de supprimer le field située à un index précis
     * @param index     : index de la valeur à supprimer
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      cn.removeFieldAt(3);
     * </pre>
     */
    public void removeFieldAt(int index)
    {
        try
        {
            this.fields.remove(index);
        }
        catch(Exception e)
        {
        }
    }

    /**
     * <strong>removeAllFields</strong> est une méthode qui permet de supprimer tous les fields de la liste des fields
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      cn.removeAllFields();
     * </pre>
     */
    public void removeAllFields()
    {
        this.fields.clear();
    }





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //




}
