package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;





/** Classe implémentant les nodes à valeur textuelle unique
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class SimpleControlledVocabularyNode extends BaseNode
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  

    /** Cette propriété contient la valeur du node */
    private String value;
    
    
    
    
    
    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      SimpleControlledVocabularyNode scvn = new SimpleControlledVocabularyNode(); 
     * </pre>
     */
    public SimpleControlledVocabularyNode()
    {
        this.setMultiple(false);
        this.setTypeClass("controlledVocabulary");
    }

    /**
     * Constructeur nécessitant deux paramètres : typeName et values
     * @param typeName  nom du node
     * @param value     valeur du node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      SimpleControlledVocabularyNode scvn = new SimpleControlledVocabularyNode("name", "controlledValue"); 
     * </pre>
     */
    public SimpleControlledVocabularyNode(String typeName, String value)
    {
        this.setMultiple(false);
        this.setTypeClass("controlledVocabulary");
        this.setTypeName(typeName);
        this.setValue(value);
    }


    
    
    
    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
     * @return valeur de l'attribut <strong>value</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String value = scvn.getValue(); 
     * </pre>
     */
    public String getValue()
    {
        return this.value;
    }

    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>value</strong>
     * @param value : valeur à enregistrer dans l'attribut <strong>value</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      scvn.setValue("controlledValue"); 
     * </pre>
     */
    public void setValue(String value)
    {
        if (this.isAuthorizedValue(value))
            this.value = value;
        else
            this.value = "";
    }


    
    
    
    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             

    /**
     * <strong>isAuthorizedValue</strong> est une méthode qui permet de s'assurer que la valeur en paramètre est bien une valeur autorisée par le vocabulaire contrôlé
     * @param value la valeur à tester
     * @return true si la valeur est autorisée, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if(scvn.isAuthorizedValue("value"))
     *      {
     *          ...
     *      }
     * </pre>
     */
    public Boolean isAuthorizedValue(String value)
    {
        ArrayList<String> controlledVocabulary = ControlledVocabulary.listValuesFor(this.getTypeName());
        return controlledVocabulary.contains(value);
    }

    /**
     * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
     * @return true si le node est valide, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if(scvn.isValid())
     *      {
     *          ...
     *      }
     * </pre>
     */
    public Boolean isValid()
    {
        return this.getMultiple() instanceof Boolean && !this.getMultiple()
            && this.getTypeClass() instanceof String && this.getTypeClass() == "controlledVocabulary"
            && this.getTypeName() instanceof String
            && this.getValue() instanceof String;
    }

    /**
     * <strong>toJSON</strong> est une méthode qui permet d'obtenir une réprésentation JSON du node
     * @return la représentation du node sous forme de chaine JSON
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = scvn.toJSON(); 
     * </pre>
     */
    public String toJSON()
    {
        if (!this.isValid())
            return "";

        return new GsonBuilder().setPrettyPrinting()
                                .create()
                                .toJson(this);
    }

    /**
     * <strong>toString</strong> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)
     * @return la représentation du node sous forme de chaine de caractère
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = scvn.toString(); 
     * </pre>
     */
    public String toString()
    {
        if (!this.isValid())
            return "";

        return new Gson().toJson(this);
    }

    
    
    
    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //





}
