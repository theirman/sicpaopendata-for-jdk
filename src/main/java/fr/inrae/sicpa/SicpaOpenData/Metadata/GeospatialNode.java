package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.Hashtable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/** Classe implémentant le node Geospatial
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class GeospatialNode extends CategoryNode
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  


    /** Cette propriété recense la liste des fields requis pour ce node */
    private transient Hashtable<String,String> authorizedFields;

    /** Cette propriété recense la liste des fields requis pour ce node */
    private transient Hashtable<String,String> requiredFields;



    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      GeospatialNode cn = new GeospatialNode(); 
     * </pre>
     */
    public GeospatialNode()
    {
        this.setDisplayName("Geospatial Metadata");
        this.initFields();
        this.authorizedFields   = AuthorizedFields.listAuthorizedFieldsForGeospatialNode();
        this.requiredFields     = RequiredFields.listRequiredFieldsForGeospatialNode();
    }


    
    
    
    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //





    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             

    /**
     * <strong>addField</strong> est une méthode qui permet d'ajouter un noeud de type MultipleCompoundNode à la liste des fields du node
     * @param node : node à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleCompoundNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,BaseNode> value1 = new Hashtable<String,BaseNode>()
     *      {{
     *          put("name11", new SimplePrimitiveNode("name11", "value11"));
     *          put("name12", new SimplePrimitiveNode("name12", "value12"));
     *      }};
     *  
     *      Hashtable<String,BaseNode> value2 = new Hashtable<String,BaseNode>()
     *      {{
     *          put("name21", new SimplePrimitiveNode("name21", "value21"));
     *          put("name22", new SimplePrimitiveNode("name22", "value22"));
     *      }};
     *          
     *      ArrayList<Hashtable<String,BaseNode>> values = new ArrayList<Hashtable<String,BaseNode>>() {{ add(value1); add(value2); }};
     *          
     *      GeospatialNode cn = new GeospatialNode();
     *      cn.addField(new MultipleCompoundNode("name", values));
     * </pre>
     */
    public Boolean addField(MultipleCompoundNode node)
    {
        if (!node.isValid() || !this.authorizedFields.containsKey(node.getTypeName()) || this.authorizedFields.get(node.getTypeName()) != "MultipleCompoundNode")
            return false;

        this.fields.add(node);
        return this.fields.contains(node);
    }

    /**
     * <strong>addField</strong> est une méthode qui permet d'ajouter un noeud de type MultipleControlledVocabularyNode à la liste des fields du node
     * @param node : node à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultipleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      List<String> values = new List<String>()
     *      {{
     *          add("controlledValue1");
     *          add("controlledValue2");
     *          add("controlledValue3");
     *      }};
     *          
     *      GeospatialNode cn = new GeospatialNode();
     *      cn.addField(new MultipleControlledVocabularyNode("name", values));
     * </pre>
     */
    public Boolean addField(MultipleControlledVocabularyNode node)
    {
        if (!node.isValid() || !this.authorizedFields.containsKey(node.getTypeName()) || this.authorizedFields.get(node.getTypeName()) != "MultipleControlledVocabularyNode")
            return false;

        this.fields.add(node);
        return this.fields.contains(node);
    }

    /**
     * <strong>addField</strong> est une méthode qui permet d'ajouter un noeud de type MultiplePrimitiveNode à la liste des fields du node
     * @param node : node à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.MultiplePrimitiveNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      List<String> values = new List<String>()
     *      {{
     *          add("value1");
     *          add("value2");
     *          add("value3");
     *      }};
     *          
     *      GeospatialNode cn = new GeospatialNode();
     *      cn.addField(new MultiplePrimitiveNode("name", values));
     * </pre>
     */
    public Boolean addField(MultiplePrimitiveNode node)
    {
        if (!node.isValid() || !this.authorizedFields.containsKey(node.getTypeName()) || this.authorizedFields.get(node.getTypeName()) != "MultiplePrimitiveNode")
            return false;

        this.fields.add(node);
        return this.fields.contains(node);
    }

    /**
     * <strong>addField</strong> est une méthode qui permet d'ajouter un noeud de type SimpleCompoundNode à la liste des fields du node
     * @param node : node à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleCompoundNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,BaseNode> value = new Hashtable<String,BaseNode>()
     *      {{
     *          put("name1", new SimplePrimitiveNode("name1", "value1"));
     *          put("name2", new SimplePrimitiveNode("name2", "value2"));
     *          put("name3", new SimplePrimitiveNode("name3", "value3"));
     *      }};
     *  
     *      GeospatialNode cn = new GeospatialNode();
     *      cn.addField(new SimpleCompoundNode("name", value));
     * </pre>
     */
    public Boolean addField(SimpleCompoundNode node)
    {
        if (!node.isValid() || !this.authorizedFields.containsKey(node.getTypeName()) || this.authorizedFields.get(node.getTypeName()) != "SimpleCompoundNode")
            return false;

        this.fields.add(node);
        return this.fields.contains(node);
    }

    /**
     * <strong>addField</strong> est une méthode qui permet d'ajouter un noeud de type SimpleControlledVocabularyNode à la liste des fields du node
     * @param node : node à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimpleControlledVocabularyNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      GeospatialNode cn = new GeospatialNode();
     *      cn.addField(new SimpleControlledVocabularyNode("name", "controlledValue"));
     * </pre>
     */
    public Boolean addField(SimpleControlledVocabularyNode node)
    {
        if (!node.isValid() || !this.authorizedFields.containsKey(node.getTypeName()) || this.authorizedFields.get(node.getTypeName()) != "SimpleControlledVocabularyNode")
            return false;

        this.fields.add(node);
        return this.fields.contains(node);
    }

    /**
     * <strong>addField</strong> est une méthode qui permet d'ajouter un noeud de type SimplePrimitiveNode à la liste des fields du node
     * @param node : node à ajouter 
     * @see fr.inrae.sicpa.SicpaOpenData.Metadata.SimplePrimitiveNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      GeospatialNode cn = new GeospatialNode();
     *      cn.addField(new SimplePrimitiveNode("name", "value"));
     * </pre>
     */
    public Boolean addField(SimplePrimitiveNode node)
    {
        if (!node.isValid() || !this.authorizedFields.containsKey(node.getTypeName()) || this.authorizedFields.get(node.getTypeName()) != "SimplePrimitiveNode")
            return false;

        this.fields.add(node);
        return this.fields.contains(node);
    }

    /**
     * <strong>isValid</strong>  est une méthode qui permet de s'assurer de la validité du node
     * @return true si le node est valide, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if(gn.isValid)
     *      {
     *          ...
     *      }
     * </pre>
     */
    public Boolean isValid()
    {
        Boolean isRequiredPresent   = true;
        Boolean isPresentAuthorized = true;

        for (String key : this.requiredFields.keySet())
        {
            if (!this.fieldNames().contains(key))
            {
                isRequiredPresent = false;
                break;
            }
        }

        for(BaseNode field : this.fields)
        {
            if (!this.authorizedFields.containsKey(field.getTypeName()))
            {
                isPresentAuthorized = false;
                break;
            }

            if (!field.getClass().getName().contains(this.authorizedFields.get(field.getTypeName())))
            {
                isPresentAuthorized = false;
                break;
            }
        }

        return this.getDisplayName() == "Geospatial Metadata" && isRequiredPresent && isPresentAuthorized;
    }

    /**
     * <strong>toJSON</strong> est une méthode qui permet d'obtenir une réprésentation JSON du node
     * @return la représentation du node sous forme de chaine JSON
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String json = gn.toJSON(); 
     * </pre>
     */
    public String toJSON()
    {
        if (!this.isValid())
            return "";

        return new GsonBuilder().setPrettyPrinting()
                                .create()
                                .toJson(this);
    }

    /**
     * <strong>toString</strong> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)
     * @return la représentation du node sous forme de chaine de caractère
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String json = gn.toString(); 
     * </pre>
     */
    public String toString()
    {
        if (!this.isValid())
            return "";

        return new Gson().toJson(this);
    }





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //



}
