package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.Hashtable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/** Classe implémentant la construction du document JSON des métadonnées
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class MetadataDocument
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  

    /** Cette propriété indique la licence du dataset */
    private String license;

    /** Cette propriété indique les termes d'utilisation du dataset */
    private String termsOfUse;

    /** Cette propriété permet de gérer la liste des category nodes du document */
    private Hashtable<String,CategoryNode> metadataBlocks;




    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      MetadataDocument md = new MetadataDocument();
     * </pre>
     */
    public MetadataDocument()
    {
        this.setLicense("NONE");
        this.setTermsOfUse("<img src=\"https://www.etalab.gouv.fr/wp-content/uploads/2011/10/licence-ouverte-open-licence.gif\" alt=\"Licence Ouverte\" height=\"100\">" +
                                "<a href=\"https://www.etalab.gouv.fr/licence-ouverte-open-licence\">Licence Ouverte / Open Licence Version 2.0</a> " +
                                "compatible CC BY");
        this.metadataBlocks = new Hashtable<String,CategoryNode>();
    }

    /**
     * Constructeur nécessitant deux paramètres : license et termsOfUse
     * @param license : valeur à enregistrer dans l'attribut <strong>license</strong>
     * @param termsOfUse : valeur à enregistrer dans l'attribut <strong>termsOfUse</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      MetadataDocument md = new MetadataDocument("license", "termsOfUse");
     * </pre>
     */
    public MetadataDocument(String license, String termsOfUse)
    {
        this.setLicense(license);
        this.setTermsOfUse(termsOfUse);
        this.metadataBlocks = new Hashtable<String,CategoryNode>();
    }


    
    
    
    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>license</strong> 
     * @return valeur de l'attribut <strong>license</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String license = md.getLicense();
     * </pre>
     */
    public String getLicense()
    {
        return this.license;
    }

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>metadataBlocks</strong> 
     * @return valeur de l'attribut <strong>metadataBlocks</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      Hashtable<String,CategoryNode> metadataBlocks = md.getMetadataBlocks();
     * </pre>
     */
    public Hashtable<String,CategoryNode> getMetadataBlocks()
    {
        return this.metadataBlocks;
    }

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>termsOfUse</strong> 
     * @return valeur de l'attribut <strong>termsOfUse</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String value = md.getTermsOfUse();
     * </pre>
     */
    public String getTermsOfUse()
    {
        return this.termsOfUse;
    }

    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>license</strong>
     * @param license : valeur à enregistrer dans l'attribut <strong>license</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      md.setLicense("license"); 
     * </pre>
     */
    public void setLicense(String license)
    {
        this.license = license;
    }

    /**
     * Permet de mettre à jour la valeur de l'attribut <strong>termsOfUse</strong>
     * @param termsOfUse : valeur à enregistrer dans l'attribut <strong>termsOfUse</strong>
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      md.setTermsOfUse("termsOfUse"); 
     * </pre>
     */
    public void setTermsOfUse(String termsOfUse)
    {
        this.termsOfUse = termsOfUse;
    }


    
    
    
    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             


    /**
     * <strong>addBiomedicalNode</strong> est une méthode qui permet d'ajouter le node biomedical à la liste des category nodes
     * @param biomedicalNode : la valeur du node biomedical
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     BiomedicalNode bn = new BiomedicalNode();
     *     ...
     *     ...
     *     
     *     md.addBiomedicalNode(bn);
     * </pre>
     */
    public void addBiomedicalNode(BiomedicalNode biomedicalNode)
    {
        this.metadataBlocks.put("biomedical", biomedicalNode);
    }

    /**
     * <strong>addCitationNode</strong> est une méthode qui permet d'ajouter le node citation à la liste des category nodes
     * @param citationNode : la valeur du node citationNode
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     CitationNode cn = new CitationNode();
     *     ...
     *     ...
     *     
     *     md.addCitationNode(cn);
     * </pre>
     */
    public void addCitationNode(CitationNode citationNode)
    {
        this.metadataBlocks.put("citation", citationNode);
    }

    /**
     * <strong>addDerivedTextNode</strong> est une méthode qui permet d'ajouter le node derivedText à la liste des category nodes
     * @param derivedTextNode : la valeur du node derivedText
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     DerivedTextNode dtn = new DerivedTextNode();
     *     ...
     *     ...
     *     
     *     md.addDerivedTextNode(dtn);
     * </pre>
     */
    public void addDerivedTextNode(DerivedTextNode derivedTextNode)
    {
        this.metadataBlocks.put("Derived-text", derivedTextNode);
    }

    /**
     * <strong>addGeospatialNode</strong> est une méthode qui permet d'ajouter le node geospatial à la liste des category nodes
     * @param geospatialNode : la valeur du node geospatial
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     GeospatialNode gn = new GeospatialNode();
     *     ...
     *     ...
     *     
     *     md.addGeospatialNode(gn);
     * </pre>
     */
    public void addGeospatialNode(GeospatialNode geospatialNode)
    {
        this.metadataBlocks.put("geospatial", geospatialNode);
    }

    /**
     * <strong>addJournalNode</strong> est une méthode qui permet d'ajouter le node journal à la liste des category nodes
     * @param journalNode : la valeur du node journal
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     JournalNode jn = new JournalNode();
     *     ...
     *     ...
     *     
     *     md.addJournalNode(jn);
     * </pre>
     */
    public void addJournalNode(JournalNode journalNode)
    {
        this.metadataBlocks.put("journal", journalNode);
    }

    /**
     * <strong>addSemanticsNode</strong> est une méthode qui permet d'ajouter le node semantics à la liste des category nodes
     * @param semanticsNode : la valeur du node semantics
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     SemanticsNode sn = new SemanticsNode();
     *     ...
     *     ...
     *     
     *     md.addSemanticsNode(sn);
     * </pre>
     */
    public void addSemanticsNode(SemanticsNode semanticsNode)
    {
        this.metadataBlocks.put("semantics", semanticsNode);
    }

    /**
     * <strong>addSocialScienceNode</strong> est une méthode qui permet d'ajouter le node socialScience à la liste des category nodes
     * @param socialScienceNode : la valeur du node socialScience
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *     SocialScienceNode ssn = new SocialScienceNode();
     *     ...
     *     ...
     *     
     *     md.addSocialScienceNode(ssn);
     * </pre>
     */
    public void addSocialScienceNode(SocialScienceNode socialScienceNode)
    {
        this.metadataBlocks.put("socialscience", socialScienceNode);
    }

    /**
     * <strong>toJSON</strong> est une méthode qui permet d'obtenir une réprésentation JSON du document
     * @return la représentation du document sous forme de chaine JSON
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String json = md.toJSON(); 
     * </pre>
     */
    public String toJSON()
    {
        Hashtable<String,Object> fields = new Hashtable<String,Object>();
        fields.put("license", this.getLicense());
        fields.put("termsOfUse", this.getTermsOfUse());
        fields.put("metadataBlocks", this.getMetadataBlocks());

        Hashtable<String, Hashtable<String,Object>> document = new Hashtable<String, Hashtable<String,Object>>();
        document.put("datasetVersion", fields);

        return new GsonBuilder().setPrettyPrinting()
                .create()
                .toJson(document);
    }

    /**
     * <strong>toString</strong> est une méthode qui permet d'obtenir une réprésentation textuelle du document (JSON minifié)
     * @return la représentation du document sous forme de chaine de caractère
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre>
     *      String json = md.toString(); 
     * </pre>
     */
    public String toString()
    {
        Hashtable<String,Object> fields = new Hashtable<String,Object>();
        fields.put("license", this.getLicense());
        fields.put("termsOfUse", this.getTermsOfUse());
        fields.put("metadataBlocks", this.getMetadataBlocks());

        Hashtable<String, Hashtable<String,Object>> document = new Hashtable<String, Hashtable<String,Object>>();
        document.put("datasetVersion", fields);

        return new Gson().toJson(document);
    }







    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //



}