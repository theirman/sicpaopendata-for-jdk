package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;





/** Classe implémentant les nodes à valeurs textuelles multiples
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class MultiplePrimitiveNode extends BaseNode
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  

    /** Cette propriété contient la valeur du node */
    private List<String> value;





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          

    /**
     * Constructeur sans paramètre
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      MultiplePrimitiveNode mpn = new MultiplePrimitiveNode(); 
     * </pre>
     */
    public MultiplePrimitiveNode()
    {
        this.setMultiple(true);
        this.setTypeClass("primitive");
        this.setTypeName("");

        this.value = new ArrayList<String>();
    }
    
    /**
     * Constructeur nécessitant deux paramètres : typeName et values
     * @param typeName : nom du node
     * @param values : valeurs du node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      List<String> values = new ArrayList<String>();
     *      values.add("value1");
     *      values.add("value2");
     *      values.add("value3");
     *      
     *      MultiplePrimitiveNode mpn = new MultiplePrimitiveNode("name", values);     
     * </pre>
     */
    public MultiplePrimitiveNode(String typeName, List<String> values)
    {
        this.setMultiple(true);
        this.setTypeClass("primitive");
        this.setTypeName(typeName);
        this.value = new ArrayList<String>();

        this.addValues(values);
    }





    //     ___  ____________________________  _____  ____
    //    / _ |/ ___/ ___/ __/ __/ __/ __/ / / / _ \/ __/
    //   / __ / /__/ /__/ _/_\ \_\ \/ _// /_/ / , _/\ \  
    //  /_/ |_\___/\___/___/___/___/___/\____/_/|_/___/  
    //

    /**
     * Permet d'obtenir la valeur de l'attribut <strong>value</strong> 
     * @return valeur de l'attribut <strong>value</strong> 
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      List<String> value = mpn.getValue(); 
     * </pre>
     */
    public List<String> getValue()
    {
        return this.value;
    }


    
    
    
    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             

    /**
     * <strong>addValue</strong> est une méthode qui permet d'ajouter une valeur à la liste des valeurs du node
     * @param value : valeur à ajouter
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.addValue("value"); 
     * </pre>
     */
    public void addValue(String value)
    {
        this.value.add(value);
    }

    /**
     * <strong>addValues</strong> est une méthode qui permet d'ajouter une liste de valeurs à la liste des valeurs du node
     * @param values : valeurs à ajouter
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      List<String> values = new ArrayList<String>();
     *      values.add("value1");
     *      values.add("value2");
     *      values.add("value3");
     *      
     *      mpn.addValues(values);
     * </pre>
     */
    public void addValues(List<String> values)
    {
        for(String value : values)
            this.addValue(value);
    }

    /**
     * <strong>clearValues</strong> est une méthode qui permet de réinitialiser la liste des valeurs du node
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.clearValues(); 
     * </pre>
     */
    public void clearValues()
    {
        this.value.clear();
    }

    /**
     * <strong>containsValue</strong> est une méthode qui permet de vérifier si une valeur existe dans la liste des valeurs du node
     * @param value : valeur à vérifier
     * @return true si la valeur appartient à la liste de valeurs du node, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.containsValue("value"); 
     * </pre>
     */
    public Boolean containsValue(String value)
    {
        return this.value.contains(value);
    }

    /**
     * <strong>getValueAt</strong> est une méthode qui permet de récupérer la valeur située à un index précis
     * @param index : index de la valeur à récupérer
     * @return valeur situé à l'index
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.getValueAt(3); 
     * </pre>
     */
    public String getValueAt(int index)
    {
        try
        {
            return this.value.get(index);
        }
        catch(Exception e)
        {
            return "";
        }
    }

    /**
     * <strong>indexOf</strong> est une méthode qui permet de récupérer l'index d'une valeur
     * @param value : valeur à rechercher
     * @return index de la valeur
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.indexOf("value"); 
     * </pre>
     */
    public int indexOf(String value)
    {
        return this.value.indexOf(value);
    }

    /**
     * <strong>isValid</strong> est une méthode qui permet de s'assurer de la validité du node
     * @return true si le node est valide, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      if(mpn.isValid())
     *      {
     *          ...
     *      }
     * </pre>
     */
    public Boolean isValid()
    {
        return this.getMultiple()  instanceof Boolean       && this.getMultiple()
            && this.getTypeClass() instanceof String        && this.getTypeClass() == "primitive"
            && this.getTypeName()  instanceof String
            && this.getValue()     instanceof ArrayList     && this.isValueValid();
    }

    /**
     * <strong>isValueValid</strong> est une méthode qui permet de s'assurer de la validité de la valeur <br/>
     * => Chaque élément de la liste de valeurs du node doit être une chaîne de caractères
     * @return true si la valeur est valide, false sinon
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *          if(this.isValueValid())
     *          {
     *              ...
     *          }
     * </pre>
     */
    private Boolean isValueValid()
    {
        for (String val : this.value)
            if (!(val instanceof String))
                return false;

        return true;
    }

    /**
     * <strong>removeAll</strong> est une méthode qui permet de supprimer toutes les valeurs de la liste des valeurs
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.removeAll(); 
     * </pre>
     */
    public void removeAll()
    {
        this.clearValues();
    }

    /**
     * <strong>removeValue</strong> est une méthode qui permet de supprimer une valeur de la liste des valeurs
     * @param value : valeur à supprimer
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.removeValue("value"); 
     * </pre>
     */
    public void removeValue(String value)
    {
        try
        {
            this.value.remove(value);
        }
        catch(Exception e)
        {
        }
    }

    /**
     * <strong>removeValueAt</strong> est une méthode qui permet de supprimer la valeur située à un index précis
     * @param index : index de la valeur à supprimer
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      mpn.removeValueAt(3); 
     * </pre>
     */
    public void removeValueAt(int index)
    {
        try
        {
            this.value.remove(index);
        }
        catch(Exception e)
        {
        }
    }

    /**
     * <strong>toJSON</strong> est une méthode qui permet d'obtenir une réprésentation JSON du node
     * @return la représentation du node sous forme de chaine JSON
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = mpn.toJSON(); 
     * </pre>
     */
    public String toJSON()
    {
        if (!this.isValid())
            return "";

        return new GsonBuilder().setPrettyPrinting()
                                .create()
                                .toJson(this);
    }

    /**
     * <strong>toString</strong> est une méthode qui permet d'obtenir une réprésentation textuelle du node (JSON minifié)
     * @return la représentation du node sous forme de chaine de caractère
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      String json = mpn.toString(); 
     * </pre>
     */
    public String toString()
    {
        if (!this.isValid())
            return "";

        return new Gson().toJson(this);
    }

    



    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //





}
    
    