package fr.inrae.sicpa.SicpaOpenData.Metadata;

import java.util.Hashtable;

/** Classe détaillant les valeurs requises par nodes
 *  @author Thierry HEIRMAN
 *  @since Juin 2021
 */
public class RequiredFields
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          





    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //


   /**
     * <strong>listRequiredFields</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le document des métadonnées
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFields(); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFields()
    {
        Hashtable<String,String> requiredFields = new Hashtable<String,String>();
        requiredFields.putAll(RequiredFields.listRequiredFieldsForBiomedicalNode());
        requiredFields.putAll(RequiredFields.listRequiredFieldsForCitationNode());
        requiredFields.putAll(RequiredFields.listRequiredFieldsForDerivedTextNode());
        requiredFields.putAll(RequiredFields.listRequiredFieldsForGeospatialNode());
        requiredFields.putAll(RequiredFields.listRequiredFieldsForJournalNode());
        requiredFields.putAll(RequiredFields.listRequiredFieldsForSemanticsNode());
        requiredFields.putAll(RequiredFields.listRequiredFieldsForSocialScienceNode());
        return requiredFields;
    }

    /**
     * <strong>listRequiredFields</strong> est une méthode qui permet de récupérer la liste de tous les fields requis pour un node donné
     * @param node : le node pour lequel lister les champs requis
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFields("citation"); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFields(String node)
    {
        switch (node)
        {
            case "biomedical":
                return RequiredFields.listRequiredFieldsForBiomedicalNode();

            case "citation":
                return RequiredFields.listRequiredFieldsForCitationNode();

            case "Derived-text":
                return RequiredFields.listRequiredFieldsForDerivedTextNode();

            case "geospatial":
                return RequiredFields.listRequiredFieldsForGeospatialNode();

            case "journal":
                return RequiredFields.listRequiredFieldsForJournalNode();

            case "semantics":
                return RequiredFields.listRequiredFieldsForSemanticsNode();

            case "socialscience":
                return RequiredFields.listRequiredFieldsForSocialScienceNode();

            default:
                return RequiredFields.listRequiredFields();
        }
    }

    /**
     * <strong>listRequiredFieldsForBiomedicalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "biomedical"
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFieldsForBiomedicalNode(); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFieldsForBiomedicalNode()
    {
        return new Hashtable<String,String>();
    }

    /**
     * <strong>listRequiredFieldsForCitationNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "citation"
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFieldsForCitationNode(); 
     * </pre>
     */
    @SuppressWarnings("serial")
    public static Hashtable<String,String> listRequiredFieldsForCitationNode()
    {
        return new Hashtable<String,String>()
        {{
            put( "title",                      "SimplePrimitiveNode"               );
            put( "author",                     "MultipleCompoundNode"              );
            put( "datasetContact",             "MultipleCompoundNode"              );
            put( "dsDescription",              "MultipleCompoundNode"              );
            put( "subject",                    "MultipleControlledVocabularyNode"  );
            put( "kindOfData",                 "MultipleControlledVocabularyNode"  );
        }};
    }

    /**
     * <strong>listRequiredFieldsForDerivedTextNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "Derived-text"
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFieldsForDerivedTextNode(); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFieldsForDerivedTextNode()
    {
        return new Hashtable<String,String>();
    }

    /**
     * <strong>listRequiredFieldsForGeospatialNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "geospatial"
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFieldsForGeospatialNode(); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFieldsForGeospatialNode()
    {
        return new Hashtable<String,String>();
    }

    /**
     * <strong>listRequiredFieldsForJournalNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "journal"
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFieldsForJournalNode(); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFieldsForJournalNode()
    {
        return new Hashtable<String,String>();
    }

    /**
     * <strong>listRequiredFieldsForSemanticsNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "semantics"
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFieldsForSemanticsNode(); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFieldsForSemanticsNode()
    {
        return new Hashtable<String,String>();
    }

    /**
     * <strong>listRequiredFieldsForSocialScienceNode</strong> est une méthode qui permet de récupérer la liste de tous les fields requis dans le noeud "socialscience"
     * @return la liste des fields requis
     * 
     * <hr>
     * <strong>Exemple : </strong>
     * <pre> 
     *      Hashtable<String,String> requiredFields = RequiredFields.listRequiredFieldsForSocialScienceNode(); 
     * </pre>
     */
    public static Hashtable<String,String> listRequiredFieldsForSocialScienceNode()
    {
        return new Hashtable<String,String>();
    }
}
