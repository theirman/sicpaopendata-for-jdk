package fr.inrae.sicpa.SicpaOpenData.Helper;

import java.io.IOException;
import java.util.Properties;

import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/** Classe de méthodes paratagées
 *  @author Thierry HEIRMAN
 *  @since Mars 2022
 */
public class Helper
{
    //    ___ _______________  _______  __  ____________
    //   / _ /_  __/_  __/ _ \/  _/ _ )/ / / /_  __/ __/
    //  / __ |/ /   / / / , _// // _  / /_/ / / / _\ \  
    // /_/ |_/_/   /_/ /_/|_/___/____/\____/ /_/ /___/  
    //                                                  





    //   _________  _  _______________  __  ___________________  _____  ____
    //  / ___/ __ \/ |/ / __/_  __/ _ \/ / / / ___/_  __/ __/ / / / _ \/ __/
    // / /__/ /_/ /    /\ \  / / / , _/ /_/ / /__  / / / _// /_/ / , _/\ \  
    // \___/\____/_/|_/___/ /_/ /_/|_|\____/\___/ /_/ /___/\____/_/|_/___/  
    //                                                                          





    //    __  _______________ ______  ___  ________
    //   /  |/  / __/_  __/ // / __ \/ _ \/ __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/  
    //                                             





    //    __  _______________ ______  ___  ________    _____________ ______________  __  __________
    //   /  |/  / __/_ __/  // / __ \/ _ \/ __/ __/   / __/_  __/ _ /_  __/  _/ __ \/ / / / __/ __/
    //  / /|_/ / _/  / / / _  / /_/ / // / _/_\ \    _\ \  / / / __ |/ / _/ // /_/ / /_/ / _/_\ \  
    // /_/  /_/___/ /_/ /_//_/\____/____/___/___/   /___/ /_/ /_/ |_/_/ /___/\___\_\____/___/___/  
    //

    public static String getFromWS(String url)
    {
        String response = "";
        
        try
        {
            // je crée un client http
            CloseableHttpClient httpClient = HttpClients.createDefault();

            // je crée la requête http
            HttpGet httpRequest = new HttpGet(url);
            
            //j'ajoute les headers à la requête
            httpRequest.addHeader(HttpHeaders.ACCEPT, "application/json");
            
            // j'exécute la requête et stocke la réponse dans une repose http
            CloseableHttpResponse httpResponse = httpClient.execute(httpRequest);
            
            //je traite la réponse
            if (httpResponse.getEntity() != null) 
                response = EntityUtils.toString(httpResponse.getEntity());
            
            httpClient.close();
        } 
        catch (ClientProtocolException e)
        {
            e.printStackTrace();
        } 
        catch (IOException e)
        {
            e.printStackTrace();
        }
        
        return response;
    }

    public static String getProperty(String propertyName)
    {
        String propertyValue = "";
        
        try 
        {
            Properties prop = new Properties();
            prop.load(Helper.class.getClassLoader().getResourceAsStream("SicpaOpenData.properties"));
            propertyValue = prop.getProperty(propertyName);  
        } 
        catch (IOException ioe) 
        {
            ioe.printStackTrace();
        }
        
        return propertyValue;
    }



}
