# Utiliser SicpaOpenData dans un projet Java

![Logo SICPA](https://germinal.toulouse.inra.fr/~theirman/img/CC_BY_INRAE_CatiSicpa.jpg)

Ce projet consiste en la création d'une librairie au format JAR pour faciliter les déploiements de sets de données vers les portails d'ouverture des données de l'INRAE

## Prérequis :
-	Le JAR `SicpaOpenData.jar` *(je considère dans cette doc qu’il se trouve dans lib du projet)*

## Option 1 : Utiliser le Build Path pour ajouter le JAR dans votre projet Java

-	Ouvrir le menu **Project** > **Properties**
-	Sélectionner **Java Build Path**
-	Cliquer sur **Add External JAR**
-	Dans **./lib**, sélectionner **SicpaOpenData.jar**
-	Cliquer sur **Apply and Close**

## Option 2 : Utiliser Maven pour ajouter le JAR dans votre projet Java

Dans la section `<dependencies>` du **pom.xml**, ajouter : 
```xml
    <dependency>
	  <groupId>fr.inrae.sicpa</groupId>
	  <artifactId>SicpaOpenData</artifactId>
	  <version>1.0</version>
	  <scope>system</scope>
	  <systemPath>${basedir}/lib/SicpaOpenData.jar</systemPath>
    </dependency>
```

## Utiliser SicpaOpenData.dll dans votre projet Java

```java
// utiliser l’API SicpaOpenData.API
import fr.inrae.sicpa.SicpaOpenData.API.DataverseAPI;
// utiliser l’API SicpaOpenData.Metadata
import fr.inrae.sicpa.SicpaOpenData.Metadata.*;
```

Et pour la suite, consulter la documentation technique de l’API [ici](https://germinal.toulouse.inra.fr/~theirman/SicpaOpenData/JDK/index.html)
